<?php

namespace Sugarcoat\Tests\Support;

use GuzzleHttp\Psr7\Response;
use GuzzleHttp\Psr7\ServerRequest;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

class PsrFactory
{
    /**
     * @param $method
     * @param $uri
     * @param array $body
     * @param array $headers
     * @return ServerRequestInterface
     */
    public static function createRequest($method, $uri, array $body = [], array $headers = [])
    {
        return new ServerRequest($method, $uri, $headers, null, '1.1', $body);
    }

    /**
     * @param $status
     * @param $uri
     * @param array $body
     * @param array $headers
     * @return ResponseInterface
     */
    public static function createResponse($status, array $body = [], array $headers = [])
    {
        return new Response($status, $headers, json_encode($body));
    }
}