<?php

namespace Sugarcoat\Tests\Support;

use Sugarcoat\APIWrapper\Config\Config;

class ConfigFactory
{
    const CONFIG = [
        'api_key' => '12345',
        'api_base_url' => 'http://api.sugar-coat.io/'
    ];

    /**
     * @return Config
     */
    public static function createConfig()
    {
        return new Config(self::CONFIG);
    }
}