<?php

namespace Sugarcoat\Tests\Dto;

use Sugarcoat\APIWrapper\Dto\PaginatedResponseDto;
use Sugarcoat\Tests\BaseTest;

class PaginatedResponseDtoTest extends BaseTest
{
    public function testConstructor()
    {
        $headers = [
            'some' => 'header'
        ];
        $data = [
            'some' => 'data'
        ];

        $response = new PaginatedResponseDto($headers, $data, 3, 2, 1 ,5);

        $this->assertSame($headers, $response->getHeaders());
        $this->assertSame($data, $response->getData());
        $this->assertSame(3, $response->getCount());
        $this->assertSame(2, $response->getInterval());
        $this->assertSame(1, $response->getCurrentPage());
        $this->assertSame(5, $response->getLastPage());
    }

    public function testToArray()
    {
        $headers = [
            'some' => 'header'
        ];
        $data = [
            'some' => 'data'
        ];

        $response = new PaginatedResponseDto($headers, $data, 3, 2, 1,5);

        $asArray = [
            'count' => 3,
            'interval' => 2,
            'current_page' => 1,
            'last_page' => 5,
            'headers' => [
                'some' => 'header'
            ],
            'data' => [
                'some' => 'data'
            ]
        ];

        $this->assertSame($asArray, $response->toArray());
    }

    public function testToJson()
    {
        $headers = [
            'some' => 'header'
        ];
        $data = [
            'some' => 'data'
        ];

        $response = new PaginatedResponseDto($headers, $data, 3, 2, 1,5);

        $asArray = [
            'count' => 3,
            'interval' => 2,
            'current_page' => 1,
            'last_page' => 5,
            'headers' => [
                'some' => 'header'
            ],
            'data' => [
                'some' => 'data'
            ]
        ];

        $this->assertSame(json_encode($asArray), json_encode($response));
    }
}