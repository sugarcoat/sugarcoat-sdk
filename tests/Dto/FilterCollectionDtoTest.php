<?php

namespace Sugarcoat\Tests\Dto;

use Faker;
use Sugarcoat\APIWrapper\Dto\FilterCollectionDto;
use Sugarcoat\APIWrapper\Dto\FilterDto;
use Sugarcoat\Tests\BaseTest;

class FilterCollectionDtoTest extends BaseTest
{
    public function testConstructor()
    {
        $filter1 = $this->filterFactory();
        $filter2 = $this->filterFactory();

        $collection = new FilterCollectionDto([$filter1, $filter2]);

        $this->assertCount(2, $collection->getFilterDtos());
        $this->assertSame($filter1, $collection->getFilterDtos()[0]);
        $this->assertSame($filter2, $collection->getFilterDtos()[1]);
    }

    public function testAccessors()
    {
        $collection = new FilterCollectionDto();

        $this->assertCount(0, $collection->getFilterDtos());

        $filter1 = $this->filterFactory();
        $filter2 = $this->filterFactory();

        $collection->addFilterDto($filter1);
        $collection->addFilterDto($filter2);

        $this->assertCount(2, $collection->getFilterDtos());
        $this->assertSame($filter1, $collection->getFilterDtos()[0]);
        $this->assertSame($filter2, $collection->getFilterDtos()[1]);
    }

    public function testToArray()
    {
        $filter1 = $this->filterFactory();
        $filter2 = $this->filterFactory();

        $collection = new FilterCollectionDto([$filter1, $filter2]);

        $asArray = [
            'filters' => [
                [
                    'property' => $filter1->getProperty(),
                    'value' => $filter1->getValue(),
                    'operator' => $filter1->getOperator()
                ],
                [
                    'property' => $filter2->getProperty(),
                    'value' => $filter2->getValue(),
                    'operator' => $filter2->getOperator()
                ]
            ]
        ];

        $this->assertSame($asArray, $collection->toArray());
    }

    public function testToJson()
    {
        $filter1 = $this->filterFactory();
        $filter2 = $this->filterFactory();

        $collection = new FilterCollectionDto([$filter1, $filter2]);

        $asArray = [
            'filters' => [
                [
                    'property' => $filter1->getProperty(),
                    'value' => $filter1->getValue(),
                    'operator' => $filter1->getOperator()
                ],
                [
                    'property' => $filter2->getProperty(),
                    'value' => $filter2->getValue(),
                    'operator' => $filter2->getOperator()
                ]
            ]
        ];

        $this->assertSame(json_encode($asArray), json_encode($collection));
    }

    /**
     * @return FilterDto
     */
    private function filterFactory()
    {
        $faker = Faker\Factory::create();

        $property = $faker->name();
        $value = $faker->name();
        $operator = '=';

        return new FilterDto($property, $value, $operator);
    }
}