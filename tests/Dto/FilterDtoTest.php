<?php

namespace Sugarcoat\Tests\Dto;

use Faker;
use Sugarcoat\APIWrapper\Dto\FilterDto;
use Sugarcoat\APIWrapper\Exception\InvalidDtoException;
use Sugarcoat\Tests\BaseTest;

class FilterDtoTest extends BaseTest
{
    public function testConstructor()
    {
        $faker = Faker\Factory::create();

        $property = $faker->name();
        $value = $faker->name();
        $operator = '=';

        $filter = new FilterDto($property, $value, $operator);

        $this->assertSame($property, $filter->getProperty());
        $this->assertSame($value, $filter->getValue());
        $this->assertSame($operator, $filter->getOperator());
    }

    public function testInvalidConstructor()
    {
        $this->expectException(InvalidDtoException::class);
        $this->expectExceptionMessage("Filter operator 'nonsense' not supported");

        $faker = Faker\Factory::create();

        $property = $faker->name();
        $value = $faker->name();
        $operator = 'nonsense';

        new FilterDto($property, $value, $operator);
    }

    public function testToArray()
    {
        $faker = Faker\Factory::create();

        $property = $faker->name();
        $value = $faker->name();
        $operator = '=';

        $filter = new FilterDto($property, $value, $operator);

        $asArray = [
            'property' => $filter->getProperty(),
            'value' => $filter->getValue(),
            'operator' => $filter->getOperator()
        ];

        $this->assertSame($asArray, $filter->toArray());
    }

    public function testToJson()
    {
        $faker = Faker\Factory::create();

        $property = $faker->name();
        $value = $faker->name();
        $operator = '=';

        $filter = new FilterDto($property, $value, $operator);

        $asArray = [
            'property' => $filter->getProperty(),
            'value' => $filter->getValue(),
            'operator' => $filter->getOperator(),
        ];

        $this->assertSame(json_encode($asArray), json_encode($filter));
    }
}