<?php

namespace Sugarcoat\Tests\Dto;

use Faker;
use Sugarcoat\APIWrapper\Dto\SortDto;
use Sugarcoat\APIWrapper\Exception\InvalidDtoException;
use Sugarcoat\Tests\BaseTest;

class SortDtoTest extends BaseTest
{
    public function testConstructor()
    {
        $faker = Faker\Factory::create();

        $property = $faker->name();
        $direction = 'asc';

        $filter = new SortDto($property, $direction);

        $this->assertSame($property, $filter->getProperty());
        $this->assertSame($direction, $filter->getDirection());
    }

    public function testInvalidConstructor()
    {
        $this->expectException(InvalidDtoException::class);
        $this->expectExceptionMessage("Sort direction 'nonsense' not supported");

        $faker = Faker\Factory::create();

        $property = $faker->name();
        $direction = 'nonsense';

        new SortDto($property, $direction);
    }

    public function testToArray()
    {
        $faker = Faker\Factory::create();

        $property = $faker->name();
        $direction = 'asc';

        $filter = new SortDto($property, $direction);

        $asArray = [
            'property' => $filter->getProperty(),
            'direction' => $filter->getDirection()
        ];

        $this->assertSame($asArray, $filter->toArray());
    }

    public function testToJson()
    {
        $faker = Faker\Factory::create();

        $property = $faker->name();
        $direction = 'asc';

        $filter = new SortDto($property, $direction);

        $asArray = [
            'property' => $filter->getProperty(),
            'direction' => $filter->getDirection()
        ];

        $this->assertSame(json_encode($asArray), json_encode($filter));
    }
}