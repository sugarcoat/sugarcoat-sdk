<?php

namespace Sugarcoat\Tests\Dto;

use Faker;
use Sugarcoat\APIWrapper\Dto\SortCollectionDto;
use Sugarcoat\APIWrapper\Dto\SortDto;
use Sugarcoat\Tests\BaseTest;

class SortCollectionDtoTest extends BaseTest
{
    public function testConstructor()
    {
        $sort1 = $this->sortFactory();
        $sort2 = $this->sortFactory();

        $collection = new SortCollectionDto([$sort1, $sort2]);

        $this->assertCount(2, $collection->getSortDtos());
        $this->assertSame($sort1, $collection->getSortDtos()[0]);
        $this->assertSame($sort2, $collection->getSortDtos()[1]);
    }

    public function testAccessors()
    {
        $collection = new SortCollectionDto();

        $this->assertCount(0, $collection->getSortDtos());

        $sort1 = $this->sortFactory();
        $sort2 = $this->sortFactory();

        $collection->addSortDto($sort1);
        $collection->addSortDto($sort2);

        $this->assertCount(2, $collection->getSortDtos());
        $this->assertSame($sort1, $collection->getSortDtos()[0]);
        $this->assertSame($sort2, $collection->getSortDtos()[1]);
    }

    public function testToArray()
    {
        $sort1 = $this->sortFactory();
        $sort2 = $this->sortFactory();

        $collection = new SortCollectionDto([$sort1, $sort2]);

        $asArray = [
            'sort' => [
                [
                    'property' => $sort1->getProperty(),
                    'direction' => $sort1->getDirection()
                ],
                [
                    'property' => $sort2->getProperty(),
                    'direction' => $sort2->getDirection()
                ]
            ]
        ];

        $this->assertSame($asArray, $collection->toArray());
    }

    public function testToJson()
    {
        $sort1 = $this->sortFactory();
        $sort2 = $this->sortFactory();

        $collection = new SortCollectionDto([$sort1, $sort2]);

        $asArray = [
            'sort' => [
                [
                    'property' => $sort1->getProperty(),
                    'direction' => $sort1->getDirection()
                ],
                [
                    'property' => $sort2->getProperty(),
                    'direction' => $sort2->getDirection()
                ]
            ]
        ];

        $this->assertSame(json_encode($asArray), json_encode($collection));
    }

    /**
     * @return SortDto
     */
    private function sortFactory()
    {
        $faker = Faker\Factory::create();

        $property = $faker->name();
        $direction = 'desc';

        return new SortDto($property, $direction);
    }
}