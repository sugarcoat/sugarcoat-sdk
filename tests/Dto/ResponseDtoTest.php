<?php

namespace Sugarcoat\Tests\Dto;

use Sugarcoat\APIWrapper\Dto\ResponseDto;
use Sugarcoat\Tests\BaseTest;

class ResponseDtoTest extends BaseTest
{
    public function testConstructor()
    {
        $headers = [
            'some' => 'header'
        ];
        $data = [
            'some' => 'data'
        ];

        $response = new ResponseDto($headers, $data);

        $this->assertSame($headers, $response->getHeaders());
        $this->assertSame($data, $response->getData());
    }

    public function testToArray()
    {
        $headers = [
            'some' => 'header'
        ];
        $data = [
            'some' => 'data'
        ];

        $response = new ResponseDto($headers, $data);

        $asArray = [
            'headers' => [
                'some' => 'header'
            ],
            'data' => [
                'some' => 'data'
            ]
        ];

        $this->assertSame($asArray, $response->toArray());
    }

    public function testToJson()
    {
        $headers = [
            'some' => 'header'
        ];
        $data = [
            'some' => 'data'
        ];

        $response = new ResponseDto($headers, $data);

        $asArray = [
            'headers' => [
                'some' => 'header'
            ],
            'data' => [
                'some' => 'data'
            ]
        ];

        $this->assertSame(json_encode($asArray), json_encode($response));
    }
}