<?php

namespace Sugarcoat\Tests\Dto;

use Faker;
use Sugarcoat\APIWrapper\Dto\PaginationDto;
use Sugarcoat\Tests\BaseTest;

class PaginationDtoTest extends BaseTest
{
    public function testConstructor()
    {
        $faker = Faker\Factory::create();

        $pageNumber = $faker->numberBetween(1, 10);
        $interval = $faker->numberBetween(1, 10);

        $filter = new PaginationDto($pageNumber, $interval);

        $this->assertSame($pageNumber, $filter->getPageNumber());
        $this->assertSame($interval, $filter->getInterval());
    }

    public function testToArray()
    {
        $faker = Faker\Factory::create();

        $pageNumber = $faker->numberBetween(1, 10);
        $interval = $faker->numberBetween(1, 10);

        $filter = new PaginationDto($pageNumber, $interval);

        $asArray = [
            'page' => $filter->getPageNumber(),
            'pagination_interval' => $filter->getInterval()
        ];

        $this->assertSame($asArray, $filter->toArray());
    }

    public function testToJson()
    {
        $faker = Faker\Factory::create();

        $pageNumber = $faker->numberBetween(1, 10);
        $interval = $faker->numberBetween(1, 10);

        $filter = new PaginationDto($pageNumber, $interval);

        $asArray = [
            'page' => $filter->getPageNumber(),
            'pagination_interval' => $filter->getInterval()
        ];

        $this->assertSame(json_encode($asArray), json_encode($filter));
    }
}