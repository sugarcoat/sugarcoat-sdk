<?php

namespace Sugarcoat\Tests;

use Psr\Http\Message\ServerRequestInterface;

abstract class BaseTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @param $method
     * @param $uri
     * @param $body
     * @param $headers
     * @return \Closure
     */
    protected function assertRequest($method, $uri, $body = [], $headers = [])
    {
        return function ($request) use ($method, $uri, $body, $headers) {
            /** @var ServerRequestInterface $request */

            $authHeader = ['Authorization' => ['12345']];

            if(empty($headers)){
                $headers = $authHeader;
            }
            else{
                $headers = array_merge($authHeader, $headers);
            }

            $this->assertInstanceOf(ServerRequestInterface::class, $request);
            $this->assertEquals($headers, $request->getHeaders());
            $this->assertEquals($method, $request->getMethod());
            $this->assertEquals($uri, $request->getUri()->getPath());
            $this->assertEquals($body, $request->getServerParams());
            return true;
        };
    }
}