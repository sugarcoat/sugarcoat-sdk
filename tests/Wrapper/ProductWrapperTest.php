<?php

namespace Sugarcoat\Tests\Wrapper;

use Mockery;
use Sugarcoat\APIWrapper\Client\HttpClient;
use Sugarcoat\APIWrapper\Constant\HttpMethod;
use Sugarcoat\APIWrapper\Dto\FilterCollectionDto;
use Sugarcoat\APIWrapper\Dto\FilterDto;
use Sugarcoat\APIWrapper\Dto\PaginatedResponseDto;
use Sugarcoat\APIWrapper\Dto\PaginationDto;
use Sugarcoat\APIWrapper\Dto\ResponseDto;
use Sugarcoat\APIWrapper\Dto\SortCollectionDto;
use Sugarcoat\APIWrapper\Dto\SortDto;
use Sugarcoat\APIWrapper\Wrapper\ProductWrapper;
use Sugarcoat\Tests\BaseTest;
use Sugarcoat\Tests\Support\ConfigFactory;
use Sugarcoat\Tests\Support\PsrFactory;

class ProductWrapperTest extends BaseTest
{
    public function testGetProducts()
    {
        $responseData = $this->paginatedResponseData();

        $paginationDto =  new PaginationDto(2, 10);
        $filterCollectionDto = new FilterCollectionDto([
            new FilterDto('property', 'value')
        ]);
        $sortCollectionDto = new SortCollectionDto([
            new SortDto('property', 'asc')
        ]);

        $requestData = array_merge(
            $paginationDto->toArray(),
            $filterCollectionDto->toArray(),
            $sortCollectionDto->toArray()
        );

        $response = PsrFactory::createResponse(200, $responseData);

        $clientMock = Mockery::mock(HttpClient::class)
            ->shouldReceive('sendRequest')
            ->withArgs(
                $this->assertRequest(HttpMethod::HTTP_GET, "products", $requestData)
            )
            ->andReturn($response)
            ->getMock();

        $wrapper = new ProductWrapper(
            ConfigFactory::createConfig(),
            $clientMock
        );

        $responseDto = $wrapper->getProducts($paginationDto, $filterCollectionDto, $sortCollectionDto);

        $this->assertInstanceOf(PaginatedResponseDto::class, $responseDto);
        $this->assertEquals(PaginatedResponseDto::class, get_class($responseDto));
        $this->assertEquals($responseData['data'], $responseDto->getData());
    }

    public function testGetProductById()
    {
        $id = 2;

        $responseData = $this->responseData();

        $response = PsrFactory::createResponse(200, $responseData);

        $clientMock = Mockery::mock(HttpClient::class)
            ->shouldReceive('sendRequest')
            ->withArgs(
                $this->assertRequest(HttpMethod::HTTP_GET, "products/2")
            )
            ->andReturn($response)
            ->getMock();

        $wrapper = new ProductWrapper(
            ConfigFactory::createConfig(),
            $clientMock
        );

        $responseDto = $wrapper->getProductById($id);

        $this->assertInstanceOf(ResponseDto::class, $responseDto);
        $this->assertEquals(ResponseDto::class, get_class($responseDto));
        $this->assertEquals($responseData['data'], $responseDto->getData());
    }

    public function testGetProductByName()
    {
        $name = 'some_name';

        $responseData = $this->paginatedResponseData();

        $response = PsrFactory::createResponse(200, $responseData);

        $clientMock = Mockery::mock(HttpClient::class)
            ->shouldReceive('sendRequest')
            ->withArgs(
                $this->assertRequest(HttpMethod::HTTP_GET, "products", [
                    'filters' => [
                        [
                            'property' => 'name',
                            'value' => $name,
                            'operator' => '='
                        ]
                    ],
                    'include' => '*'
                ])
            )
            ->andReturn($response)
            ->getMock();

        $wrapper = new ProductWrapper(
            ConfigFactory::createConfig(),
            $clientMock
        );

        $responseDto = $wrapper->getProductByName($name);

        $this->assertInstanceOf(PaginatedResponseDto::class, $responseDto);
        $this->assertEquals(PaginatedResponseDto::class, get_class($responseDto));
        $this->assertEquals($responseData['data'], $responseDto->getData());
    }

    public function testGetProductBySlug()
    {
        $slug = 'some_slug';

        $responseData = $this->paginatedResponseData();

        $response = PsrFactory::createResponse(200, $responseData);

        $clientMock = Mockery::mock(HttpClient::class)
            ->shouldReceive('sendRequest')
            ->withArgs(
                $this->assertRequest(HttpMethod::HTTP_GET, "products", [
                    'filters' => [
                        [
                            'property' => 'slug',
                            'value' => $slug,
                            'operator' => '='
                        ]
                    ],
                    'include' => '*'
                ])
            )
            ->andReturn($response)
            ->getMock();

        $wrapper = new ProductWrapper(
            ConfigFactory::createConfig(),
            $clientMock
        );

        $responseDto = $wrapper->getProductBySlug($slug);

        $this->assertInstanceOf(ResponseDto::class, $responseDto);
        $this->assertEquals(ResponseDto::class, get_class($responseDto));
        $this->assertEquals($responseData['data'][0], $responseDto->getData());
    }

    public function testGetProductsByTagIds()
    {
        $tagIds = [1, 2, 3];

        $responseData = $this->paginatedResponseData();

        $response = PsrFactory::createResponse(200, $responseData);

        $clientMock = Mockery::mock(HttpClient::class)
            ->shouldReceive('sendRequest')
            ->withArgs(
                $this->assertRequest(HttpMethod::HTTP_GET, "products", [
                    'filters' => [
                        [
                            'property' => 'tags',
                            'value' => $tagIds,
                            'operator' => 'where'
                        ]
                    ]
                ])
            )
            ->andReturn($response)
            ->getMock();

        $wrapper = new ProductWrapper(
            ConfigFactory::createConfig(),
            $clientMock
        );

        $responseDto = $wrapper->getProductsByTagIds($tagIds);

        $this->assertInstanceOf(PaginatedResponseDto::class, $responseDto);
        $this->assertEquals(PaginatedResponseDto::class, get_class($responseDto));
        $this->assertEquals($responseData['data'], $responseDto->getData());
    }

    public function testGetProductsByIds()
    {
        $ids = [1, 2, 3];

        $responseData = $this->paginatedResponseData();

        $response = PsrFactory::createResponse(200, $responseData);

        $clientMock = Mockery::mock(HttpClient::class)
            ->shouldReceive('sendRequest')
            ->withArgs(
                $this->assertRequest(HttpMethod::HTTP_GET, "products", [
                    'filters' => [
                        [
                            'property' => 'id',
                            'value' => $ids,
                            'operator' => 'whereIn'
                        ]
                    ]
                ])
            )
            ->andReturn($response)
            ->getMock();

        $wrapper = new ProductWrapper(
            ConfigFactory::createConfig(),
            $clientMock
        );

        $responseDto = $wrapper->getProductsByIds($ids);

        $this->assertInstanceOf(PaginatedResponseDto::class, $responseDto);
        $this->assertEquals(PaginatedResponseDto::class, get_class($responseDto));
        $this->assertEquals($responseData['data'], $responseDto->getData());
    }

    public function testSearchProducts()
    {
        $searchTerm = 'some_search_term';

        $responseData = $this->paginatedResponseData();

        $response = PsrFactory::createResponse(200, $responseData);

        $clientMock = Mockery::mock(HttpClient::class)
            ->shouldReceive('sendRequest')
            ->withArgs(
                $this->assertRequest(HttpMethod::HTTP_GET, "search/products", [
                        'query' => $searchTerm
                    ]
                )
            )
            ->andReturn($response)
            ->getMock();

        $wrapper = new ProductWrapper(
            ConfigFactory::createConfig(),
            $clientMock
        );

        $responseDto = $wrapper->searchProducts($searchTerm);

        $this->assertInstanceOf(PaginatedResponseDto::class, $responseDto);
        $this->assertEquals(PaginatedResponseDto::class, get_class($responseDto));
        $this->assertEquals($responseData['data'], $responseDto->getData());
    }

    public function testGetRecommendedProducts()
    {
        $ids = [1, 2, 3];
        $type = 'some_type';

        $responseData = $this->paginatedResponseData();

        $paginationDto =  new PaginationDto(2, 10);
        $filterCollectionDto = new FilterCollectionDto([
            new FilterDto('property', 'value')
        ]);
        $sortCollectionDto = new SortCollectionDto([
            new SortDto('property', 'asc')
        ]);

        $requestData = array_merge(
            $paginationDto->toArray(),
            $filterCollectionDto->toArray(),
            $sortCollectionDto->toArray()
        );

        $response = PsrFactory::createResponse(200, $responseData);

        $clientMock = Mockery::mock(HttpClient::class)
            ->shouldReceive('sendRequest')
            ->withArgs(
                $this->assertRequest(HttpMethod::HTTP_GET, "products/recommendations", array_merge([
                    'product_id' => $ids,
                    'type' => $type
                ], $requestData))
            )
            ->andReturn($response)
            ->getMock();

        $wrapper = new ProductWrapper(
            ConfigFactory::createConfig(),
            $clientMock
        );

        $responseDto = $wrapper->getRecommendedProducts($ids, $type, $paginationDto, $filterCollectionDto, $sortCollectionDto);

        $this->assertInstanceOf(PaginatedResponseDto::class, $responseDto);
        $this->assertEquals(PaginatedResponseDto::class, get_class($responseDto));
        $this->assertEquals($responseData['data'], $responseDto->getData());
    }

    /**
     * @return array
     */
    protected function responseData()
    {
        return [
            'data' => [
                'some' => 'data'
            ]
        ];
    }

    /**
     * @return array
     */
    protected function paginatedResponseData()
    {
        return [
            'data' => [
                [
                    'some' => 'data'
                ],
                [
                    'some' => 'data'
                ]
            ]
        ];
    }
}