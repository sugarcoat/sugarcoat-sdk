<?php

namespace Sugarcoat\Tests\Wrapper;

use Mockery;
use Sugarcoat\APIWrapper\Client\HttpClient;
use Sugarcoat\APIWrapper\Constant\HttpMethod;
use Sugarcoat\APIWrapper\Dto\FilterCollectionDto;
use Sugarcoat\APIWrapper\Dto\FilterDto;
use Sugarcoat\APIWrapper\Dto\PaginatedResponseDto;
use Sugarcoat\APIWrapper\Dto\PaginationDto;
use Sugarcoat\APIWrapper\Dto\ResponseDto;
use Sugarcoat\APIWrapper\Dto\SortCollectionDto;
use Sugarcoat\APIWrapper\Dto\SortDto;
use Sugarcoat\APIWrapper\Wrapper\ProductTypeWrapper;
use Sugarcoat\Tests\BaseTest;
use Sugarcoat\Tests\Support\ConfigFactory;
use Sugarcoat\Tests\Support\PsrFactory;

class ProductTypeWrapperTest extends BaseTest
{
    public function testGetProductTypes()
    {
        $responseData = $this->paginatedResponseData();

        $paginationDto =  new PaginationDto(2, 10);
        $filterCollectionDto = new FilterCollectionDto([
            new FilterDto('property', 'value')
        ]);
        $sortCollectionDto = new SortCollectionDto([
            new SortDto('property', 'asc')
        ]);

        $requestData = array_merge(
            $paginationDto->toArray(),
            $filterCollectionDto->toArray(),
            $sortCollectionDto->toArray()
        );

        $response = PsrFactory::createResponse(200, $responseData);

        $clientMock = Mockery::mock(HttpClient::class)
            ->shouldReceive('sendRequest')
            ->withArgs(
                $this->assertRequest(HttpMethod::HTTP_GET, "product-types", $requestData)
            )
            ->andReturn($response)
            ->getMock();

        $wrapper = new ProductTypeWrapper(
            ConfigFactory::createConfig(),
            $clientMock
        );

        $responseDto = $wrapper->getProductTypes($paginationDto, $filterCollectionDto, $sortCollectionDto);

        $this->assertInstanceOf(PaginatedResponseDto::class, $responseDto);
        $this->assertEquals(PaginatedResponseDto::class, get_class($responseDto));
        $this->assertEquals($responseData['data'], $responseDto->getData());
    }

    public function testGetProductTypeById()
    {
        $id = 2;

        $responseData = $this->responseData();

        $response = PsrFactory::createResponse(200, $responseData);

        $clientMock = Mockery::mock(HttpClient::class)
            ->shouldReceive('sendRequest')
            ->withArgs(
                $this->assertRequest(HttpMethod::HTTP_GET, "product-types/2")
            )
            ->andReturn($response)
            ->getMock();

        $wrapper = new ProductTypeWrapper(
            ConfigFactory::createConfig(),
            $clientMock
        );

        $responseDto = $wrapper->getProductTypeById($id);

        $this->assertInstanceOf(ResponseDto::class, $responseDto);
        $this->assertEquals(ResponseDto::class, get_class($responseDto));
        $this->assertEquals($responseData['data'], $responseDto->getData());
    }

    public function testGetProductTypeBySlug()
    {
        $slug = 'some_slug';

        $responseData = $this->paginatedResponseData();

        $response = PsrFactory::createResponse(200, $responseData);

        $clientMock = Mockery::mock(HttpClient::class)
            ->shouldReceive('sendRequest')
            ->withArgs(
                $this->assertRequest(HttpMethod::HTTP_GET, "product-types", [
                    'filters' => [
                        [
                            'property' => 'slug',
                            'value' => $slug,
                            'operator' => '='
                        ]
                    ],
                    'include' => '*'
                ])
            )
            ->andReturn($response)
            ->getMock();

        $wrapper = new ProductTypeWrapper(
            ConfigFactory::createConfig(),
            $clientMock
        );

        $responseDto = $wrapper->getProductTypeBySlug($slug);

        $this->assertInstanceOf(ResponseDto::class, $responseDto);
        $this->assertEquals(ResponseDto::class, get_class($responseDto));
        $this->assertEquals($responseData['data'][0], $responseDto->getData());
    }

    /**
     * @return array
     */
    protected function responseData()
    {
        return [
            'data' => [
                'some' => 'data'
            ]
        ];
    }

    /**
     * @return array
     */
    protected function paginatedResponseData()
    {
        return [
            'data' => [
                [
                    'some' => 'data'
                ],
                [
                    'some' => 'data'
                ]
            ]
        ];
    }
}