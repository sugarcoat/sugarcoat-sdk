<?php

namespace Sugarcoat\Tests\Wrapper;

use Mockery;
use Sugarcoat\APIWrapper\Client\HttpClient;
use Sugarcoat\APIWrapper\Constant\HttpMethod;
use Sugarcoat\APIWrapper\Dto\PaginatedResponseDto;
use Sugarcoat\APIWrapper\Dto\ResponseDto;
use Sugarcoat\APIWrapper\Wrapper\CustomerWrapper;
use Sugarcoat\Tests\BaseTest;
use Sugarcoat\Tests\Support\ConfigFactory;
use Sugarcoat\Tests\Support\PsrFactory;

class CustomerWrapperTest extends BaseTest
{
    public function testGetCustomerById()
    {
        $id = 2;

        $responseData = $this->responseData();

        $response = PsrFactory::createResponse(200, $responseData);

        $clientMock = Mockery::mock(HttpClient::class)
            ->shouldReceive('sendRequest')
            ->withArgs(
                $this->assertRequest(HttpMethod::HTTP_GET, "customers/2")
            )
            ->andReturn($response)
            ->getMock();

        $wrapper = new CustomerWrapper(
            ConfigFactory::createConfig(),
            $clientMock
        );

        $responseDto = $wrapper->getCustomerById($id);

        $this->assertInstanceOf(ResponseDto::class, $responseDto);
        $this->assertEquals(ResponseDto::class, get_class($responseDto));
        $this->assertEquals($responseData['data'], $responseDto->getData());
    }

    public function testCreateCustomer()
    {
        $responseData = $this->responseData();

        $requestData = [
            'some' => 'request'
        ];

        $response = PsrFactory::createResponse(200, $responseData);

        $clientMock = Mockery::mock(HttpClient::class)
            ->shouldReceive('sendRequest')
            ->withArgs(
                $this->assertRequest(HttpMethod::HTTP_POST, "customers", $requestData)
            )
            ->andReturn($response)
            ->getMock();

        $wrapper = new CustomerWrapper(
            ConfigFactory::createConfig(),
            $clientMock
        );

        $responseDto = $wrapper->createCustomer($requestData);

        $this->assertInstanceOf(ResponseDto::class, $responseDto);
        $this->assertEquals(ResponseDto::class, get_class($responseDto));
        $this->assertEquals($responseData['data'], $responseDto->getData());
    }

    public function testUpdateCustomer()
    {
        $id = 2;

        $responseData = $this->responseData();

        $requestData = [
            'some' => 'request'
        ];

        $response = PsrFactory::createResponse(200, $responseData);

        $clientMock = Mockery::mock(HttpClient::class)
            ->shouldReceive('sendRequest')
            ->withArgs(
                $this->assertRequest(HttpMethod::HTTP_PUT, "customers/2", $requestData)
            )
            ->andReturn($response)
            ->getMock();

        $wrapper = new CustomerWrapper(
            ConfigFactory::createConfig(),
            $clientMock
        );

        $responseDto = $wrapper->updateCustomer($id, $requestData);

        $this->assertInstanceOf(ResponseDto::class, $responseDto);
        $this->assertEquals(ResponseDto::class, get_class($responseDto));
        $this->assertEquals($responseData['data'], $responseDto->getData());
    }

    public function testGetCustomerOrders()
    {
        $id = 2;

        $responseData = $this->responseData();

        $response = PsrFactory::createResponse(200, $responseData);

        $clientMock = Mockery::mock(HttpClient::class)
            ->shouldReceive('sendRequest')
            ->withArgs(
                $this->assertRequest(HttpMethod::HTTP_GET, "customers/2/orders")
            )
            ->andReturn($response)
            ->getMock();

        $wrapper = new CustomerWrapper(
            ConfigFactory::createConfig(),
            $clientMock
        );

        $responseDto = $wrapper->getCustomerOrders($id);

        $this->assertInstanceOf(PaginatedResponseDto::class, $responseDto);
        $this->assertEquals(PaginatedResponseDto::class, get_class($responseDto));
        $this->assertEquals($responseData['data'], $responseDto->getData());
    }

    /**
     * @return array
     */
    protected function responseData()
    {
        return [
            'data' => [
                'some' => 'data'
            ]
        ];
    }

    /**
     * @return array
     */
    protected function paginatedResponseData()
    {
        return [
            'data' => [
                [
                    'some' => 'data'
                ],
                [
                    'some' => 'data'
                ]
            ]
        ];
    }
}