<?php

namespace Sugarcoat\Tests\Wrapper;

use Mockery;
use Sugarcoat\APIWrapper\Client\HttpClient;
use Sugarcoat\APIWrapper\Constant\HttpMethod;
use Sugarcoat\APIWrapper\Dto\FilterCollectionDto;
use Sugarcoat\APIWrapper\Dto\FilterDto;
use Sugarcoat\APIWrapper\Dto\PaginatedResponseDto;
use Sugarcoat\APIWrapper\Dto\PaginationDto;
use Sugarcoat\APIWrapper\Dto\ResponseDto;
use Sugarcoat\APIWrapper\Dto\SortCollectionDto;
use Sugarcoat\APIWrapper\Dto\SortDto;
use Sugarcoat\APIWrapper\Wrapper\CountryWrapper;
use Sugarcoat\Tests\BaseTest;
use Sugarcoat\Tests\Support\ConfigFactory;
use Sugarcoat\Tests\Support\PsrFactory;

class CountryWrapperTest extends BaseTest
{
    public function testGetCountries()
    {
        $responseData = [
            'data' => [
                [
                    'some' => 'data'
                ],
                [
                    'some' => 'data'
                ]
            ]
        ];

        $response = PsrFactory::createResponse(200, $responseData);

        $clientMock = Mockery::mock(HttpClient::class)
            ->shouldReceive('sendRequest')
            ->withArgs(
                $this->assertRequest(HttpMethod::HTTP_GET, "countries")
            )
            ->andReturn($response)
            ->getMock();

        $wrapper = new CountryWrapper(
            ConfigFactory::createConfig(),
            $clientMock
        );

        $responseDto = $wrapper->getCountries();

        $this->assertInstanceOf(PaginatedResponseDto::class, $responseDto);
        $this->assertEquals(PaginatedResponseDto::class, get_class($responseDto));
        $this->assertEquals($responseData['data'], $responseDto->getData());
    }
}