<?php

namespace Sugarcoat\Tests\Wrapper;

use Mockery;
use Psr\Http\Message\ResponseInterface;
use Sugarcoat\APIWrapper\Client\ClientInterface;
use Sugarcoat\APIWrapper\Constant\HttpMethod;
use Sugarcoat\APIWrapper\Wrapper\BaseWrapper;
use Sugarcoat\Tests\BaseTest;
use Sugarcoat\Tests\Support\ConfigFactory;
use Sugarcoat\Tests\Support\PsrFactory;

class BaseWrapperTest extends BaseTest
{
    public function testGetApiBaseUrl()
    {
        $config = ConfigFactory::createConfig();

        $client = Mockery::mock(ClientInterface::class);

        $wrapper = new BaseWrapper($config, $client);

        $this->assertEquals($config->getApiBaseUrl(), $wrapper->getApiBaseUrl());
    }

    public function testSendRequest()
    {
        $headers = [
            'some' => 'header'
        ];

        $responseData = [
            'some' => 'response'
        ];

        $requestData = [
            'some' => 'request'
        ];

        $response = PsrFactory::createResponse(200, $responseData, $headers);
        $request = PsrFactory::createRequest(HttpMethod::HTTP_PUT, 'somewhere', $requestData, $headers);

        $config = ConfigFactory::createConfig();

        $client = Mockery::mock(ClientInterface::class)
            ->shouldReceive('sendRequest')
            ->withArgs(
                $this->assertRequest(HttpMethod::HTTP_PUT, "somewhere", $requestData, ['some' => ['header']])
            )
            ->andReturn($response)
            ->getMock();

        $wrapper = new BaseWrapper($config, $client);

        $response = $wrapper->sendRequest($request);

        $this->assertInstanceOf(ResponseInterface::class, $response);
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertEquals(['some' => ['header']], $response->getHeaders());
        $this->assertEquals($responseData, json_decode($response->getBody()->getContents(), true));
    }
}