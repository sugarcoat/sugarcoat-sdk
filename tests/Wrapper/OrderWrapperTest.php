<?php

namespace Sugarcoat\Tests\Wrapper;

use Mockery;
use Sugarcoat\APIWrapper\Client\HttpClient;
use Sugarcoat\APIWrapper\Constant\HttpMethod;
use Sugarcoat\APIWrapper\Dto\FilterCollectionDto;
use Sugarcoat\APIWrapper\Dto\FilterDto;
use Sugarcoat\APIWrapper\Dto\PaginatedResponseDto;
use Sugarcoat\APIWrapper\Dto\PaginationDto;
use Sugarcoat\APIWrapper\Dto\ResponseDto;
use Sugarcoat\APIWrapper\Dto\SortCollectionDto;
use Sugarcoat\APIWrapper\Dto\SortDto;
use Sugarcoat\APIWrapper\Wrapper\OrderWrapper;
use Sugarcoat\Tests\BaseTest;
use Sugarcoat\Tests\Support\ConfigFactory;
use Sugarcoat\Tests\Support\PsrFactory;

class OrderWrapperTest extends BaseTest
{
    public function testGetOrderById()
    {
        $id = 2;

        $responseData = $this->responseData();

        $response = PsrFactory::createResponse(200, $responseData);

        $clientMock = Mockery::mock(HttpClient::class)
            ->shouldReceive('sendRequest')
            ->withArgs(
                $this->assertRequest(HttpMethod::HTTP_GET, "orders/2")
            )
            ->andReturn($response)
            ->getMock();

        $wrapper = new OrderWrapper(
            ConfigFactory::createConfig(),
            $clientMock
        );

        $responseDto = $wrapper->getOrderById($id);

        $this->assertInstanceOf(ResponseDto::class, $responseDto);
        $this->assertEquals(ResponseDto::class, get_class($responseDto));
        $this->assertEquals($responseData['data'], $responseDto->getData());
    }

    public function testGetOrderByCode()
    {
        $code = 'some_code';

        $responseData = $this->paginatedResponseData();

        $response = PsrFactory::createResponse(200, $responseData);

        $clientMock = Mockery::mock(HttpClient::class)
            ->shouldReceive('sendRequest')
            ->withArgs(
                $this->assertRequest(HttpMethod::HTTP_GET, "orders", [
                    'filters' => [
                        [
                            'property' => 'code',
                            'value' => $code,
                            'operator' => '='
                        ]
                    ],
                    'include' => '*'
                ])
            )
            ->andReturn($response)
            ->getMock();

        $wrapper = new OrderWrapper(
            ConfigFactory::createConfig(),
            $clientMock
        );

        $responseDto = $wrapper->getOrderByCode($code);

        $this->assertInstanceOf(ResponseDto::class, $responseDto);
        $this->assertEquals(ResponseDto::class, get_class($responseDto));
        $this->assertEquals($responseData['data'][0], $responseDto->getData());
    }

    public function testCreateOrder()
    {
        $customerId = 1;
        $basketReference = 'some_reference';
        $paymentGatewayId = 2;
        $termsId = 1;
        $partnerReference = 'some_partner';
        $productData = [
            [
                'id' => 3
            ]
        ];
        $customerData = [
            'email' => 'some_email'
        ];

        $responseData = $this->responseData();

        $response = PsrFactory::createResponse(200, $responseData);

        $clientMock = Mockery::mock(HttpClient::class)
            ->shouldReceive('sendRequest')
            ->withArgs(
                $this->assertRequest(HttpMethod::HTTP_POST, "orders", [
                    'customer_id' => $customerId,
                    'basket_reference' => $basketReference,
                    'payment_gateway_id' => $paymentGatewayId,
                    'terms_id' => $termsId,
                    'partner_reference' => $partnerReference,
                    'products' => $productData,
                    'customer' => $customerData
                ])
            )
            ->andReturn($response)
            ->getMock();

        $wrapper = new OrderWrapper(
            ConfigFactory::createConfig(),
            $clientMock
        );

        $responseDto = $wrapper->createOrder($customerId, $basketReference, $paymentGatewayId, $termsId, $partnerReference, $productData, $customerData);

        $this->assertInstanceOf(ResponseDto::class, $responseDto);
        $this->assertEquals(ResponseDto::class, get_class($responseDto));
        $this->assertEquals($responseData['data'], $responseDto->getData());
    }

    public function testCompleteOrder()
    {
        $orderId = 1;
        $transactionReference = 'some_reference';
        $trace = [
            'some' => 'trace'
        ];

        $responseData = $this->responseData();

        $response = PsrFactory::createResponse(200, $responseData);

        $clientMock = Mockery::mock(HttpClient::class)
            ->shouldReceive('sendRequest')
            ->withArgs(
                $this->assertRequest(HttpMethod::HTTP_POST, "orders/1/complete", [
                    'transaction_reference' => $transactionReference,
                    'trace' => $trace
                ])
            )
            ->andReturn($response)
            ->getMock();

        $wrapper = new OrderWrapper(
            ConfigFactory::createConfig(),
            $clientMock
        );

        $responseDto = $wrapper->completeOrder($orderId, $transactionReference, $trace);

        $this->assertInstanceOf(ResponseDto::class, $responseDto);
        $this->assertEquals(ResponseDto::class, get_class($responseDto));
        $this->assertEquals($responseData['data'], $responseDto->getData());
    }

    public function testFailedOrder()
    {
        $orderId = 1;
        $trace = [
            'some' => 'trace'
        ];

        $responseData = $this->responseData();

        $response = PsrFactory::createResponse(200, $responseData);

        $clientMock = Mockery::mock(HttpClient::class)
            ->shouldReceive('sendRequest')
            ->withArgs(
                $this->assertRequest(HttpMethod::HTTP_POST, "orders/1/fail", [
                    'trace' => $trace
                ])
            )
            ->andReturn($response)
            ->getMock();

        $wrapper = new OrderWrapper(
            ConfigFactory::createConfig(),
            $clientMock
        );

        $responseDto = $wrapper->failedOrder($orderId, $trace);

        $this->assertInstanceOf(ResponseDto::class, $responseDto);
        $this->assertEquals(ResponseDto::class, get_class($responseDto));
        $this->assertEquals($responseData['data'], $responseDto->getData());
    }

    public function testPayOrder()
    {
        $id = 2;

        $responseData = $this->responseData();

        $response = PsrFactory::createResponse(200, $responseData);

        $clientMock = Mockery::mock(HttpClient::class)
            ->shouldReceive('sendRequest')
            ->withArgs(
                $this->assertRequest(HttpMethod::HTTP_GET, "orders/2/pay")
            )
            ->andReturn($response)
            ->getMock();

        $wrapper = new OrderWrapper(
            ConfigFactory::createConfig(),
            $clientMock
        );

        $responseDto = $wrapper->payOrder($id);

        $this->assertInstanceOf(ResponseDto::class, $responseDto);
        $this->assertEquals(ResponseDto::class, get_class($responseDto));
        $this->assertEquals($responseData['data'], $responseDto->getData());
    }

    public function testGetDeliveryTypes()
    {
        $responseData = $this->paginatedResponseData();

        $paginationDto =  new PaginationDto(2, 10);
        $filterCollectionDto = new FilterCollectionDto([
            new FilterDto('property', 'value')
        ]);
        $sortCollectionDto = new SortCollectionDto([
            new SortDto('property', 'asc')
        ]);

        $requestData = array_merge(
            $paginationDto->toArray(),
            $filterCollectionDto->toArray(),
            $sortCollectionDto->toArray()
        );

        $response = PsrFactory::createResponse(200, $responseData);

        $clientMock = Mockery::mock(HttpClient::class)
            ->shouldReceive('sendRequest')
            ->withArgs(
                $this->assertRequest(HttpMethod::HTTP_GET, "delivery-types", $requestData)
            )
            ->andReturn($response)
            ->getMock();

        $wrapper = new OrderWrapper(
            ConfigFactory::createConfig(),
            $clientMock
        );

        $responseDto = $wrapper->getDeliveryTypes($paginationDto, $filterCollectionDto, $sortCollectionDto);

        $this->assertInstanceOf(PaginatedResponseDto::class, $responseDto);
        $this->assertEquals(PaginatedResponseDto::class, get_class($responseDto));
        $this->assertEquals($responseData['data'], $responseDto->getData());
    }

    /**
     * @return array
     */
    protected function responseData()
    {
        return [
            'data' => [
                'some' => 'data'
            ]
        ];
    }

    /**
     * @return array
     */
    protected function paginatedResponseData()
    {
        return [
            'data' => [
                [
                    'some' => 'data'
                ],
                [
                    'some' => 'data'
                ]
            ]
        ];
    }
}