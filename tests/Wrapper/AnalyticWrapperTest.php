<?php

namespace Sugarcoat\Tests\Wrapper;

use Mockery;
use Sugarcoat\APIWrapper\Client\HttpClient;
use Sugarcoat\APIWrapper\Constant\HttpMethod;
use Sugarcoat\APIWrapper\Dto\ResponseDto;
use Sugarcoat\APIWrapper\Wrapper\AnalyticWrapper;
use Sugarcoat\Tests\BaseTest;
use Sugarcoat\Tests\Support\ConfigFactory;
use Sugarcoat\Tests\Support\PsrFactory;

class AnalyticWrapperTest extends BaseTest
{
    public function testTrackAnalytic()
    {
        $responseData = [
            'data' => [
                'some' => 'data'
            ]
        ];

        $requestData = [
            'some' => 'request'
        ];

        $response = PsrFactory::createResponse(200, $responseData);

        $clientMock = Mockery::mock(HttpClient::class)
            ->shouldReceive('sendRequest')
            ->withArgs(
                $this->assertRequest(HttpMethod::HTTP_GET, "analytics", $requestData)
            )
            ->andReturn($response)
            ->getMock();

        $wrapper = new AnalyticWrapper(
            ConfigFactory::createConfig(),
            $clientMock
        );

        $responseDto = $wrapper->trackAnalytic($requestData);

        $this->assertInstanceOf(ResponseDto::class, $responseDto);
        $this->assertEquals(ResponseDto::class, get_class($responseDto));
        $this->assertEquals($responseData['data'], $responseDto->getData());
    }

    public function testTrackAnalyticByProductId()
    {
        $productId = 1;

        $responseData = [
            'data' => [
                'some' => 'data'
            ]
        ];

        $requestData = [
            'some' => 'request'
        ];

        $response = PsrFactory::createResponse(200, $responseData);

        $clientMock = Mockery::mock(HttpClient::class)
            ->shouldReceive('sendRequest')
            ->withArgs(
                $this->assertRequest(HttpMethod::HTTP_GET, "analytics", array_merge([
                    'product_id' => $productId
                ], $requestData))
            )
            ->andReturn($response)
            ->getMock();

        $wrapper = new AnalyticWrapper(
            ConfigFactory::createConfig(),
            $clientMock
        );

        $responseDto = $wrapper->trackAnalyticByProductId($productId, $requestData);

        $this->assertInstanceOf(ResponseDto::class, $responseDto);
        $this->assertEquals(ResponseDto::class, get_class($responseDto));
        $this->assertEquals($responseData['data'], $responseDto->getData());
    }

    public function testTrackAnalyticByDestination()
    {
        $destination = 'some_where';

        $responseData = [
            'data' => [
                'some' => 'data'
            ]
        ];

        $requestData = [
            'some' => 'request'
        ];

        $response = PsrFactory::createResponse(200, $responseData);

        $clientMock = Mockery::mock(HttpClient::class)
            ->shouldReceive('sendRequest')
            ->withArgs(
                $this->assertRequest(HttpMethod::HTTP_GET, "analytics", array_merge([
                    'destination' => $destination
                ], $requestData))
            )
            ->andReturn($response)
            ->getMock();

        $wrapper = new AnalyticWrapper(
            ConfigFactory::createConfig(),
            $clientMock
        );

        $responseDto = $wrapper->trackAnalyticByDestination($destination, $requestData);

        $this->assertInstanceOf(ResponseDto::class, $responseDto);
        $this->assertEquals(ResponseDto::class, get_class($responseDto));
        $this->assertEquals($responseData['data'], $responseDto->getData());
    }

    public function testTrackAnalyticByType()
    {
        $type = 'some_type';

        $responseData = [
            'data' => [
                'some' => 'data'
            ]
        ];

        $requestData = [
            'some' => 'request'
        ];

        $response = PsrFactory::createResponse(200, $responseData);

        $clientMock = Mockery::mock(HttpClient::class)
            ->shouldReceive('sendRequest')
            ->withArgs(
                $this->assertRequest(HttpMethod::HTTP_GET, "analytics", array_merge([
                    'type' => $type
                ], $requestData))
            )
            ->andReturn($response)
            ->getMock();

        $wrapper = new AnalyticWrapper(
            ConfigFactory::createConfig(),
            $clientMock
        );

        $responseDto = $wrapper->trackAnalyticByType($type, $requestData);

        $this->assertInstanceOf(ResponseDto::class, $responseDto);
        $this->assertEquals(ResponseDto::class, get_class($responseDto));
        $this->assertEquals($responseData['data'], $responseDto->getData());
    }
}