<?php

namespace Sugarcoat\Tests\Wrapper;

use Mockery;
use Sugarcoat\APIWrapper\Client\HttpClient;
use Sugarcoat\APIWrapper\Constant\HttpMethod;
use Sugarcoat\APIWrapper\Dto\ResponseDto;
use Sugarcoat\APIWrapper\Wrapper\BasketWrapper;
use Sugarcoat\Tests\BaseTest;
use Sugarcoat\Tests\Support\ConfigFactory;
use Sugarcoat\Tests\Support\PsrFactory;

class BasketWrapperTest extends BaseTest
{
    public function testGetBasket()
    {
        $token = 'some_token';

        $responseData = $this->responseData();

        $response = PsrFactory::createResponse(200, $responseData);

        $clientMock = Mockery::mock(HttpClient::class)
            ->shouldReceive('sendRequest')
            ->withArgs(
                $this->assertRequest(HttpMethod::HTTP_GET, "baskets/some_token")
            )
            ->andReturn($response)
            ->getMock();

        $wrapper = new BasketWrapper(
            ConfigFactory::createConfig(),
            $clientMock
        );

        $responseDto = $wrapper->getBasket($token);

        $this->assertInstanceOf(ResponseDto::class, $responseDto);
        $this->assertEquals(ResponseDto::class, get_class($responseDto));
        $this->assertEquals($responseData['data'], $responseDto->getData());
    }

    public function testGetBasketDeliveryTypes()
    {
        $token = 'some_token';

        $responseData = $this->responseData();

        $response = PsrFactory::createResponse(200, $responseData);

        $clientMock = Mockery::mock(HttpClient::class)
            ->shouldReceive('sendRequest')
            ->withArgs(
                $this->assertRequest(HttpMethod::HTTP_GET, "baskets/some_token/delivery-types")
            )
            ->andReturn($response)
            ->getMock();

        $wrapper = new BasketWrapper(
            ConfigFactory::createConfig(),
            $clientMock
        );

        $responseDto = $wrapper->getBasketDeliveryTypes($token);

        $this->assertInstanceOf(ResponseDto::class, $responseDto);
        $this->assertEquals(ResponseDto::class, get_class($responseDto));
        $this->assertEquals($responseData['data'], $responseDto->getData());
    }

    public function testCreateBasket()
    {
        $userId = 1;
        $deliveryTypeId = 1;
        $customerId = 1;

        $responseData = $this->responseData();

        $requestData = [
            'some' => 'request'
        ];

        $response = PsrFactory::createResponse(200, $responseData);

        $clientMock = Mockery::mock(HttpClient::class)
            ->shouldReceive('sendRequest')
            ->withArgs(
                $this->assertRequest(HttpMethod::HTTP_POST, "baskets", [
                    'user_id' => $userId,
                    'products' => $requestData,
                    'delivery_type_id' => $deliveryTypeId,
                    'customer_id' => $customerId
                ])
            )
            ->andReturn($response)
            ->getMock();

        $wrapper = new BasketWrapper(
            ConfigFactory::createConfig(),
            $clientMock
        );

        $responseDto = $wrapper->createBasket($userId, $requestData, $deliveryTypeId, $customerId);

        $this->assertInstanceOf(ResponseDto::class, $responseDto);
        $this->assertEquals(ResponseDto::class, get_class($responseDto));
        $this->assertEquals($responseData['data'], $responseDto->getData());
    }

    public function testUpdateBasket()
    {
        $token = 'some_token';
        $userId = 1;
        $deliveryTypeId = 1;
        $customerId = 1;

        $responseData = $this->responseData();

        $requestData = [
            'some' => 'request'
        ];

        $response = PsrFactory::createResponse(200, $responseData);

        $clientMock = Mockery::mock(HttpClient::class)
            ->shouldReceive('sendRequest')
            ->withArgs(
                $this->assertRequest(HttpMethod::HTTP_PUT, "baskets/some_token", [
                    'user_id' => $userId,
                    'products' => $requestData,
                    'delivery_type_id' => $deliveryTypeId,
                    'customer_id' => $customerId
                ])
            )
            ->andReturn($response)
            ->getMock();

        $wrapper = new BasketWrapper(
            ConfigFactory::createConfig(),
            $clientMock
        );

        $responseDto = $wrapper->updateBasket($token, $userId, $requestData, $deliveryTypeId, $customerId);

        $this->assertInstanceOf(ResponseDto::class, $responseDto);
        $this->assertEquals(ResponseDto::class, get_class($responseDto));
        $this->assertEquals($responseData['data'], $responseDto->getData());
    }

    public function testClearBasket()
    {
        $token = 'some_token';

        $responseData = $this->responseData();

        $response = PsrFactory::createResponse(200, $responseData);

        $clientMock = Mockery::mock(HttpClient::class)
            ->shouldReceive('sendRequest')
            ->withArgs(
                $this->assertRequest(HttpMethod::HTTP_DELETE, "baskets/some_token")
            )
            ->andReturn($response)
            ->getMock();

        $wrapper = new BasketWrapper(
            ConfigFactory::createConfig(),
            $clientMock
        );

        $responseDto = $wrapper->clearBasket($token);

        $this->assertInstanceOf(ResponseDto::class, $responseDto);
        $this->assertEquals(ResponseDto::class, get_class($responseDto));
        $this->assertEquals($responseData['data'], $responseDto->getData());
    }

    public function testAddProductToBasket()
    {
        $token = 'some_token';
        $productId = 1;
        $quantity = 10;
        $deliveryTypeId = 2;

        $responseData = $this->responseData();

        $response = PsrFactory::createResponse(200, $responseData);

        $clientMock = Mockery::mock(HttpClient::class)
            ->shouldReceive('sendRequest')
            ->withArgs(
                $this->assertRequest(HttpMethod::HTTP_PUT, "baskets/some_token/products/1", [
                    'quantity' => $quantity,
                    'delivery_type_id' => $deliveryTypeId
                ])
            )
            ->andReturn($response)
            ->getMock();

        $wrapper = new BasketWrapper(
            ConfigFactory::createConfig(),
            $clientMock
        );

        $responseDto = $wrapper->addProductToBasket($token, $productId, $quantity, $deliveryTypeId);

        $this->assertInstanceOf(ResponseDto::class, $responseDto);
        $this->assertEquals(ResponseDto::class, get_class($responseDto));
        $this->assertEquals($responseData['data'], $responseDto->getData());
    }

    public function testRemoveProductFromBasket()
    {
        $token = 'some_token';
        $productId = 1;

        $responseData = $this->responseData();

        $response = PsrFactory::createResponse(200, $responseData);

        $clientMock = Mockery::mock(HttpClient::class)
            ->shouldReceive('sendRequest')
            ->withArgs(
                $this->assertRequest(HttpMethod::HTTP_DELETE, "baskets/some_token/products/1")
            )
            ->andReturn($response)
            ->getMock();

        $wrapper = new BasketWrapper(
            ConfigFactory::createConfig(),
            $clientMock
        );

        $responseDto = $wrapper->removeProductFromBasket($token, $productId);

        $this->assertInstanceOf(ResponseDto::class, $responseDto);
        $this->assertEquals(ResponseDto::class, get_class($responseDto));
        $this->assertEquals($responseData['data'], $responseDto->getData());
    }

    public function testGetDiscountCode()
    {
        $code = 'some_code';

        $responseData = $this->responseData();

        $response = PsrFactory::createResponse(200, $responseData);

        $clientMock = Mockery::mock(HttpClient::class)
            ->shouldReceive('sendRequest')
            ->withArgs(
                $this->assertRequest(HttpMethod::HTTP_GET, "discount-codes/some_code")
            )
            ->andReturn($response)
            ->getMock();

        $wrapper = new BasketWrapper(
            ConfigFactory::createConfig(),
            $clientMock
        );

        $responseDto = $wrapper->getDiscountCode($code);

        $this->assertInstanceOf(ResponseDto::class, $responseDto);
        $this->assertEquals(ResponseDto::class, get_class($responseDto));
        $this->assertEquals($responseData['data'], $responseDto->getData());
    }

    public function testApplyDiscountToBasket()
    {
        $token = 'some_token';
        $discountCodeId = 2;

        $responseData = $this->responseData();

        $response = PsrFactory::createResponse(200, $responseData);

        $clientMock = Mockery::mock(HttpClient::class)
            ->shouldReceive('sendRequest')
            ->withArgs(
                $this->assertRequest(HttpMethod::HTTP_PUT, "baskets/some_token/discount-codes/2")
            )
            ->andReturn($response)
            ->getMock();

        $wrapper = new BasketWrapper(
            ConfigFactory::createConfig(),
            $clientMock
        );

        $responseDto = $wrapper->applyDiscountToBasket($token, $discountCodeId);

        $this->assertInstanceOf(ResponseDto::class, $responseDto);
        $this->assertEquals(ResponseDto::class, get_class($responseDto));
        $this->assertEquals($responseData['data'], $responseDto->getData());
    }

    public function testRemoveDiscountCodesFromBasket()
    {
        $token = 'some_token';

        $responseData = $this->responseData();

        $response = PsrFactory::createResponse(200, $responseData);

        $clientMock = Mockery::mock(HttpClient::class)
            ->shouldReceive('sendRequest')
            ->withArgs(
                $this->assertRequest(HttpMethod::HTTP_DELETE, "baskets/some_token/discount-codes")
            )
            ->andReturn($response)
            ->getMock();

        $wrapper = new BasketWrapper(
            ConfigFactory::createConfig(),
            $clientMock
        );

        $responseDto = $wrapper->removeDiscountCodesFromBasket($token);

        $this->assertInstanceOf(ResponseDto::class, $responseDto);
        $this->assertEquals(ResponseDto::class, get_class($responseDto));
        $this->assertEquals($responseData['data'], $responseDto->getData());
    }

    public function testApplyDeliveryToBasket()
    {
        $token = 'some_token';
        $deliveryTypeId = 1;

        $responseData = $this->responseData();

        $response = PsrFactory::createResponse(200, $responseData);

        $clientMock = Mockery::mock(HttpClient::class)
            ->shouldReceive('sendRequest')
            ->withArgs(
                $this->assertRequest(HttpMethod::HTTP_PUT, "baskets/some_token/delivery-types/1")
            )
            ->andReturn($response)
            ->getMock();

        $wrapper = new BasketWrapper(
            ConfigFactory::createConfig(),
            $clientMock
        );

        $responseDto = $wrapper->applyDeliveryToBasket($token, $deliveryTypeId);

        $this->assertInstanceOf(ResponseDto::class, $responseDto);
        $this->assertEquals(ResponseDto::class, get_class($responseDto));
        $this->assertEquals($responseData['data'], $responseDto->getData());
    }

    /**
     * @return array
     */
    protected function responseData()
    {
        return [
            'data' => [
                'some' => 'data'
            ]
        ];
    }

    /**
     * @return array
     */
    protected function paginatedResponseData()
    {
        return [
            'data' => [
                [
                    'some' => 'data'
                ],
                [
                    'some' => 'data'
                ]
            ]
        ];
    }
}