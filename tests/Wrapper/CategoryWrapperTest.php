<?php

namespace Sugarcoat\Tests\Wrapper;

use Mockery;
use Sugarcoat\APIWrapper\Client\HttpClient;
use Sugarcoat\APIWrapper\Constant\HttpMethod;
use Sugarcoat\APIWrapper\Dto\FilterCollectionDto;
use Sugarcoat\APIWrapper\Dto\FilterDto;
use Sugarcoat\APIWrapper\Dto\PaginatedResponseDto;
use Sugarcoat\APIWrapper\Dto\PaginationDto;
use Sugarcoat\APIWrapper\Dto\ResponseDto;
use Sugarcoat\APIWrapper\Dto\SortCollectionDto;
use Sugarcoat\APIWrapper\Dto\SortDto;
use Sugarcoat\APIWrapper\Wrapper\CategoryWrapper;
use Sugarcoat\Tests\BaseTest;
use Sugarcoat\Tests\Support\ConfigFactory;
use Sugarcoat\Tests\Support\PsrFactory;

class CategoryWrapperTest extends BaseTest
{
    public function testGetCategories()
    {
        $responseData = $this->paginatedResponseData();

        $paginationDto =  new PaginationDto(2, 10);
        $filterCollectionDto = new FilterCollectionDto([
            new FilterDto('property', 'value')
        ]);
        $sortCollectionDto = new SortCollectionDto([
            new SortDto('property', 'asc')
        ]);

        $requestData = array_merge(
            $paginationDto->toArray(),
            $filterCollectionDto->toArray(),
            $sortCollectionDto->toArray()
        );

        $response = PsrFactory::createResponse(200, $responseData);

        $clientMock = Mockery::mock(HttpClient::class)
            ->shouldReceive('sendRequest')
            ->withArgs(
                $this->assertRequest(HttpMethod::HTTP_GET, "categories", $requestData)
            )
            ->andReturn($response)
            ->getMock();

        $wrapper = new CategoryWrapper(
            ConfigFactory::createConfig(),
            $clientMock
        );

        $responseDto = $wrapper->getCategories($paginationDto, $filterCollectionDto, $sortCollectionDto);

        $this->assertInstanceOf(PaginatedResponseDto::class, $responseDto);
        $this->assertEquals(PaginatedResponseDto::class, get_class($responseDto));
        $this->assertEquals($responseData['data'], $responseDto->getData());
    }

    public function testGetCategoryById()
    {
        $id = 2;

        $responseData = $this->responseData();

        $response = PsrFactory::createResponse(200, $responseData);

        $clientMock = Mockery::mock(HttpClient::class)
            ->shouldReceive('sendRequest')
            ->withArgs(
                $this->assertRequest(HttpMethod::HTTP_GET, "categories/2")
            )
            ->andReturn($response)
            ->getMock();

        $wrapper = new CategoryWrapper(
            ConfigFactory::createConfig(),
            $clientMock
        );

        $responseDto = $wrapper->getCategoryById($id);

        $this->assertInstanceOf(ResponseDto::class, $responseDto);
        $this->assertEquals(ResponseDto::class, get_class($responseDto));
        $this->assertEquals($responseData['data'], $responseDto->getData());
    }

    public function testGetCategoryBySlug()
    {
        $slug = 'some_slug';

        $responseData = $this->paginatedResponseData();

        $response = PsrFactory::createResponse(200, $responseData);

        $clientMock = Mockery::mock(HttpClient::class)
            ->shouldReceive('sendRequest')
            ->withArgs(
                $this->assertRequest(HttpMethod::HTTP_GET, "categories", [
                    'filters' => [
                        [
                            'property' => 'slug',
                            'value' => $slug,
                            'operator' => '='
                        ]
                    ],
                    'include' => '*'
                ])
            )
            ->andReturn($response)
            ->getMock();

        $wrapper = new CategoryWrapper(
            ConfigFactory::createConfig(),
            $clientMock
        );

        $responseDto = $wrapper->getCategoryBySlug($slug);

        $this->assertInstanceOf(ResponseDto::class, $responseDto);
        $this->assertEquals(ResponseDto::class, get_class($responseDto));
        $this->assertEquals($responseData['data'][0], $responseDto->getData());
    }

    public function testGetCategoryOptions()
    {
        $responseData = $this->paginatedResponseData();

        $paginationDto =  new PaginationDto(2, 10);
        $filterCollectionDto = new FilterCollectionDto([
            new FilterDto('property', 'value')
        ]);
        $sortCollectionDto = new SortCollectionDto([
            new SortDto('property', 'asc')
        ]);

        $requestData = array_merge(
            $paginationDto->toArray(),
            $filterCollectionDto->toArray(),
            $sortCollectionDto->toArray()
        );

        $response = PsrFactory::createResponse(200, $responseData);

        $clientMock = Mockery::mock(HttpClient::class)
            ->shouldReceive('sendRequest')
            ->withArgs(
                $this->assertRequest(HttpMethod::HTTP_GET, "category-options", $requestData)
            )
            ->andReturn($response)
            ->getMock();

        $wrapper = new CategoryWrapper(
            ConfigFactory::createConfig(),
            $clientMock
        );

        $responseDto = $wrapper->getCategoryOptions($paginationDto, $filterCollectionDto, $sortCollectionDto);

        $this->assertInstanceOf(PaginatedResponseDto::class, $responseDto);
        $this->assertEquals(PaginatedResponseDto::class, get_class($responseDto));
        $this->assertEquals($responseData['data'], $responseDto->getData());
    }

    public function testGetCategoryOptionById()
    {
        $id = 2;

        $responseData = $this->responseData();

        $response = PsrFactory::createResponse(200, $responseData);

        $clientMock = Mockery::mock(HttpClient::class)
            ->shouldReceive('sendRequest')
            ->withArgs(
                $this->assertRequest(HttpMethod::HTTP_GET, "category-options/2")
            )
            ->andReturn($response)
            ->getMock();

        $wrapper = new CategoryWrapper(
            ConfigFactory::createConfig(),
            $clientMock
        );

        $responseDto = $wrapper->getCategoryOptionById($id);

        $this->assertInstanceOf(ResponseDto::class, $responseDto);
        $this->assertEquals(ResponseDto::class, get_class($responseDto));
        $this->assertEquals($responseData['data'], $responseDto->getData());
    }

    public function testGetCategoryOptionBySlug()
    {
        $slug = 'some_slug';

        $responseData = $this->paginatedResponseData();

        $response = PsrFactory::createResponse(200, $responseData);

        $clientMock = Mockery::mock(HttpClient::class)
            ->shouldReceive('sendRequest')
            ->withArgs(
                $this->assertRequest(HttpMethod::HTTP_GET, "category-options", [
                    'filters' => [
                        [
                            'property' => 'slug',
                            'value' => $slug,
                            'operator' => '='
                        ]
                    ],
                    'include' => '*'
                ])
            )
            ->andReturn($response)
            ->getMock();

        $wrapper = new CategoryWrapper(
            ConfigFactory::createConfig(),
            $clientMock
        );

        $responseDto = $wrapper->getCategoryOptionBySlug($slug);

        $this->assertInstanceOf(ResponseDto::class, $responseDto);
        $this->assertEquals(ResponseDto::class, get_class($responseDto));
        $this->assertEquals($responseData['data'][0], $responseDto->getData());
    }

    /**
     * @return array
     */
    protected function responseData()
    {
        return [
            'data' => [
                'some' => 'data'
            ]
        ];
    }

    /**
     * @return array
     */
    protected function paginatedResponseData()
    {
        return [
            'data' => [
                [
                    'some' => 'data'
                ],
                [
                    'some' => 'data'
                ]
            ]
        ];
    }
}