<?php

namespace Sugarcoat\Tests\Exception;

use GuzzleHttp\Psr7\Response;
use Sugarcoat\APIWrapper\Exception\ErrorResponseException;
use Sugarcoat\Tests\BaseTest;

class ErrorResponseExceptionTest extends BaseTest
{
    public function testConstructor()
    {
        $errors = [
           'type' => 'user_unauthorised',
            'code' => 401,
            'message' => 'Unauthorised'
        ];
        $previousException = new \Exception('Bad things');
        $response = new Response(401, [], json_encode($errors));

        $exception = new ErrorResponseException($response, $errors, 'Bad things', 1, $previousException);

        $this->assertEquals($response, $exception->getResponse());
        $this->assertSame($errors, $exception->getErrors());
        $this->assertEquals('Bad things', $exception->getMessage());
        $this->assertEquals(1, $exception->getCode());
        $this->assertEquals($previousException, $exception->getPrevious());
    }
}