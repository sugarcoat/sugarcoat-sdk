<?php

namespace Sugarcoat\Tests\Exception;

use GuzzleHttp\Psr7\Response;
use Sugarcoat\APIWrapper\Exception\ServiceErrorResponseException;
use Sugarcoat\Tests\BaseTest;

class ServiceErrorResponseExceptionTest extends BaseTest
{
    public function testConstructor()
    {
        $errors = [
           'type' => 'user_unauthorised',
            'code' => 401,
            'message' => 'Unauthorised'
        ];
        $previousException = new \Exception('Bad things');
        $response = new Response(401, [], json_encode($errors));

        $exception = new ServiceErrorResponseException($response, 'Bad things', 1, $previousException);

        $this->assertEquals($response, $exception->getResponse());
        $this->assertEquals('Bad things', $exception->getMessage());
        $this->assertEquals(1, $exception->getCode());
        $this->assertEquals($previousException, $exception->getPrevious());
    }
}