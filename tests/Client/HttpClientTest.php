<?php

namespace Sugarcoat\Tests\Config;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use Mockery;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Sugarcoat\APIWrapper\Client\HttpClient;
use Sugarcoat\APIWrapper\Constant\HttpMethod;
use Sugarcoat\APIWrapper\Exception\ErrorResponseException;
use Sugarcoat\APIWrapper\Exception\ServiceErrorResponseException;
use Sugarcoat\Tests\BaseTest;
use Sugarcoat\Tests\Support\ConfigFactory;
use Sugarcoat\Tests\Support\PsrFactory;

class HttpClientTest extends BaseTest
{
    public function testSendGetMocked()
    {
        $config = ConfigFactory::createConfig();

        $responseData = [
            'something' => 'else'
        ];

        $requestData = [
            'test' => 'data'
        ];

        $serverRequest = PsrFactory::createRequest(HttpMethod::HTTP_GET, 'test', $requestData, [
            'Authorization' => '12345'
        ]);

        $response = PsrFactory::createResponse(200, $responseData);

        $guzzleClient = Mockery::mock(Client::class)
            ->shouldReceive('send')
            ->withArgs(function ($request, $args) use ($serverRequest) {
                /** @var ServerRequestInterface $request */
                $this->assertInstanceOf(ServerRequestInterface::class, $request);
                $this->assertEquals(['Authorization' => ['12345']], $request->getHeaders());
                $this->assertEquals($serverRequest->getMethod(), $request->getMethod());
                $this->assertEquals($serverRequest->getUri()->getPath(), $request->getUri()->getPath());
                $this->assertEquals(['query' => $serverRequest->getServerParams()], $args);
                return true;
            })
            ->andReturn($response)
            ->getMock();

        $client = new HttpClient($config, [], $guzzleClient);

        $psrResponse = $client->sendRequest($serverRequest);

        $this->assertInstanceOf(ResponseInterface::class, $psrResponse);
        $this->assertEquals($response->getStatusCode(), $psrResponse->getStatusCode());
        $this->assertEquals($response->getHeaders(), $psrResponse->getHeaders());
        $this->assertEquals($responseData, json_decode($psrResponse->getBody()->getContents(), true));
    }

    public function testSendPostMocked()
    {
        $config = ConfigFactory::createConfig();

        $responseData = [
            'something' => 'else'
        ];

        $requestData = [
            'test' => 'data'
        ];

        $serverRequest = PsrFactory::createRequest(HttpMethod::HTTP_POST, 'test', $requestData, [
            'Authorization' => '12345'
        ]);
        $response = PsrFactory::createResponse(200, $responseData);

        $guzzleClient = Mockery::mock(Client::class)
            ->shouldReceive('send')
            ->withArgs(function ($request, $args) use ($serverRequest) {
                /** @var ServerRequestInterface $request */
                $this->assertInstanceOf(ServerRequestInterface::class, $request);
                $this->assertEquals(['Authorization' => ['12345']], $request->getHeaders());
                $this->assertEquals($serverRequest->getMethod(), $request->getMethod());
                $this->assertEquals($serverRequest->getUri()->getPath(), $request->getUri()->getPath());
                $this->assertEquals(['json' => $serverRequest->getServerParams()], $args);
                return true;
            })
            ->andReturn($response)
            ->getMock();

        $client = new HttpClient($config, [], $guzzleClient);

        $psrResponse = $client->sendRequest($serverRequest);

        $this->assertInstanceOf(ResponseInterface::class, $psrResponse);
        $this->assertEquals($response->getStatusCode(), $psrResponse->getStatusCode());
        $this->assertEquals($response->getHeaders(), $psrResponse->getHeaders());
        $this->assertEquals($responseData, json_decode($psrResponse->getBody()->getContents(), true));
    }

    public function testSendPostRequestSuccess()
    {
        $config = ConfigFactory::createConfig();

        $responseData = [
            'something' => 'else'
        ];

        $requestData = [
            'test' => 'data'
        ];

        $mock = new MockHandler([
            PsrFactory::createResponse(200, $responseData, ['Content-Length' => 0])
        ]);

        $handler = HandlerStack::create($mock);
        $client = new HttpClient($config, ['handler' => $handler]);

        $request = PsrFactory::createRequest(HttpMethod::HTTP_POST, 'test', $requestData);

        $response = $client->sendRequest($request);

        $this->assertInstanceOf(ResponseInterface::class, $response);
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertEquals(['Content-Length' => [0]], $response->getHeaders());
        $this->assertEquals(json_encode($responseData), (string)$response->getBody());
    }

    public function testSendPostRequestError()
    {
        $this->expectException(ErrorResponseException::class);

        $config = ConfigFactory::createConfig();

        $responseData = [
            'error' => 'error'
        ];

        $mock = new MockHandler([
            PsrFactory::createResponse(401, $responseData, ['Content-Length' => 0])
        ]);

        $handler = HandlerStack::create($mock);
        $client = new HttpClient($config, ['handler' => $handler]);

        $request = PsrFactory::createRequest(HttpMethod::HTTP_POST, 'test');

        $client->sendRequest($request);
    }

    public function testSendWithException()
    {
        $this->expectException(ServiceErrorResponseException::class);
        $this->expectExceptionMessage('Error Communicating with Server');

        $config = ConfigFactory::createConfig();

        $mock = new MockHandler([
            new RequestException("Error Communicating with Server", PsrFactory::createRequest('GET', 'test'))
        ]);

        $handler = HandlerStack::create($mock);
        $client = new HttpClient($config, ['handler' => $handler]);

        $request = PsrFactory::createRequest(HttpMethod::HTTP_DELETE, 'test');

        $client->sendRequest($request);
    }

    public function testSendGetRequestWithRateLimiting()
    {
        $requestData = [
            'test' => 'data'
        ];

        $responseData = [
            'something' => 'else'
        ];

        $response = PsrFactory::createResponse(200, $responseData);

        $config = ConfigFactory::createConfig();

        $serverRequest = PsrFactory::createRequest(HttpMethod::HTTP_GET, 'test', $requestData, [
            'Authorization' => '12345'
        ]);

        $mock = new MockHandler([
            new ClientException(
                "Too Many Requests",
                $serverRequest,
                PsrFactory::createResponse(429, $requestData, ['Retry-After' => 1])
            ),
            $response
        ]);

        $handler = HandlerStack::create($mock);
        $client = new HttpClient($config, ['handler' => $handler]);

        $psrResponse = $client->sendRequest($serverRequest);

        $this->assertInstanceOf(ResponseInterface::class, $psrResponse);
        $this->assertEquals($response->getStatusCode(), $psrResponse->getStatusCode());
        $this->assertEquals($response->getHeaders(), $psrResponse->getHeaders());
        $this->assertEquals($responseData, json_decode($psrResponse->getBody()->getContents(), true));
    }

    public function testSendPostRequestWithRateLimiting()
    {
        $requestData = [
            'test' => 'data'
        ];

        $responseData = [
            'something' => 'else'
        ];

        $response = PsrFactory::createResponse(200, $responseData);

        $config = ConfigFactory::createConfig();

        $serverRequest = PsrFactory::createRequest(HttpMethod::HTTP_POST, 'test', $requestData, [
            'Authorization' => '12345'
        ]);

        $mock = new MockHandler([
            new ClientException(
                "Too Many Requests",
                $serverRequest,
                PsrFactory::createResponse(429, $requestData, ['Retry-After' => 1])
            ),
            $response
        ]);

        $handler = HandlerStack::create($mock);
        $client = new HttpClient($config, ['handler' => $handler]);

        $psrResponse = $client->sendRequest($serverRequest);

        $this->assertInstanceOf(ResponseInterface::class, $psrResponse);
        $this->assertEquals($response->getStatusCode(), $psrResponse->getStatusCode());
        $this->assertEquals($response->getHeaders(), $psrResponse->getHeaders());
        $this->assertEquals($responseData, json_decode($psrResponse->getBody()->getContents(), true));
    }
}