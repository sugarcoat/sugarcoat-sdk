<?php

namespace Sugarcoat\Tests;

use Mockery;
use Sugarcoat\APIWrapper\ApiWrapper;
use Sugarcoat\APIWrapper\Client\ClientInterface;
use Sugarcoat\APIWrapper\Dto\FilterCollectionDto;
use Sugarcoat\APIWrapper\Dto\FilterDto;
use Sugarcoat\APIWrapper\Dto\PaginatedResponseDto;
use Sugarcoat\APIWrapper\Dto\PaginationDto;
use Sugarcoat\APIWrapper\Dto\ResponseDto;
use Sugarcoat\APIWrapper\Dto\SortCollectionDto;
use Sugarcoat\APIWrapper\Dto\SortDto;
use Sugarcoat\APIWrapper\Wrapper\AnalyticWrapper;
use Sugarcoat\APIWrapper\Wrapper\AuthWrapper;
use Sugarcoat\APIWrapper\Wrapper\BasketWrapper;
use Sugarcoat\APIWrapper\Wrapper\CategoryWrapper;
use Sugarcoat\APIWrapper\Wrapper\CountryWrapper;
use Sugarcoat\APIWrapper\Wrapper\CustomerWrapper;
use Sugarcoat\APIWrapper\Wrapper\OrderWrapper;
use Sugarcoat\APIWrapper\Wrapper\PageWrapper;
use Sugarcoat\APIWrapper\Wrapper\PartnerWrapper;
use Sugarcoat\APIWrapper\Wrapper\ProductGroupWrapper;
use Sugarcoat\APIWrapper\Wrapper\ProductTypeWrapper;
use Sugarcoat\APIWrapper\Wrapper\ProductWrapper;
use Sugarcoat\APIWrapper\Wrapper\StoreWrapper;
use Sugarcoat\APIWrapper\Wrapper\TagWrapper;
use Sugarcoat\APIWrapper\Wrapper\TermWrapper;
use Sugarcoat\APIWrapper\Wrapper\UserWrapper;
use Sugarcoat\APIWrapper\Wrapper\SiteWrapper;
use Sugarcoat\Tests\Support\ConfigFactory;

class ApiWrapperTest extends BaseTest
{
    public function testGetToken()
    {
        $token = 'some_token';

        $responseData = [
            'some' => 'response'
        ];

        $response = new ResponseDto([], $responseData);

        $wrapper = Mockery::mock(AuthWrapper::class)
            ->shouldReceive('getToken')
            ->withArgs([$token])
            ->andReturn($response)
            ->getMock();

        $facade = $this->apiFacadeFactory(AuthWrapper::class, $wrapper);

        $responseDto = $facade->getToken($token);

        $this->assertInstanceOf(ResponseDto::class, $responseDto);
        $this->assertEquals(ResponseDto::class, get_class($responseDto));
        $this->assertEquals($responseData, $responseDto->getData());
    }

    public function testCreateUserToken()
    {
        $email = 'some_email';
        $password = 'some_password';

        $responseData = [
            'some' => 'response'
        ];

        $response = new ResponseDto([], $responseData);

        $wrapper = Mockery::mock(AuthWrapper::class)
            ->shouldReceive('createUserToken')
            ->withArgs([$email, $password])
            ->andReturn($response)
            ->getMock();

        $facade = $this->apiFacadeFactory(AuthWrapper::class, $wrapper);

        $responseDto = $facade->createUserToken($email, $password);

        $this->assertInstanceOf(ResponseDto::class, $responseDto);
        $this->assertEquals(ResponseDto::class, get_class($responseDto));
        $this->assertEquals($responseData, $responseDto->getData());
    }


    public function testGetTokenByToken()
    {
        $token = 'some_token';

        $responseData = [
            'some' => 'response'
        ];

        $response = new ResponseDto([], $responseData);

        $wrapper = Mockery::mock(AuthWrapper::class)
            ->shouldReceive('getTokenByToken')
            ->withArgs([$token])
            ->andReturn($response)
            ->getMock();

        $facade = $this->apiFacadeFactory(AuthWrapper::class, $wrapper);

        $responseDto = $facade->getTokenByToken($token);

        $this->assertInstanceOf(ResponseDto::class, $responseDto);
        $this->assertEquals(ResponseDto::class, get_class($responseDto));
        $this->assertEquals($responseData, $responseDto->getData());
    }

    public function testGetStore()
    {
        $responseData = [
            'some' => 'response'
        ];

        $response = new ResponseDto([], $responseData);

        $wrapper = Mockery::mock(StoreWrapper::class)
            ->shouldReceive('getStore')
            ->andReturn($response)
            ->getMock();

        $facade = $this->apiFacadeFactory(StoreWrapper::class, $wrapper);

        $responseDto = $facade->getStore();

        $this->assertInstanceOf(ResponseDto::class, $responseDto);
        $this->assertEquals(ResponseDto::class, get_class($responseDto));
        $this->assertEquals($responseData, $responseDto->getData());
    }

    /**
     * @group focus
     */
    public function testGetStorePaymentGateways()
    {
        $responseData = [
            'some' => 'response'
        ];

        $paginationDto = new PaginationDto(1, 2);
        $filterCollectionDto = new FilterCollectionDto([
            new FilterDto('property', 'value')
        ]);
        $sortCollectionDto = new SortCollectionDto([
            new SortDto('property', 'asc')
        ]);

        $response = new PaginatedResponseDto([], $responseData, 1, 2, 3,4);

        $wrapper = Mockery::mock(StoreWrapper::class)
            ->shouldReceive('getStorePaymentMethods')
            ->withArgs([$paginationDto, $filterCollectionDto, $sortCollectionDto])
            ->andReturn($response)
            ->getMock();

        $facade = $this->apiFacadeFactory(StoreWrapper::class, $wrapper);

        $responseDto = $facade->getStorePaymentMethods($paginationDto, $filterCollectionDto, $sortCollectionDto);

        $this->assertInstanceOf(PaginatedResponseDto::class, $responseDto);
        $this->assertEquals(PaginatedResponseDto::class, get_class($responseDto));
        $this->assertEquals($responseData, $responseDto->getData());
    }

    public function testGetProducts()
    {
        $responseData = [
            'some' => 'response'
        ];

        $paginationDto = new PaginationDto(1, 2);
        $filterCollectionDto = new FilterCollectionDto([
            new FilterDto('property', 'value')
        ]);
        $sortCollectionDto = new SortCollectionDto([
            new SortDto('property', 'asc')
        ]);

        $response = new PaginatedResponseDto([], $responseData, 1, 2, 3,4);

        $wrapper = Mockery::mock(ProductWrapper::class)
            ->shouldReceive('getProducts')
            ->withArgs([$paginationDto, $filterCollectionDto, $sortCollectionDto])
            ->andReturn($response)
            ->getMock();

        $facade = $this->apiFacadeFactory(ProductWrapper::class, $wrapper);

        $responseDto = $facade->getProducts($paginationDto, $filterCollectionDto, $sortCollectionDto);

        $this->assertInstanceOf(PaginatedResponseDto::class, $responseDto);
        $this->assertEquals(PaginatedResponseDto::class, get_class($responseDto));
        $this->assertEquals($responseData, $responseDto->getData());
    }


    public function testGetProductById()
    {
        $id = 1;

        $responseData = [
            'some' => 'response'
        ];

        $response = new ResponseDto([], $responseData);

        $wrapper = Mockery::mock(ProductWrapper::class)
            ->shouldReceive('getProductById')
            ->withArgs([$id])
            ->andReturn($response)
            ->getMock();

        $facade = $this->apiFacadeFactory(ProductWrapper::class, $wrapper);

        $responseDto = $facade->getProductById($id);

        $this->assertInstanceOf(ResponseDto::class, $responseDto);
        $this->assertEquals(ResponseDto::class, get_class($responseDto));
        $this->assertEquals($responseData, $responseDto->getData());
    }


    public function testGetProductByName()
    {
        $name = 'some_name';

        $responseData = [
            'some' => 'response'
        ];

        $response = new PaginatedResponseDto([], $responseData, 1, 2, 3,4);

        $wrapper = Mockery::mock(ProductWrapper::class)
            ->shouldReceive('getProductByName')
            ->withArgs([$name])
            ->andReturn($response)
            ->getMock();

        $facade = $this->apiFacadeFactory(ProductWrapper::class, $wrapper);

        $responseDto = $facade->getProductByName($name);

        $this->assertInstanceOf(PaginatedResponseDto::class, $responseDto);
        $this->assertEquals(PaginatedResponseDto::class, get_class($responseDto));
        $this->assertEquals($responseData, $responseDto->getData());
    }

    public function testGetProductBySlug()
    {
        $slug = 'some_slug';

        $responseData = [
            'some' => 'response'
        ];

        $response = new ResponseDto([], $responseData);

        $wrapper = Mockery::mock(ProductWrapper::class)
            ->shouldReceive('getProductBySlug')
            ->withArgs([$slug])
            ->andReturn($response)
            ->getMock();

        $facade = $this->apiFacadeFactory(ProductWrapper::class, $wrapper);

        $responseDto = $facade->getProductBySlug($slug);

        $this->assertInstanceOf(ResponseDto::class, $responseDto);
        $this->assertEquals(ResponseDto::class, get_class($responseDto));
        $this->assertEquals($responseData, $responseDto->getData());
    }


    public function testGetProductsByTagIds()
    {
        $ids = [1, 2, 3];

        $responseData = [
            'some' => 'response'
        ];

        $response = new PaginatedResponseDto([], $responseData, 1, 2, 3,4);

        $wrapper = Mockery::mock(ProductWrapper::class)
            ->shouldReceive('getProductsByTagIds')
            ->withArgs([$ids])
            ->andReturn($response)
            ->getMock();

        $facade = $this->apiFacadeFactory(ProductWrapper::class, $wrapper);

        $responseDto = $facade->getProductsByTagIds($ids);

        $this->assertInstanceOf(PaginatedResponseDto::class, $responseDto);
        $this->assertEquals(PaginatedResponseDto::class, get_class($responseDto));
        $this->assertEquals($responseData, $responseDto->getData());
    }


    public function testGetProductsByIds()
    {
        $ids = [1, 2, 3];

        $responseData = [
            'some' => 'response'
        ];

        $response = new PaginatedResponseDto([], $responseData, 1, 2, 3,4);

        $wrapper = Mockery::mock(ProductWrapper::class)
            ->shouldReceive('getProductsByIds')
            ->withArgs([$ids])
            ->andReturn($response)
            ->getMock();

        $facade = $this->apiFacadeFactory(ProductWrapper::class, $wrapper);

        $responseDto = $facade->getProductsByIds($ids);

        $this->assertInstanceOf(PaginatedResponseDto::class, $responseDto);
        $this->assertEquals(PaginatedResponseDto::class, get_class($responseDto));
        $this->assertEquals($responseData, $responseDto->getData());
    }


    public function testSearchProducts()
    {
        $searchTerm = 'some_search_term';

        $responseData = [
            'some' => 'response'
        ];

        $response = new PaginatedResponseDto([], $responseData, 1, 2, 3,4);

        $wrapper = Mockery::mock(ProductWrapper::class)
            ->shouldReceive('searchProducts')
            ->withArgs([$searchTerm])
            ->andReturn($response)
            ->getMock();

        $facade = $this->apiFacadeFactory(ProductWrapper::class, $wrapper);

        $responseDto = $facade->searchProducts($searchTerm);

        $this->assertInstanceOf(PaginatedResponseDto::class, $responseDto);
        $this->assertEquals(PaginatedResponseDto::class, get_class($responseDto));
        $this->assertEquals($responseData, $responseDto->getData());
    }

    public function testGetRecommendedProducts()
    {
        $ids = [1, 2];
        $type = 'some_type';

        $responseData = [
            'some' => 'response'
        ];

        $paginationDto = new PaginationDto(1, 2);
        $filterCollectionDto = new FilterCollectionDto([
            new FilterDto('property', 'value')
        ]);
        $sortCollectionDto = new SortCollectionDto([
            new SortDto('property', 'asc')
        ]);

        $response = new PaginatedResponseDto([], $responseData, 1, 2, 3,4);

        $wrapper = Mockery::mock(ProductWrapper::class)
            ->shouldReceive('getRecommendedProducts')
            ->withArgs([$ids, $type, $paginationDto, $filterCollectionDto, $sortCollectionDto])
            ->andReturn($response)
            ->getMock();

        $facade = $this->apiFacadeFactory(ProductWrapper::class, $wrapper);

        $responseDto = $facade->getRecommendedProducts($ids, $type, $paginationDto, $filterCollectionDto, $sortCollectionDto);

        $this->assertInstanceOf(PaginatedResponseDto::class, $responseDto);
        $this->assertEquals(PaginatedResponseDto::class, get_class($responseDto));
        $this->assertEquals($responseData, $responseDto->getData());
    }

    public function testGetProductTypes()
    {
        $responseData = [
            'some' => 'response'
        ];

        $paginationDto = new PaginationDto(1, 2);
        $filterCollectionDto = new FilterCollectionDto([
            new FilterDto('property', 'value')
        ]);
        $sortCollectionDto = new SortCollectionDto([
            new SortDto('property', 'asc')
        ]);

        $response = new PaginatedResponseDto([], $responseData, 1, 2, 3,4);

        $wrapper = Mockery::mock(ProductWrapper::class)
            ->shouldReceive('getProductTypes')
            ->withArgs([$paginationDto, $filterCollectionDto, $sortCollectionDto])
            ->andReturn($response)
            ->getMock();

        $facade = $this->apiFacadeFactory(ProductTypeWrapper::class, $wrapper);

        $responseDto = $facade->getProductTypes($paginationDto, $filterCollectionDto, $sortCollectionDto);

        $this->assertInstanceOf(PaginatedResponseDto::class, $responseDto);
        $this->assertEquals(PaginatedResponseDto::class, get_class($responseDto));
        $this->assertEquals($responseData, $responseDto->getData());
    }

    public function testGetProductTypeById()
    {
        $id = 1;

        $responseData = [
            'some' => 'response'
        ];

        $response = new ResponseDto([], $responseData);

        $wrapper = Mockery::mock(ProductTypeWrapper::class)
            ->shouldReceive('getProductTypeById')
            ->withArgs([$id])
            ->andReturn($response)
            ->getMock();

        $facade = $this->apiFacadeFactory(ProductTypeWrapper::class, $wrapper);

        $responseDto = $facade->getProductTypeById($id);

        $this->assertInstanceOf(ResponseDto::class, $responseDto);
        $this->assertEquals(ResponseDto::class, get_class($responseDto));
        $this->assertEquals($responseData, $responseDto->getData());
    }

    public function testGetProductTypeBySlug()
    {
        $slug = 'some_slug';

        $responseData = [
            'some' => 'response'
        ];

        $response = new ResponseDto([], $responseData);

        $wrapper = Mockery::mock(ProductWrapper::class)
            ->shouldReceive('getProductTypeBySlug')
            ->withArgs([$slug])
            ->andReturn($response)
            ->getMock();

        $facade = $this->apiFacadeFactory(ProductTypeWrapper::class, $wrapper);

        $responseDto = $facade->getProductTypeBySlug($slug);

        $this->assertInstanceOf(ResponseDto::class, $responseDto);
        $this->assertEquals(ResponseDto::class, get_class($responseDto));
        $this->assertEquals($responseData, $responseDto->getData());
    }

    public function testTrackAnalytic()
    {
        $responseData = [
            'some' => 'response'
        ];

        $requestData = [
            'some' => 'request'
        ];

        $response = new ResponseDto([], $responseData);

        $wrapper = Mockery::mock(AnalyticWrapper::class)
            ->shouldReceive('trackAnalytic')
            ->withArgs([$requestData])
            ->andReturn($response)
            ->getMock();

        $facade = $this->apiFacadeFactory(AnalyticWrapper::class, $wrapper);

        $responseDto = $facade->trackAnalytic($requestData);

        $this->assertInstanceOf(ResponseDto::class, $responseDto);
        $this->assertEquals(ResponseDto::class, get_class($responseDto));
        $this->assertEquals($responseData, $responseDto->getData());
    }


    public function testTrackAnalyticByProductId()
    {
        $productId = 1;

        $responseData = [
            'some' => 'response'
        ];

        $requestData = [
            'some' => 'request'
        ];

        $response = new ResponseDto([], $responseData);

        $wrapper = Mockery::mock(AnalyticWrapper::class)
            ->shouldReceive('trackAnalyticByProductId')
            ->withArgs([$productId, $requestData])
            ->andReturn($response)
            ->getMock();

        $facade = $this->apiFacadeFactory(AnalyticWrapper::class, $wrapper);

        $responseDto = $facade->trackAnalyticByProductId($productId, $requestData);

        $this->assertInstanceOf(ResponseDto::class, $responseDto);
        $this->assertEquals(ResponseDto::class, get_class($responseDto));
        $this->assertEquals($responseData, $responseDto->getData());
    }


    public function testTrackAnalyticByDestination()
    {
        $destination = 'some_destination';

        $responseData = [
            'some' => 'response'
        ];

        $requestData = [
            'some' => 'request'
        ];

        $response = new ResponseDto([], $responseData);

        $wrapper = Mockery::mock(AnalyticWrapper::class)
            ->shouldReceive('trackAnalyticByDestination')
            ->withArgs([$destination, $requestData])
            ->andReturn($response)
            ->getMock();

        $facade = $this->apiFacadeFactory(AnalyticWrapper::class, $wrapper);

        $responseDto = $facade->trackAnalyticByDestination($destination, $requestData);

        $this->assertInstanceOf(ResponseDto::class, $responseDto);
        $this->assertEquals(ResponseDto::class, get_class($responseDto));
        $this->assertEquals($responseData, $responseDto->getData());
    }

    public function testTrackAnalyticByType()
    {
        $type = 'some_type';

        $responseData = [
            'some' => 'response'
        ];

        $requestData = [
            'some' => 'request'
        ];

        $response = new ResponseDto([], $responseData);

        $wrapper = Mockery::mock(AnalyticWrapper::class)
            ->shouldReceive('trackAnalyticByType')
            ->withArgs([$type, $requestData])
            ->andReturn($response)
            ->getMock();

        $facade = $this->apiFacadeFactory(AnalyticWrapper::class, $wrapper);

        $responseDto = $facade->trackAnalyticByType($type, $requestData);

        $this->assertInstanceOf(ResponseDto::class, $responseDto);
        $this->assertEquals(ResponseDto::class, get_class($responseDto));
        $this->assertEquals($responseData, $responseDto->getData());
    }

    public function testGetCategories()
    {
        $responseData = [
            'some' => 'response'
        ];

        $paginationDto = new PaginationDto(1, 2);
        $filterCollectionDto = new FilterCollectionDto([
            new FilterDto('property', 'value')
        ]);
        $sortCollectionDto = new SortCollectionDto([
            new SortDto('property', 'asc')
        ]);

        $response = new PaginatedResponseDto([], $responseData, 1, 2, 3,4);

        $wrapper = Mockery::mock(CategoryWrapper::class)
            ->shouldReceive('getCategories')
            ->withArgs([$paginationDto, $filterCollectionDto, $sortCollectionDto])
            ->andReturn($response)
            ->getMock();

        $facade = $this->apiFacadeFactory(CategoryWrapper::class, $wrapper);

        $responseDto = $facade->getCategories($paginationDto, $filterCollectionDto, $sortCollectionDto);

        $this->assertInstanceOf(PaginatedResponseDto::class, $responseDto);
        $this->assertEquals(PaginatedResponseDto::class, get_class($responseDto));
        $this->assertEquals($responseData, $responseDto->getData());
    }


    public function testGetCategoryById()
    {
        $id = 1;

        $responseData = [
            'some' => 'response'
        ];

        $response = new ResponseDto([], $responseData);

        $wrapper = Mockery::mock(CategoryWrapper::class)
            ->shouldReceive('getCategoryById')
            ->withArgs([$id])
            ->andReturn($response)
            ->getMock();

        $facade = $this->apiFacadeFactory(CategoryWrapper::class, $wrapper);

        $responseDto = $facade->getCategoryById($id);

        $this->assertInstanceOf(ResponseDto::class, $responseDto);
        $this->assertEquals(ResponseDto::class, get_class($responseDto));
        $this->assertEquals($responseData, $responseDto->getData());
    }

    public function testGetCategoryBySlug()
    {
        $slug = 'some_slug';

        $responseData = [
            'some' => 'response'
        ];

        $response = new ResponseDto([], $responseData);

        $wrapper = Mockery::mock(CategoryWrapper::class)
            ->shouldReceive('getCategoryBySlug')
            ->withArgs([$slug])
            ->andReturn($response)
            ->getMock();

        $facade = $this->apiFacadeFactory(CategoryWrapper::class, $wrapper);

        $responseDto = $facade->getCategoryBySlug($slug);

        $this->assertInstanceOf(ResponseDto::class, $responseDto);
        $this->assertEquals(ResponseDto::class, get_class($responseDto));
        $this->assertEquals($responseData, $responseDto->getData());
    }


    public function testGetCategoryOptions()
    {
        $responseData = [
            'some' => 'response'
        ];

        $paginationDto = new PaginationDto(1, 2);
        $filterCollectionDto = new FilterCollectionDto([
            new FilterDto('property', 'value')
        ]);
        $sortCollectionDto = new SortCollectionDto([
            new SortDto('property', 'asc')
        ]);

        $response = new PaginatedResponseDto([], $responseData, 1, 2, 3,4);

        $wrapper = Mockery::mock(CategoryWrapper::class)
            ->shouldReceive('getCategoryOptions')
            ->withArgs([$paginationDto, $filterCollectionDto, $sortCollectionDto])
            ->andReturn($response)
            ->getMock();

        $facade = $this->apiFacadeFactory(CategoryWrapper::class, $wrapper);

        $responseDto = $facade->getCategoryOptions($paginationDto, $filterCollectionDto, $sortCollectionDto);

        $this->assertInstanceOf(PaginatedResponseDto::class, $responseDto);
        $this->assertEquals(PaginatedResponseDto::class, get_class($responseDto));
        $this->assertEquals($responseData, $responseDto->getData());
    }


    public function testGetCategoryOptionById()
    {
        $id = 1;

        $responseData = [
            'some' => 'response'
        ];

        $response = new ResponseDto([], $responseData);

        $wrapper = Mockery::mock(CategoryWrapper::class)
            ->shouldReceive('getCategoryOptionById')
            ->withArgs([$id])
            ->andReturn($response)
            ->getMock();

        $facade = $this->apiFacadeFactory(CategoryWrapper::class, $wrapper);

        $responseDto = $facade->getCategoryOptionById($id);

        $this->assertInstanceOf(ResponseDto::class, $responseDto);
        $this->assertEquals(ResponseDto::class, get_class($responseDto));
        $this->assertEquals($responseData, $responseDto->getData());
    }


    public function testGetCategoryOptionBySlug()
    {
        $slug = 'some_slug';

        $responseData = [
            'some' => 'response'
        ];

        $response = new ResponseDto([], $responseData);

        $wrapper = Mockery::mock(CategoryWrapper::class)
            ->shouldReceive('getCategoryOptionBySlug')
            ->withArgs([$slug])
            ->andReturn($response)
            ->getMock();

        $facade = $this->apiFacadeFactory(CategoryWrapper::class, $wrapper);

        $responseDto = $facade->getCategoryOptionBySlug($slug);

        $this->assertInstanceOf(ResponseDto::class, $responseDto);
        $this->assertEquals(ResponseDto::class, get_class($responseDto));
        $this->assertEquals($responseData, $responseDto->getData());
    }


    public function testGetBasket()
    {
        $token = 'some_token';

        $responseData = [
            'some' => 'response'
        ];

        $response = new ResponseDto([], $responseData);

        $wrapper = Mockery::mock(BasketWrapper::class)
            ->shouldReceive('getBasket')
            ->withArgs([$token])
            ->andReturn($response)
            ->getMock();

        $facade = $this->apiFacadeFactory(BasketWrapper::class, $wrapper);

        $responseDto = $facade->getBasket($token);

        $this->assertInstanceOf(ResponseDto::class, $responseDto);
        $this->assertEquals(ResponseDto::class, get_class($responseDto));
        $this->assertEquals($responseData, $responseDto->getData());
    }


    public function testGetBasketDeliveryTypes()
    {
        $token = 'some_token';

        $responseData = [
            'some' => 'response'
        ];

        $response = new ResponseDto([], $responseData);

        $wrapper = Mockery::mock(BasketWrapper::class)
            ->shouldReceive('getBasketDeliveryTypes')
            ->withArgs([$token])
            ->andReturn($response)
            ->getMock();

        $facade = $this->apiFacadeFactory(BasketWrapper::class, $wrapper);

        $responseDto = $facade->getBasketDeliveryTypes($token);

        $this->assertInstanceOf(ResponseDto::class, $responseDto);
        $this->assertEquals(ResponseDto::class, get_class($responseDto));
        $this->assertEquals($responseData, $responseDto->getData());
    }

    public function testCreateBasket()
    {
        $userId = 2;
        $deliveryTypeId = 1;
        $customerId = 3;

        $responseData = [
            'some' => 'response'
        ];

        $requestData = [
            'some' => 'request'
        ];

        $response = new ResponseDto([], $responseData);

        $wrapper = Mockery::mock(BasketWrapper::class)
            ->shouldReceive('createBasket')
            ->withArgs([$userId, $requestData, $deliveryTypeId, $customerId])
            ->andReturn($response)
            ->getMock();

        $facade = $this->apiFacadeFactory(BasketWrapper::class, $wrapper);

        $responseDto = $facade->createBasket($userId, $requestData, $deliveryTypeId, $customerId);

        $this->assertInstanceOf(ResponseDto::class, $responseDto);
        $this->assertEquals(ResponseDto::class, get_class($responseDto));
        $this->assertEquals($responseData, $responseDto->getData());
    }

    public function testUpdateBasket()
    {
        $token = 'some_token';
        $userId = 2;
        $deliveryTypeId = 1;
        $customerId = 3;

        $responseData = [
            'some' => 'response'
        ];

        $requestData = [
            'some' => 'request'
        ];

        $response = new ResponseDto([], $responseData);

        $wrapper = Mockery::mock(BasketWrapper::class)
            ->shouldReceive('updateBasket')
            ->withArgs([$token, $userId, $requestData, $deliveryTypeId, $customerId])
            ->andReturn($response)
            ->getMock();

        $facade = $this->apiFacadeFactory(BasketWrapper::class, $wrapper);

        $responseDto = $facade->updateBasket($token, $userId, $requestData, $deliveryTypeId, $customerId);

        $this->assertInstanceOf(ResponseDto::class, $responseDto);
        $this->assertEquals(ResponseDto::class, get_class($responseDto));
        $this->assertEquals($responseData, $responseDto->getData());
    }


    public function testClearBasket()
    {
        $token = 'some_token';

        $responseData = [
            'some' => 'response'
        ];

        $response = new ResponseDto([], $responseData);

        $wrapper = Mockery::mock(BasketWrapper::class)
            ->shouldReceive('clearBasket')
            ->withArgs([$token])
            ->andReturn($response)
            ->getMock();

        $facade = $this->apiFacadeFactory(BasketWrapper::class, $wrapper);

        $responseDto = $facade->clearBasket($token);

        $this->assertInstanceOf(ResponseDto::class, $responseDto);
        $this->assertEquals(ResponseDto::class, get_class($responseDto));
        $this->assertEquals($responseData, $responseDto->getData());
    }


    public function testAddProductToBasket()
    {
        $token = 'some_token';
        $productId = 2;
        $quantity = 10;
        $deliveryTypeId = 2;

        $responseData = [
            'some' => 'response'
        ];

        $response = new ResponseDto([], $responseData);

        $wrapper = Mockery::mock(BasketWrapper::class)
            ->shouldReceive('addProductToBasket')
            ->withArgs([$token, $productId, $quantity, $deliveryTypeId])
            ->andReturn($response)
            ->getMock();

        $facade = $this->apiFacadeFactory(BasketWrapper::class, $wrapper);

        $responseDto = $facade->addProductToBasket($token, $productId, $quantity, $deliveryTypeId);

        $this->assertInstanceOf(ResponseDto::class, $responseDto);
        $this->assertEquals(ResponseDto::class, get_class($responseDto));
        $this->assertEquals($responseData, $responseDto->getData());
    }


    public function testRemoveProductFromBasket()
    {
        $token = 'some_token';
        $productId = 2;

        $responseData = [
            'some' => 'response'
        ];

        $response = new ResponseDto([], $responseData);

        $wrapper = Mockery::mock(BasketWrapper::class)
            ->shouldReceive('removeProductFromBasket')
            ->withArgs([$token, $productId])
            ->andReturn($response)
            ->getMock();

        $facade = $this->apiFacadeFactory(BasketWrapper::class, $wrapper);

        $responseDto = $facade->removeProductFromBasket($token, $productId);

        $this->assertInstanceOf(ResponseDto::class, $responseDto);
        $this->assertEquals(ResponseDto::class, get_class($responseDto));
        $this->assertEquals($responseData, $responseDto->getData());
    }


    public function testApplyDiscountToBasket()
    {
        $token = 'some_token';
        $discountCodeId = 2;

        $responseData = [
            'some' => 'response'
        ];

        $response = new ResponseDto([], $responseData);

        $wrapper = Mockery::mock(BasketWrapper::class)
            ->shouldReceive('applyDiscountToBasket')
            ->withArgs([$token, $discountCodeId])
            ->andReturn($response)
            ->getMock();

        $facade = $this->apiFacadeFactory(BasketWrapper::class, $wrapper);

        $responseDto = $facade->applyDiscountToBasket($token, $discountCodeId);

        $this->assertInstanceOf(ResponseDto::class, $responseDto);
        $this->assertEquals(ResponseDto::class, get_class($responseDto));
        $this->assertEquals($responseData, $responseDto->getData());
    }


    public function testRemoveDiscountCodesFromBasket()
    {
        $token = 'some_token';

        $responseData = [
            'some' => 'response'
        ];

        $response = new ResponseDto([], $responseData);

        $wrapper = Mockery::mock(BasketWrapper::class)
            ->shouldReceive('removeDiscountCodesFromBasket')
            ->withArgs([$token])
            ->andReturn($response)
            ->getMock();

        $facade = $this->apiFacadeFactory(BasketWrapper::class, $wrapper);

        $responseDto = $facade->removeDiscountCodesFromBasket($token);

        $this->assertInstanceOf(ResponseDto::class, $responseDto);
        $this->assertEquals(ResponseDto::class, get_class($responseDto));
        $this->assertEquals($responseData, $responseDto->getData());
    }


    public function testApplyDeliveryToBasket()
    {
        $token = 'some_token';
        $deliveryTypeId = 1;

        $responseData = [
            'some' => 'response'
        ];

        $response = new ResponseDto([], $responseData);

        $wrapper = Mockery::mock(BasketWrapper::class)
            ->shouldReceive('applyDeliveryToBasket')
            ->withArgs([$token, $deliveryTypeId])
            ->andReturn($response)
            ->getMock();

        $facade = $this->apiFacadeFactory(BasketWrapper::class, $wrapper);

        $responseDto = $facade->applyDeliveryToBasket($token, $deliveryTypeId);

        $this->assertInstanceOf(ResponseDto::class, $responseDto);
        $this->assertEquals(ResponseDto::class, get_class($responseDto));
        $this->assertEquals($responseData, $responseDto->getData());
    }

    public function testGetDiscountCode()
    {
        $discountCode = 'some_code';

        $responseData = [
            'some' => 'response'
        ];

        $response = new ResponseDto([], $responseData);

        $wrapper = Mockery::mock(BasketWrapper::class)
            ->shouldReceive('getDiscountCode')
            ->withArgs([$discountCode])
            ->andReturn($response)
            ->getMock();

        $facade = $this->apiFacadeFactory(BasketWrapper::class, $wrapper);

        $responseDto = $facade->getDiscountCode($discountCode);

        $this->assertInstanceOf(ResponseDto::class, $responseDto);
        $this->assertEquals(ResponseDto::class, get_class($responseDto));
        $this->assertEquals($responseData, $responseDto->getData());
    }


    public function testGetUserById()
    {
        $id = 1;

        $responseData = [
            'some' => 'response'
        ];

        $response = new ResponseDto([], $responseData);

        $wrapper = Mockery::mock(UserWrapper::class)
            ->shouldReceive('getUserById')
            ->withArgs([$id])
            ->andReturn($response)
            ->getMock();

        $facade = $this->apiFacadeFactory(UserWrapper::class, $wrapper);

        $responseDto = $facade->getUserById($id);

        $this->assertInstanceOf(ResponseDto::class, $responseDto);
        $this->assertEquals(ResponseDto::class, get_class($responseDto));
        $this->assertEquals($responseData, $responseDto->getData());
    }

    public function testRegisterUser()
    {
        $responseData = [
            'some' => 'response'
        ];

        $requestData = [
            'some' => 'request'
        ];

        $response = new ResponseDto([], $responseData);

        $wrapper = Mockery::mock(UserWrapper::class)
            ->shouldReceive('registerUser')
            ->withArgs([$requestData])
            ->andReturn($response)
            ->getMock();

        $facade = $this->apiFacadeFactory(UserWrapper::class, $wrapper);

        $responseDto = $facade->registerUser($requestData);

        $this->assertInstanceOf(ResponseDto::class, $responseDto);
        $this->assertEquals(ResponseDto::class, get_class($responseDto));
        $this->assertEquals($responseData, $responseDto->getData());
    }

    public function testUpdateUser()
    {
        $id = 2;

        $responseData = [
            'some' => 'response'
        ];

        $requestData = [
            'some' => 'request'
        ];

        $response = new ResponseDto([], $responseData);

        $wrapper = Mockery::mock(UserWrapper::class)
            ->shouldReceive('updateUser')
            ->withArgs([$id, $requestData])
            ->andReturn($response)
            ->getMock();

        $facade = $this->apiFacadeFactory(UserWrapper::class, $wrapper);

        $responseDto = $facade->updateUser($id, $requestData);

        $this->assertInstanceOf(ResponseDto::class, $responseDto);
        $this->assertEquals(ResponseDto::class, get_class($responseDto));
        $this->assertEquals($responseData, $responseDto->getData());
    }

    public function testActivateAccount()
    {
        $code = 'some_code';

        $responseData = [
            'some' => 'response'
        ];

        $response = new ResponseDto([], $responseData);

        $wrapper = Mockery::mock(UserWrapper::class)
            ->shouldReceive('activateAccount')
            ->withArgs([$code])
            ->andReturn($response)
            ->getMock();

        $facade = $this->apiFacadeFactory(UserWrapper::class, $wrapper);

        $responseDto = $facade->activateAccount($code);

        $this->assertInstanceOf(ResponseDto::class, $responseDto);
        $this->assertEquals(ResponseDto::class, get_class($responseDto));
        $this->assertEquals($responseData, $responseDto->getData());
    }


    public function testResendActivation()
    {
        $email = 'some_email';

        $responseData = [
            'some' => 'response'
        ];

        $response = new ResponseDto([], $responseData);

        $wrapper = Mockery::mock(UserWrapper::class)
            ->shouldReceive('resendActivation')
            ->withArgs([$email])
            ->andReturn($response)
            ->getMock();

        $facade = $this->apiFacadeFactory(UserWrapper::class, $wrapper);

        $responseDto = $facade->resendActivation($email);

        $this->assertInstanceOf(ResponseDto::class, $responseDto);
        $this->assertEquals(ResponseDto::class, get_class($responseDto));
        $this->assertEquals($responseData, $responseDto->getData());
    }


    public function testForgottenPassword()
    {
        $email = 'some_email';

        $responseData = [
            'some' => 'response'
        ];

        $response = new ResponseDto([], $responseData);

        $wrapper = Mockery::mock(UserWrapper::class)
            ->shouldReceive('forgottenPassword')
            ->withArgs([$email])
            ->andReturn($response)
            ->getMock();

        $facade = $this->apiFacadeFactory(UserWrapper::class, $wrapper);

        $responseDto = $facade->forgottenPassword($email);

        $this->assertInstanceOf(ResponseDto::class, $responseDto);
        $this->assertEquals(ResponseDto::class, get_class($responseDto));
        $this->assertEquals($responseData, $responseDto->getData());
    }


    public function testResetPassword()
    {
        $code = 'some_code';
        $password = 'some_password';

        $responseData = [
            'some' => 'response'
        ];

        $response = new ResponseDto([], $responseData);

        $wrapper = Mockery::mock(UserWrapper::class)
            ->shouldReceive('resetPassword')
            ->withArgs([$code, $password])
            ->andReturn($response)
            ->getMock();

        $facade = $this->apiFacadeFactory(UserWrapper::class, $wrapper);

        $responseDto = $facade->resetPassword($code, $password);

        $this->assertInstanceOf(ResponseDto::class, $responseDto);
        $this->assertEquals(ResponseDto::class, get_class($responseDto));
        $this->assertEquals($responseData, $responseDto->getData());
    }


    public function testLogin()
    {
        $email = 'some_email';
        $password = 'some_password';

        $responseData = [
            'some' => 'response'
        ];

        $response = new ResponseDto([], $responseData);

        $wrapper = Mockery::mock(UserWrapper::class)
            ->shouldReceive('login')
            ->withArgs([$email, $password])
            ->andReturn($response)
            ->getMock();

        $facade = $this->apiFacadeFactory(UserWrapper::class, $wrapper);

        $responseDto = $facade->login($email, $password);

        $this->assertInstanceOf(ResponseDto::class, $responseDto);
        $this->assertEquals(ResponseDto::class, get_class($responseDto));
        $this->assertEquals($responseData, $responseDto->getData());
    }

    public function testGetFavourites()
    {
        $id = 1;

        $responseData = [
            'some' => 'response'
        ];

        $response = new PaginatedResponseDto([], $responseData, 1, 2, 3,4);

        $wrapper = Mockery::mock(UserWrapper::class)
            ->shouldReceive('getFavourites')
            ->withArgs([$id])
            ->andReturn($response)
            ->getMock();

        $facade = $this->apiFacadeFactory(UserWrapper::class, $wrapper);

        $responseDto = $facade->getFavourites($id);

        $this->assertInstanceOf(PaginatedResponseDto::class, $responseDto);
        $this->assertEquals(PaginatedResponseDto::class, get_class($responseDto));
        $this->assertEquals($responseData, $responseDto->getData());
    }


    public function testAddProductToFavourites()
    {
        $userId = 1;
        $productId = 2;

        $responseData = [
            'some' => 'response'
        ];

        $response = new PaginatedResponseDto([], $responseData, 1, 2, 3,4);

        $wrapper = Mockery::mock(UserWrapper::class)
            ->shouldReceive('addProductToFavourites')
            ->withArgs([$userId, $productId])
            ->andReturn($response)
            ->getMock();

        $facade = $this->apiFacadeFactory(UserWrapper::class, $wrapper);

        $responseDto = $facade->addProductToFavourites($userId, $productId);

        $this->assertInstanceOf(PaginatedResponseDto::class, $responseDto);
        $this->assertEquals(PaginatedResponseDto::class, get_class($responseDto));
        $this->assertEquals($responseData, $responseDto->getData());
    }


    public function testRemoveProductFromFavourites()
    {
        $userId = 1;
        $productId = 2;

        $responseData = [
            'some' => 'response'
        ];

        $response = new PaginatedResponseDto([], $responseData, 1, 2, 3,4);

        $wrapper = Mockery::mock(UserWrapper::class)
            ->shouldReceive('removeProductFromFavourites')
            ->withArgs([$userId, $productId])
            ->andReturn($response)
            ->getMock();

        $facade = $this->apiFacadeFactory(UserWrapper::class, $wrapper);

        $responseDto = $facade->removeProductFromFavourites($userId, $productId);

        $this->assertInstanceOf(PaginatedResponseDto::class, $responseDto);
        $this->assertEquals(PaginatedResponseDto::class, get_class($responseDto));
        $this->assertEquals($responseData, $responseDto->getData());
    }


    public function testGetUserProductRecommendations()
    {
        $id = 1;

        $responseData = [
            'some' => 'response'
        ];

        $response = new PaginatedResponseDto([], $responseData, 1, 2, 3,4);

        $wrapper = Mockery::mock(UserWrapper::class)
            ->shouldReceive('getUserProductRecommendations')
            ->withArgs([$id])
            ->andReturn($response)
            ->getMock();

        $facade = $this->apiFacadeFactory(UserWrapper::class, $wrapper);

        $responseDto = $facade->getUserProductRecommendations($id);

        $this->assertInstanceOf(PaginatedResponseDto::class, $responseDto);
        $this->assertEquals(PaginatedResponseDto::class, get_class($responseDto));
        $this->assertEquals($responseData, $responseDto->getData());
    }


    public function testGetCustomerById()
    {
        $id = 1;

        $responseData = [
            'some' => 'response'
        ];

        $response = new ResponseDto([], $responseData);

        $wrapper = Mockery::mock(CustomerWrapper::class)
            ->shouldReceive('getCustomerById')
            ->withArgs([$id])
            ->andReturn($response)
            ->getMock();

        $facade = $this->apiFacadeFactory(CustomerWrapper::class, $wrapper);

        $responseDto = $facade->getCustomerById($id);

        $this->assertInstanceOf(ResponseDto::class, $responseDto);
        $this->assertEquals(ResponseDto::class, get_class($responseDto));
        $this->assertEquals($responseData, $responseDto->getData());
    }


    public function testCreateCustomer()
    {
        $responseData = [
            'some' => 'response'
        ];

        $requestData = [
            'some' => 'request'
        ];

        $response = new ResponseDto([], $responseData);

        $wrapper = Mockery::mock(CustomerWrapper::class)
            ->shouldReceive('createCustomer')
            ->withArgs([$requestData])
            ->andReturn($response)
            ->getMock();

        $facade = $this->apiFacadeFactory(CustomerWrapper::class, $wrapper);

        $responseDto = $facade->createCustomer($requestData);

        $this->assertInstanceOf(ResponseDto::class, $responseDto);
        $this->assertEquals(ResponseDto::class, get_class($responseDto));
        $this->assertEquals($responseData, $responseDto->getData());
    }

    public function testUpdateCustomer()
    {
        $id = 2;

        $responseData = [
            'some' => 'response'
        ];

        $requestData = [
            'some' => 'request'
        ];

        $response = new ResponseDto([], $responseData);

        $wrapper = Mockery::mock(CustomerWrapper::class)
            ->shouldReceive('updateCustomer')
            ->withArgs([$id, $requestData])
            ->andReturn($response)
            ->getMock();

        $facade = $this->apiFacadeFactory(CustomerWrapper::class, $wrapper);

        $responseDto = $facade->updateCustomer($id, $requestData);

        $this->assertInstanceOf(ResponseDto::class, $responseDto);
        $this->assertEquals(ResponseDto::class, get_class($responseDto));
        $this->assertEquals($responseData, $responseDto->getData());
    }


    public function testGetCustomerOrders()
    {
        $id = 1;

        $responseData = [
            'some' => 'response'
        ];

        $response = new PaginatedResponseDto([], $responseData, 1, 2, 3,4);

        $wrapper = Mockery::mock(CustomerWrapper::class)
            ->shouldReceive('getCustomerOrders')
            ->withArgs([$id])
            ->andReturn($response)
            ->getMock();

        $facade = $this->apiFacadeFactory(CustomerWrapper::class, $wrapper);

        $responseDto = $facade->getCustomerOrders($id);

        $this->assertInstanceOf(PaginatedResponseDto::class, $responseDto);
        $this->assertEquals(PaginatedResponseDto::class, get_class($responseDto));
        $this->assertEquals($responseData, $responseDto->getData());
    }


    public function testGetOrderById()
    {
        $id = 1;

        $responseData = [
            'some' => 'response'
        ];

        $response = new ResponseDto([], $responseData);

        $wrapper = Mockery::mock(OrderWrapper::class)
            ->shouldReceive('getOrderById')
            ->withArgs([$id])
            ->andReturn($response)
            ->getMock();

        $facade = $this->apiFacadeFactory(OrderWrapper::class, $wrapper);

        $responseDto = $facade->getOrderById($id);

        $this->assertInstanceOf(ResponseDto::class, $responseDto);
        $this->assertEquals(ResponseDto::class, get_class($responseDto));
        $this->assertEquals($responseData, $responseDto->getData());
    }


    public function testGetOrderByCode()
    {
        $code = 'some_code';

        $responseData = [
            'some' => 'response'
        ];

        $response = new ResponseDto([], $responseData);

        $wrapper = Mockery::mock(OrderWrapper::class)
            ->shouldReceive('getOrderByCode')
            ->withArgs([$code])
            ->andReturn($response)
            ->getMock();

        $facade = $this->apiFacadeFactory(OrderWrapper::class, $wrapper);

        $responseDto = $facade->getOrderByCode($code);

        $this->assertInstanceOf(ResponseDto::class, $responseDto);
        $this->assertEquals(ResponseDto::class, get_class($responseDto));
        $this->assertEquals($responseData, $responseDto->getData());
    }


    public function testCreateOrder()
    {
        $customerId = 1;
        $basketReference = 'some_reference';
        $paymentGatewayId = 2;
        $termsId = 1;
        $partnerReference = 'some_partner';
        $productData = [
            [
                'id' => 3
            ]
        ];
        $customerData = [
            'email' => 'some_email'
        ];

        $responseData = [
            'some' => 'response'
        ];

        $response = new ResponseDto([], $responseData);

        $wrapper = Mockery::mock(OrderWrapper::class)
            ->shouldReceive('createOrder')
            ->withArgs([$customerId, $basketReference, $paymentGatewayId, $termsId, $partnerReference, $productData, $customerData])
            ->andReturn($response)
            ->getMock();

        $facade = $this->apiFacadeFactory(OrderWrapper::class, $wrapper);

        $responseDto = $facade->createOrder($customerId, $basketReference, $paymentGatewayId, $termsId, $partnerReference, $productData, $customerData);

        $this->assertInstanceOf(ResponseDto::class, $responseDto);
        $this->assertEquals(ResponseDto::class, get_class($responseDto));
        $this->assertEquals($responseData, $responseDto->getData());
    }


    public function testCompleteOrder()
    {
        $orderId = 1;
        $transactionReference = 'some_reference';
        $trace = [
            'some' => 'trace'
        ];

        $responseData = [
            'some' => 'response'
        ];


        $response = new ResponseDto([], $responseData);

        $wrapper = Mockery::mock(OrderWrapper::class)
            ->shouldReceive('completeOrder')
            ->withArgs([$orderId, $transactionReference, $trace])
            ->andReturn($response)
            ->getMock();

        $facade = $this->apiFacadeFactory(OrderWrapper::class, $wrapper);

        $responseDto = $facade->completeOrder($orderId, $transactionReference, $trace);

        $this->assertInstanceOf(ResponseDto::class, $responseDto);
        $this->assertEquals(ResponseDto::class, get_class($responseDto));
        $this->assertEquals($responseData, $responseDto->getData());
    }


    public function testFailedOrder()
    {
        $orderId = 1;
        $trace = [
            'some' => 'trace'
        ];

        $responseData = [
            'some' => 'response'
        ];

        $response = new ResponseDto([], $responseData);

        $wrapper = Mockery::mock(OrderWrapper::class)
            ->shouldReceive('failedOrder')
            ->withArgs([$orderId, $trace])
            ->andReturn($response)
            ->getMock();

        $facade = $this->apiFacadeFactory(OrderWrapper::class, $wrapper);

        $responseDto = $facade->failedOrder($orderId, $trace);

        $this->assertInstanceOf(ResponseDto::class, $responseDto);
        $this->assertEquals(ResponseDto::class, get_class($responseDto));
        $this->assertEquals($responseData, $responseDto->getData());
    }


    public function testPayOrder()
    {
        $id = 1;

        $responseData = [
            'some' => 'response'
        ];

        $response = new ResponseDto([], $responseData);

        $wrapper = Mockery::mock(OrderWrapper::class)
            ->shouldReceive('payOrder')
            ->withArgs([$id])
            ->andReturn($response)
            ->getMock();

        $facade = $this->apiFacadeFactory(OrderWrapper::class, $wrapper);

        $responseDto = $facade->payOrder($id);

        $this->assertInstanceOf(ResponseDto::class, $responseDto);
        $this->assertEquals(ResponseDto::class, get_class($responseDto));
        $this->assertEquals($responseData, $responseDto->getData());
    }

    public function testGetDeliveryTypes()
    {
        $responseData = [
            'some' => 'response'
        ];

        $paginationDto = new PaginationDto(1, 2);
        $filterCollectionDto = new FilterCollectionDto([
            new FilterDto('property', 'value')
        ]);
        $sortCollectionDto = new SortCollectionDto([
            new SortDto('property', 'asc')
        ]);

        $response = new PaginatedResponseDto([], $responseData, 1, 2, 3, 4);

        $wrapper = Mockery::mock(OrderWrapper::class)
            ->shouldReceive('getDeliveryTypes')
            ->withArgs([$paginationDto, $filterCollectionDto, $sortCollectionDto])
            ->andReturn($response)
            ->getMock();

        $facade = $this->apiFacadeFactory(OrderWrapper::class, $wrapper);

        $responseDto = $facade->getDeliveryTypes($paginationDto, $filterCollectionDto, $sortCollectionDto);

        $this->assertInstanceOf(PaginatedResponseDto::class, $responseDto);
        $this->assertEquals(PaginatedResponseDto::class, get_class($responseDto));
        $this->assertEquals($responseData, $responseDto->getData());
    }

    public function testGetTags()
    {
        $responseData = [
            'some' => 'response'
        ];

        $paginationDto = new PaginationDto(1, 2);
        $filterCollectionDto = new FilterCollectionDto([
            new FilterDto('property', 'value')
        ]);
        $sortCollectionDto = new SortCollectionDto([
            new SortDto('property', 'asc')
        ]);

        $response = new PaginatedResponseDto([], $responseData, 1, 2, 3, 4);

        $wrapper = Mockery::mock(TagWrapper::class)
            ->shouldReceive('getTags')
            ->withArgs([$paginationDto, $filterCollectionDto, $sortCollectionDto])
            ->andReturn($response)
            ->getMock();

        $facade = $this->apiFacadeFactory(TagWrapper::class, $wrapper);

        $responseDto = $facade->getTags($paginationDto, $filterCollectionDto, $sortCollectionDto);

        $this->assertInstanceOf(PaginatedResponseDto::class, $responseDto);
        $this->assertEquals(PaginatedResponseDto::class, get_class($responseDto));
        $this->assertEquals($responseData, $responseDto->getData());
    }

    public function testGetTagBySlug()
    {
        $slug = 'some_slug';

        $responseData = [
            'some' => 'response'
        ];

        $response = new ResponseDto([], $responseData);

        $wrapper = Mockery::mock(TagWrapper::class)
            ->shouldReceive('getTagBySlug')
            ->withArgs([$slug])
            ->andReturn($response)
            ->getMock();

        $facade = $this->apiFacadeFactory(TagWrapper::class, $wrapper);

        $responseDto = $facade->getTagBySlug($slug);

        $this->assertInstanceOf(ResponseDto::class, $responseDto);
        $this->assertEquals(ResponseDto::class, get_class($responseDto));
        $this->assertEquals($responseData, $responseDto->getData());
    }

    public function testGetProductGroups()
    {
        $responseData = [
            'some' => 'response'
        ];

        $paginationDto = new PaginationDto(1, 2);
        $filterCollectionDto = new FilterCollectionDto([
            new FilterDto('property', 'value')
        ]);
        $sortCollectionDto = new SortCollectionDto([
            new SortDto('property', 'asc')
        ]);

        $response = new PaginatedResponseDto([], $responseData, 1, 2, 3, 4);

        $wrapper = Mockery::mock(ProductGroupWrapper::class)
            ->shouldReceive('getProductGroups')
            ->withArgs([$paginationDto, $filterCollectionDto, $sortCollectionDto])
            ->andReturn($response)
            ->getMock();

        $facade = $this->apiFacadeFactory(ProductGroupWrapper::class, $wrapper);

        $responseDto = $facade->getProductGroups($paginationDto, $filterCollectionDto, $sortCollectionDto);

        $this->assertInstanceOf(PaginatedResponseDto::class, $responseDto);
        $this->assertEquals(PaginatedResponseDto::class, get_class($responseDto));
        $this->assertEquals($responseData, $responseDto->getData());
    }

    public function testGetProductGroupById()
    {
        $id = 1;

        $responseData = [
            'some' => 'response'
        ];

        $response = new ResponseDto([], $responseData);

        $wrapper = Mockery::mock(ProductGroupWrapper::class)
            ->shouldReceive('getProductGroupById')
            ->withArgs([$id])
            ->andReturn($response)
            ->getMock();

        $facade = $this->apiFacadeFactory(ProductGroupWrapper::class, $wrapper);

        $responseDto = $facade->getProductGroupById($id);

        $this->assertInstanceOf(ResponseDto::class, $responseDto);
        $this->assertEquals(ResponseDto::class, get_class($responseDto));
        $this->assertEquals($responseData, $responseDto->getData());
    }

    public function testGetProductGroupBySlug()
    {
        $slug = 'some_slug';

        $responseData = [
            'some' => 'response'
        ];

        $response = new ResponseDto([], $responseData);

        $wrapper = Mockery::mock(ProductGroupWrapper::class)
            ->shouldReceive('getProductGroupBySlug')
            ->withArgs([$slug])
            ->andReturn($response)
            ->getMock();

        $facade = $this->apiFacadeFactory(ProductGroupWrapper::class, $wrapper);

        $responseDto = $facade->getProductGroupBySlug($slug);

        $this->assertInstanceOf(ResponseDto::class, $responseDto);
        $this->assertEquals(ResponseDto::class, get_class($responseDto));
        $this->assertEquals($responseData, $responseDto->getData());
    }

    public function testGetTerms()
    {
        $responseData = [
            'some' => 'response'
        ];

        $paginationDto = new PaginationDto(1, 2);
        $filterCollectionDto = new FilterCollectionDto([
            new FilterDto('property', 'value')
        ]);
        $sortCollectionDto = new SortCollectionDto([
            new SortDto('property', 'asc')
        ]);

        $response = new PaginatedResponseDto([], $responseData, 1, 2, 3, 4);

        $wrapper = Mockery::mock(TermWrapper::class)
            ->shouldReceive('getTerms')
            ->withArgs([$paginationDto, $filterCollectionDto, $sortCollectionDto])
            ->andReturn($response)
            ->getMock();

        $facade = $this->apiFacadeFactory(TermWrapper::class, $wrapper);

        $responseDto = $facade->getTerms($paginationDto, $filterCollectionDto, $sortCollectionDto);

        $this->assertInstanceOf(PaginatedResponseDto::class, $responseDto);
        $this->assertEquals(PaginatedResponseDto::class, get_class($responseDto));
        $this->assertEquals($responseData, $responseDto->getData());
    }

    public function testGetPages()
    {
        $responseData = [
            'some' => 'response'
        ];

        $paginationDto = new PaginationDto(1, 2);
        $filterCollectionDto = new FilterCollectionDto([
            new FilterDto('property', 'value')
        ]);
        $sortCollectionDto = new SortCollectionDto([
            new SortDto('property', 'asc')
        ]);

        $response = new PaginatedResponseDto([], $responseData, 1, 2, 3,4);

        $wrapper = Mockery::mock(PageWrapper::class)
            ->shouldReceive('getPages')
            ->withArgs([$paginationDto, $filterCollectionDto, $sortCollectionDto])
            ->andReturn($response)
            ->getMock();

        $facade = $this->apiFacadeFactory(PageWrapper::class, $wrapper);

        $responseDto = $facade->getPages($paginationDto, $filterCollectionDto, $sortCollectionDto);

        $this->assertInstanceOf(PaginatedResponseDto::class, $responseDto);
        $this->assertEquals(PaginatedResponseDto::class, get_class($responseDto));
        $this->assertEquals($responseData, $responseDto->getData());
    }

    public function testGetPageById()
    {
        $id = 1;

        $responseData = [
            'some' => 'response'
        ];

        $response = new ResponseDto([], $responseData);

        $wrapper = Mockery::mock(PageWrapper::class)
            ->shouldReceive('getPageById')
            ->withArgs([$id])
            ->andReturn($response)
            ->getMock();

        $facade = $this->apiFacadeFactory(PageWrapper::class, $wrapper);

        $responseDto = $facade->getPageById($id);

        $this->assertInstanceOf(ResponseDto::class, $responseDto);
        $this->assertEquals(ResponseDto::class, get_class($responseDto));
        $this->assertEquals($responseData, $responseDto->getData());
    }

    public function testGetPageBySlug()
    {
        $slug = 'some_slug';

        $responseData = [
            'some' => 'response'
        ];

        $response = new ResponseDto([], $responseData);

        $wrapper = Mockery::mock(PageWrapper::class)
            ->shouldReceive('getPageBySlug')
            ->withArgs([$slug])
            ->andReturn($response)
            ->getMock();

        $facade = $this->apiFacadeFactory(PageWrapper::class, $wrapper);

        $responseDto = $facade->getPageBySlug($slug);

        $this->assertInstanceOf(ResponseDto::class, $responseDto);
        $this->assertEquals(ResponseDto::class, get_class($responseDto));
        $this->assertEquals($responseData, $responseDto->getData());
    }

    public function testGetSites()
    {
        $responseData = [
            'some' => 'response'
        ];

        $paginationDto = new PaginationDto(1, 2);
        $filterCollectionDto = new FilterCollectionDto([
            new FilterDto('property', 'value')
        ]);
        $sortCollectionDto = new SortCollectionDto([
            new SortDto('property', 'asc')
        ]);

        $response = new PaginatedResponseDto([], $responseData, 1, 2, 3,4);

        $wrapper = Mockery::mock(SiteWrapper::class)
            ->shouldReceive('getSites')
            ->withArgs([$paginationDto, $filterCollectionDto, $sortCollectionDto])
            ->andReturn($response)
            ->getMock();

        $facade = $this->apiFacadeFactory(SiteWrapper::class, $wrapper);

        $responseDto = $facade->getSites($paginationDto, $filterCollectionDto, $sortCollectionDto);

        $this->assertInstanceOf(PaginatedResponseDto::class, $responseDto);
        $this->assertEquals(PaginatedResponseDto::class, get_class($responseDto));
        $this->assertEquals($responseData, $responseDto->getData());
    }

    public function testGetSiteById()
    {
        $id = 1;

        $responseData = [
            'some' => 'response'
        ];

        $response = new ResponseDto([], $responseData);

        $wrapper = Mockery::mock(SiteWrapper::class)
            ->shouldReceive('getSiteById')
            ->withArgs([$id])
            ->andReturn($response)
            ->getMock();

        $facade = $this->apiFacadeFactory(SiteWrapper::class, $wrapper);

        $responseDto = $facade->getSiteById($id);

        $this->assertInstanceOf(ResponseDto::class, $responseDto);
        $this->assertEquals(ResponseDto::class, get_class($responseDto));
        $this->assertEquals($responseData, $responseDto->getData());
    }

    public function testGetSiteBySlug()
    {
        $slug = 'some_slug';

        $responseData = [
            'some' => 'response'
        ];

        $response = new ResponseDto([], $responseData);

        $wrapper = Mockery::mock(SiteWrapper::class)
            ->shouldReceive('getSiteBySlug')
            ->withArgs([$slug])
            ->andReturn($response)
            ->getMock();

        $facade = $this->apiFacadeFactory(SiteWrapper::class, $wrapper);

        $responseDto = $facade->getSiteBySlug($slug);

        $this->assertInstanceOf(ResponseDto::class, $responseDto);
        $this->assertEquals(ResponseDto::class, get_class($responseDto));
        $this->assertEquals($responseData, $responseDto->getData());
    }

    public function testGetCountries()
    {
        $responseData = [
            'some' => 'response'
        ];

        $response = new PaginatedResponseDto([], $responseData, 1, 2, 3,4);

        $wrapper = Mockery::mock(CountryWrapper::class)
            ->shouldReceive('getCountries')
            ->withArgs([])
            ->andReturn($response)
            ->getMock();

        $facade = $this->apiFacadeFactory(CountryWrapper::class, $wrapper);

        $responseDto = $facade->getCountries();

        $this->assertInstanceOf(PaginatedResponseDto::class, $responseDto);
        $this->assertEquals(PaginatedResponseDto::class, get_class($responseDto));
        $this->assertEquals($responseData, $responseDto->getData());
    }

    public function testCreatePartner()
    {
        $responseData = [
            'some' => 'response'
        ];

        $requestData = [
            'some' => 'request'
        ];

        $response = new ResponseDto([], $responseData);

        $wrapper = Mockery::mock(PartnerWrapper::class)
            ->shouldReceive('createPartner')
            ->withArgs([$requestData])
            ->andReturn($response)
            ->getMock();

        $facade = $this->apiFacadeFactory(PartnerWrapper::class, $wrapper);

        $responseDto = $facade->createPartner($requestData);

        $this->assertInstanceOf(ResponseDto::class, $responseDto);
        $this->assertEquals(ResponseDto::class, get_class($responseDto));
        $this->assertEquals($responseData, $responseDto->getData());
    }

    /**
     * @param $class
     * @param $apiWrapper
     * @return ApiWrapper
     */
    protected function apiFacadeFactory($class, $apiWrapper)
    {
        return new ApiWrapper(
            ConfigFactory::CONFIG,
            Mockery::mock(ClientInterface::class),
            [
                $class => $apiWrapper
            ]
        );
    }
}