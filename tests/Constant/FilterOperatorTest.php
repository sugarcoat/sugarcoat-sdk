<?php

namespace Sugarcoat\Tests\Constant;

use Sugarcoat\APIWrapper\Constant\FilterOperator;
use Sugarcoat\Tests\BaseTest;

class FilterOperatorTest extends BaseTest
{
    /**
     * @dataProvider operatorProvider
     */
    public function testValidate($value, $expectaction)
    {
        $this->assertSame($expectaction, FilterOperator::validate($value));
    }

    /**
     * @return array[]
     */
    public function operatorProvider()
    {
        return [
            [
                'LIKE',
                true
            ],
            [
                '=',
                true
            ],
            [
                '!=',
                true
            ],
            [
                '>',
                true
            ],
            [
                'where',
                true
            ],
            [
                'whereIn',
                true
            ],
            [
                'whereBetween',
                true
            ],
            [
                'whereAnd',
                true
            ],
            [
                'NOT IN',
                false
            ]
        ];
    }
}