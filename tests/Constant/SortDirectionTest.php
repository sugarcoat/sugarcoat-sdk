<?php

namespace Sugarcoat\Tests\Constant;

use Sugarcoat\APIWrapper\Constant\SortDirection;
use Sugarcoat\Tests\BaseTest;

class SortDirectionTest extends BaseTest
{
    /**
     * @dataProvider operatorProvider
     */
    public function testValidate($value, $expectaction)
    {
        $this->assertSame($expectaction, SortDirection::validate($value));
    }

    /**
     * @return array[]
     */
    public function operatorProvider()
    {
        return [
            [
                'asc',
                true
            ],
            [
                'desc',
                true
            ],
            [
                'down',
                false
            ],
            [
                'up',
                false
            ]
        ];
    }
}