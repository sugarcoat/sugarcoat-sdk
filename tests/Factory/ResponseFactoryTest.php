<?php

namespace Sugarcoat\Tests\Factory;

use GuzzleHttp\Psr7\Response;
use Sugarcoat\APIWrapper\Dto\PaginatedResponseDto;
use Sugarcoat\APIWrapper\Dto\ResponseDto;
use Sugarcoat\APIWrapper\Factory\ResponseFactory;
use Sugarcoat\Tests\BaseTest;

class ResponseFactoryTest extends BaseTest
{
    /**
     * @dataProvider responseDtoFactoryProvider
     */
    public function testResponseFactory($statusCode, $headers, $data, $expectedData)
    {
        $response = new Response($statusCode, $headers, \json_encode($data));
        $dto = ResponseFactory::responseDtoFactory($response);

        $this->assertInstanceOf(ResponseDto::class, $dto);
        $this->assertEquals($headers, $dto->getHeaders());
        $this->assertEquals($expectedData, $dto->getData());
    }

    /**
     * @dataProvider paginatedResponseDtoFactoryProvider
     */
    public function testPaginatedResponseFactory($statusCode, $headers, $data, $paginationData, $expectedData)
    {
        $response = new Response($statusCode, $headers, \json_encode(array_merge($paginationData, $data)));
        $dto = ResponseFactory::paginatedResponseDtoFactory($response);

        $this->assertInstanceOf(PaginatedResponseDto::class, $dto);
        $this->assertEquals($headers, $dto->getHeaders());
        $this->assertEquals($expectedData, $dto->getData());
        $this->assertEquals(3, $dto->getCount());
        $this->assertEquals(2, $dto->getCurrentPage());
        $this->assertEquals(2, $dto->getCurrentPage());
    }

    public function responseDtoFactoryProvider()
    {
        return [
            [
                200,
                [
                    'some' => ['header']
                ],
                [
                    'data' => [
                        'some' => 'data'
                    ]
                ],
                [
                    'some' => 'data'
                ]
            ],
            [
                200,
                [
                    'some' => ['header']
                ],
                [
                    'some' => 'data',
                    'someother' => 'data'
                ],
                [
                    'some' => 'data',
                    'someother' => 'data'
                ]
            ],
            [
                200,
                [
                    'some' => ['header']
                ],
                [
                    'data' => []
                ],
                []
            ],
        ];
    }

    public function paginatedResponseDtoFactoryProvider()
    {
        return [
            [
                200,
                [
                    'some' => ['header']
                ],
                [
                    'count' => 3,
                    'current_page' => 2,
                    'last_page' => 1,
                    'interval' => 20
                ],
                [
                    'data' => [
                        'some' => 'data'
                    ]
                ],
                [
                    'some' => 'data'
                ]
            ],
            [
                200,
                [
                    'some' => ['header']
                ],
                [
                    'count' => 3,
                    'current_page' => 2,
                    'last_page' => 1,
                    'interval' => 20
                ],
                [
                    'data' => [
                        [
                            'some' => 'data'
                        ],
                        [
                            'some' => 'data'
                        ]
                    ]
                ],
                [
                    [
                        'some' => 'data'
                    ],
                    [
                        'some' => 'data'
                    ]
                ]
            ],
            [
                200,
                [
                    'some' => ['header']
                ],
                [
                    'count' => 3,
                    'current_page' => 2,
                    'last_page' => 1,
                    'interval' => 20
                ],
                [
                    'some' => 'data',
                    'someother' => 'data'
                ],
                [
                    'some' => 'data',
                    'someother' => 'data'
                ]
            ],
            [
                200,
                [
                    'some' => ['header']
                ],
                [
                    'count' => 3,
                    'current_page' => 2,
                    'last_page' => 1,
                    'interval' => 20
                ],
                [
                    'data' => [

                    ]
                ],
                [

                ]
            ],
        ];
    }
}