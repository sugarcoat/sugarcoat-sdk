<?php

namespace Sugarcoat\Tests\Factory;

use GuzzleHttp\Psr7\ServerRequest;
use Psr\Http\Message\RequestInterface;
use Sugarcoat\APIWrapper\Dto\FilterCollectionDto;
use Sugarcoat\APIWrapper\Dto\FilterDto;
use Sugarcoat\APIWrapper\Dto\PaginationDto;
use Sugarcoat\APIWrapper\Dto\SortCollectionDto;
use Sugarcoat\APIWrapper\Dto\SortDto;
use Sugarcoat\APIWrapper\Factory\RequestFactory;
use Sugarcoat\Tests\BaseTest;

class RequestFactoryTest extends BaseTest
{
    /**
     * @dataProvider factoryProvider
     */
    public function testFactory(
        $endPoint,
        $method,
        $data,
        $paginationDto,
        $filterCollectionDto,
        $sortCollectionDto,
        $includes,
        $expectaction
    )
    {
        $request = RequestFactory::factory(
            $endPoint,
            $method,
            $data,
            $paginationDto,
            $filterCollectionDto,
            $sortCollectionDto,
            $includes
        );

        $this->assertInstanceOf(RequestInterface::class, $request);
        $this->assertEquals($expectaction->getMethod(), $request->getMethod());
        $this->assertEquals($expectaction->getUri(), $request->getUri());
        $this->assertEquals($expectaction->getHeaders(), $request->getHeaders());
        $this->assertEquals((string)$expectaction->getBody(), (string)$request->getBody());
    }

    /**
     * @return array[]
     */
    public function factoryProvider()
    {
        return [
            [
                'test',
                'GET',
                [
                    'some' => 'data'
                ],
                null,
                null,
                null,
                [],
                new ServerRequest('GET', 'test', [], null, '1.1', [
                        'some' => 'data'
                    ]
                )
            ],
            [
                'test',
                'GET',
                [
                    'some' => 'data'
                ],
                new PaginationDto(2, 10),
                null,
                null,
                [],
                new ServerRequest('GET', 'test', [], null, '1.1', [
                        'some' => 'data',
                        'page' => 2,
                        'pagination_interval' => 10
                    ]
                )
            ],
            [
                'test',
                'GET',
                [
                    'some' => 'data'
                ],
                null,
                new FilterCollectionDto([
                    new FilterDto('property1', 'value1', '!=')
                ]),
                null,
                [],
                new ServerRequest('GET', 'test', [], null, '1.1', [
                        'some' => 'data',
                        'filters' => [
                            [
                                'property' => 'property1',
                                'value' => 'value1',
                                'operator' => '!='
                            ]
                        ]
                    ]
                )
            ],
            [
                'test',
                'GET',
                [
                    'some' => 'data'
                ],
                null,
                null,
                new SortCollectionDto([
                    new SortDto('property1', 'desc')
                ]),
                [],
                new ServerRequest('GET', 'test', [], null, '1.1', [
                        'some' => 'data',
                        'sort' => [
                            [
                                'property' => 'property1',
                                'direction' => 'desc',
                            ]
                        ]
                    ]
                )
            ],
            [
                'test',
                'GET',
                [
                    'some' => 'data'
                ],
                new PaginationDto(2, 10),
                new FilterCollectionDto([
                    new FilterDto('property1', 'value1', '!=')
                ]),
                new SortCollectionDto([
                    new SortDto('property1', 'desc')
                ]),
                [],
                new ServerRequest('GET', 'test', [], null, '1.1', [
                        'some' => 'data',
                        'page' => 2,
                        'pagination_interval' => 10,
                        'filters' => [
                            [
                                'property' => 'property1',
                                'value' => 'value1',
                                'operator' => '!='
                            ]
                        ],
                        'sort' => [
                            [
                                'property' => 'property1',
                                'direction' => 'desc',
                            ]
                        ]
                    ]
                )
            ],
            [
                'test',
                'GET',
                [
                    'some' => 'data'
                ],
                null,
                null,
                null,
                ['something', 'else'],
                new ServerRequest('GET', 'test', [], null, '1.1', [
                        'some' => 'data',
                        'includes' => 'something,else'
                    ]
                )
            ]
        ];
    }
}