<?php

namespace Sugarcoat\Tests\Config;

use Sugarcoat\APIWrapper\Config\Config;
use Sugarcoat\APIWrapper\Exception\ConfigException;
use Sugarcoat\Tests\BaseTest;

class ConfigTest extends BaseTest
{
    public function testMissingConfigKeyShouldThrowError()
    {
        $this->expectException(ConfigException::class);

        $config = new Config([]);
        $config->getApiBaseUrl();
    }

    public function testGetApiBaseUrl()
    {
        $config = new Config([
            'api_key' => '12345',
            'api_base_url' => 'http://api.sugar-coat.io/'
        ]);

        $this->assertEquals('http://api.sugar-coat.io/', $config->getApiBaseUrl());
    }

    public function testGetApiKey()
    {
        $config = new Config([
            'api_key' => '12345',
            'api_base_url' => 'http://api.sugar-coat.io/'
        ]);

        $this->assertEquals('12345', $config->getApiKey());
    }
}