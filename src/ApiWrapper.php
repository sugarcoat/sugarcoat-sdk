<?php

namespace Sugarcoat\APIWrapper;

use Sugarcoat\APIWrapper\Client\ClientInterface;
use Sugarcoat\APIWrapper\Client\HttpClient;
use Sugarcoat\APIWrapper\Config\Config;
use Sugarcoat\APIWrapper\Constant\ProductRecommendationType;
use Sugarcoat\APIWrapper\Dto\FilterCollectionDto;
use Sugarcoat\APIWrapper\Dto\PaginationDto;
use Sugarcoat\APIWrapper\Dto\SortCollectionDto;
use Sugarcoat\APIWrapper\Wrapper\AnalyticWrapper;
use Sugarcoat\APIWrapper\Wrapper\AuthWrapper;
use Sugarcoat\APIWrapper\Wrapper\BasketWrapper;
use Sugarcoat\APIWrapper\Wrapper\CategoryWrapper;
use Sugarcoat\APIWrapper\Wrapper\CountryWrapper;
use Sugarcoat\APIWrapper\Wrapper\CustomerWrapper;
use Sugarcoat\APIWrapper\Wrapper\OrderWrapper;
use Sugarcoat\APIWrapper\Wrapper\PageWrapper;
use Sugarcoat\APIWrapper\Wrapper\PartnerWrapper;
use Sugarcoat\APIWrapper\Wrapper\ProductGroupWrapper;
use Sugarcoat\APIWrapper\Wrapper\ProductTypeWrapper;
use Sugarcoat\APIWrapper\Wrapper\ProductWrapper;
use Sugarcoat\APIWrapper\Wrapper\StoreWrapper;
use Sugarcoat\APIWrapper\Wrapper\TagWrapper;
use Sugarcoat\APIWrapper\Wrapper\TermWrapper;
use Sugarcoat\APIWrapper\Wrapper\UserWrapper;
use Sugarcoat\APIWrapper\Wrapper\SiteWrapper;

use Sugarcoat\APIWrapper\Dto\ResponseDto;
use Sugarcoat\APIWrapper\Dto\PaginatedResponseDto;
use Sugarcoat\APIWrapper\Exception\ErrorResponseException;
use Sugarcoat\APIWrapper\Exception\ServiceErrorResponseException;

class ApiWrapper
{
    /** @var StoreWrapper */
    protected $store;

    /** @var ProductWrapper */
    protected $product;

    /** @var ProductTypeWrapper */
    protected $productType;

    /** @var AnalyticWrapper */
    protected $analytic;

    /** @var CategoryWrapper */
    protected $category;

    /** @var BasketWrapper */
    protected $basket;

    /** @var UserWrapper */
    protected $user;

    /** @var TagWrapper */
    protected $tag;

    /** @var CustomerWrapper */
    protected $customer;

    /** @var OrderWrapper */
    protected $order;

    /** @var ProductGroupWrapper */
    protected $group;

    /** @var AuthWrapper */
    protected $auth;

    /** @var TermWrapper */
    protected $term;

    /** @var PageWrapper */
    protected $page;

    /** @var CountryWrapper */
    protected $country;

    /** @var PartnerWrapper */
    protected $partner;

    /** @var SiteWrapper */
    protected $site;

    /**
     * ApiWrapper constructor.
     * @param array $configData
     * @param ClientInterface|null $client
     * @param array $apiWrappers
     */
    public function __construct(
        array $configData,
        ClientInterface $client = null,
        array $apiWrappers = []
    )
    {
        $config = new Config($configData);
        $client = $client ?: new HttpClient($config);

        $this->store = isset($apiWrappers[StoreWrapper::class]) ? $apiWrappers[StoreWrapper::class] : new StoreWrapper($config,
            $client);
        $this->product = isset($apiWrappers[ProductWrapper::class]) ? $apiWrappers[ProductWrapper::class] : new ProductWrapper($config,
            $client);
        $this->productType = isset($apiWrappers[ProductTypeWrapper::class]) ? $apiWrappers[ProductTypeWrapper::class] : new ProductTypeWrapper($config,
            $client);
        $this->analytic = isset($apiWrappers[AnalyticWrapper::class]) ? $apiWrappers[AnalyticWrapper::class] : new AnalyticWrapper($config,
            $client);
        $this->category = isset($apiWrappers[CategoryWrapper::class]) ? $apiWrappers[CategoryWrapper::class] : new CategoryWrapper($config,
            $client);
        $this->basket = isset($apiWrappers[BasketWrapper::class]) ? $apiWrappers[BasketWrapper::class] : new BasketWrapper($config,
            $client);
        $this->user = isset($apiWrappers[UserWrapper::class]) ? $apiWrappers[UserWrapper::class] : new UserWrapper($config,
            $client);
        $this->tag = isset($apiWrappers[TagWrapper::class]) ? $apiWrappers[TagWrapper::class] : new TagWrapper($config,
            $client);
        $this->customer = isset($apiWrappers[CustomerWrapper::class]) ? $apiWrappers[CustomerWrapper::class] : new CustomerWrapper($config,
            $client);
        $this->order = isset($apiWrappers[OrderWrapper::class]) ? $apiWrappers[OrderWrapper::class] : new OrderWrapper($config,
            $client);
        $this->group = isset($apiWrappers[ProductGroupWrapper::class]) ? $apiWrappers[ProductGroupWrapper::class] : new ProductGroupWrapper($config,
            $client);
        $this->auth = isset($apiWrappers[AuthWrapper::class]) ? $apiWrappers[AuthWrapper::class] : new AuthWrapper($config,
            $client);
        $this->term = isset($apiWrappers[TermWrapper::class]) ? $apiWrappers[TermWrapper::class] : new TermWrapper($config,
            $client);
        $this->page = isset($apiWrappers[PageWrapper::class]) ? $apiWrappers[PageWrapper::class] : new PageWrapper($config,
            $client);
        $this->country = isset($apiWrappers[CountryWrapper::class]) ? $apiWrappers[CountryWrapper::class] : new CountryWrapper($config,
            $client);
        $this->partner = isset($apiWrappers[PartnerWrapper::class]) ? $apiWrappers[PartnerWrapper::class] : new PartnerWrapper($config,
            $client);
        $this->site = isset($apiWrappers[SiteWrapper::class]) ? $apiWrappers[SiteWrapper::class] : new SiteWrapper($config,
            $client);
    }

    /**
     * string $refreshToken
     * @return ResponseDto
     * @throws ErrorResponseException
     * @throws ServiceErrorResponseException
     */
    public function getToken($refreshToken = null)
    {
        return $this->auth->getToken($refreshToken);
    }

    /**
     * @param string $email
     * @param string $password
     * @return ResponseDto
     * @throws ErrorResponseException
     * @throws ServiceErrorResponseException
     */
    public function createUserToken($email, $password)
    {
        return $this->auth->createUserToken($email, $password);
    }

    /**
     * @param string $token
     * @return ResponseDto
     * @throws ErrorResponseException
     * @throws ServiceErrorResponseException
     */
    public function getTokenByToken($token)
    {
        return $this->auth->getTokenByToken($token);
    }

    /**
     * @return ResponseDto
     * @throws ErrorResponseException
     * @throws ServiceErrorResponseException
     */
    public function getStore()
    {
        return $this->store->getStore();
    }

    /**
     * @param PaginationDto|null $paginationDto
     * @param FilterCollectionDto|null $filterCollectionDto
     * @param SortCollectionDto|null $sortCollectionDto
     * @return PaginatedResponseDto
     * @throws ErrorResponseException
     * @throws ServiceErrorResponseException
     */
    public function getStorePaymentMethods(
        PaginationDto $paginationDto = null,
        FilterCollectionDto $filterCollectionDto = null,
        SortCollectionDto $sortCollectionDto = null
    )
    {
        return $this->store->getStorePaymentMethods($paginationDto, $filterCollectionDto, $sortCollectionDto);
    }

    /**
     * @param PaginationDto|null $paginationDto
     * @param FilterCollectionDto|null $filterCollectionDto
     * @param SortCollectionDto|null $sortCollectionDto
     * @return PaginatedResponseDto
     * @throws ErrorResponseException
     * @throws ServiceErrorResponseException
     */
    public function getProducts(
        PaginationDto $paginationDto = null,
        FilterCollectionDto $filterCollectionDto = null,
        SortCollectionDto $sortCollectionDto = null
    )
    {
        return $this->product->getProducts($paginationDto, $filterCollectionDto, $sortCollectionDto);
    }

    /**
     * @param int $id
     * @return ResponseDto
     * @throws ErrorResponseException
     * @throws ServiceErrorResponseException
     */
    public function getProductById($id)
    {
        return $this->product->getProductById($id);
    }

    /**
     * @param string $name
     * @return PaginatedResponseDto
     * @throws ErrorResponseException
     * @throws ServiceErrorResponseException
     */
    public function getProductByName($name)
    {
        return $this->product->getProductByName($name);
    }

    /**
     * @param string $slug
     * @return ResponseDto
     * @throws ErrorResponseException
     * @throws ServiceErrorResponseException
     */
    public function getProductBySlug($slug)
    {
        return $this->product->getProductBySlug($slug);
    }

    /**
     * @param array $ids
     * @return PaginatedResponseDto
     * @throws ErrorResponseException
     * @throws ServiceErrorResponseException
     */
    public function getProductsByTagIds(array $ids)
    {
        return $this->product->getProductsByTagIds($ids);
    }

    /**
     * @param array $ids
     * @return PaginatedResponseDto
     * @throws ErrorResponseException
     * @throws ServiceErrorResponseException
     */
    public function getProductsByIds(array $ids)
    {
        return $this->product->getProductsByIds($ids);
    }

    /**
     * @param string $searchTerm
     * @return PaginatedResponseDto
     * @throws ErrorResponseException
     * @throws ServiceErrorResponseException
     */
    public function searchProducts($searchTerm)
    {
        return $this->product->searchProducts($searchTerm);
    }

    /**
     * @param array $ids
     * @param array $type
     * @param PaginationDto|null $paginationDto
     * @param FilterCollectionDto|null $filterCollectionDto
     * @param SortCollectionDto|null $sortCollectionDto
     * @return PaginatedResponseDto
     * @throws ErrorResponseException
     * @throws ServiceErrorResponseException
     */
    public function getRecommendedProducts(
        array $ids,
        $types = [ProductRecommendationType::ALL],
        PaginationDto $paginationDto = null,
        FilterCollectionDto $filterCollectionDto = null,
        SortCollectionDto $sortCollectionDto = null
    )
    {
        return $this->product->getRecommendedProducts(
            $ids,
            $types,
            $paginationDto,
            $filterCollectionDto,
            $sortCollectionDto
        );
    }

    /**
     * @param PaginationDto|null $paginationDto
     * @param FilterCollectionDto|null $filterCollectionDto
     * @param SortCollectionDto|null $sortCollectionDto
     * @return PaginatedResponseDto
     * @throws ErrorResponseException
     * @throws ServiceErrorResponseException
     */
    public function getProductTypes(
        PaginationDto $paginationDto = null,
        FilterCollectionDto $filterCollectionDto = null,
        SortCollectionDto $sortCollectionDto = null
    )
    {
        return $this->productType->getProductTypes($paginationDto, $filterCollectionDto, $sortCollectionDto);
    }

    /**
     * @param int $id
     * @return ResponseDto
     * @throws ErrorResponseException
     * @throws ServiceErrorResponseException
     */
    public function getProductTypeById($id)
    {
        return $this->productType->getProductTypeById($id);
    }

    /**
     * @param string $slug
     * @return ResponseDto
     * @throws ErrorResponseException
     * @throws ServiceErrorResponseException
     */
    public function getProductTypeBySlug($slug)
    {
        return $this->productType->getProductTypeBySlug($slug);
    }

    /**
     * @param array $data
     * @return ResponseDto
     * @throws ErrorResponseException
     * @throws ServiceErrorResponseException
     */
    public function trackAnalytic(array $data = [])
    {
        return $this->analytic->trackAnalytic($data);
    }

    /**
     * @param int $id
     * @param array $data
     * @return ResponseDto
     * @throws ErrorResponseException
     * @throws ServiceErrorResponseException
     */
    public function trackAnalyticByProductId($id, array $data = [])
    {
        return $this->analytic->trackAnalyticByProductId($id, $data);
    }

    /**
     * @param string $destination
     * @param array $data
     * @return ResponseDto
     * @throws ErrorResponseException
     * @throws ServiceErrorResponseException
     */
    public function trackAnalyticByDestination($destination, array $data = [])
    {
        return $this->analytic->trackAnalyticByDestination($destination, $data);
    }

    /**
     * @param string $type
     * @param array $data
     * @return ResponseDto
     * @throws ErrorResponseException
     * @throws ServiceErrorResponseException
     */
    public function trackAnalyticByType($type, array $data = [])
    {
        return $this->analytic->trackAnalyticByType($type, $data);
    }

    /**
     * @param PaginationDto|null $paginationDto
     * @param FilterCollectionDto|null $filterCollectionDto
     * @param SortCollectionDto|null $sortCollectionDto
     * @return PaginatedResponseDto
     * @throws ErrorResponseException
     * @throws ServiceErrorResponseException
     */
    public function getCategories(
        PaginationDto $paginationDto = null,
        FilterCollectionDto $filterCollectionDto = null,
        SortCollectionDto $sortCollectionDto = null
    )
    {
        return $this->category->getCategories($paginationDto, $filterCollectionDto, $sortCollectionDto);
    }

    /**
     * @param int $id
     * @return ResponseDto
     * @throws ErrorResponseException
     * @throws ServiceErrorResponseException
     */
    public function getCategoryById($id)
    {
        return $this->category->getCategoryById($id);
    }

    /**
     * @param string $slug
     * @return ResponseDto
     * @throws ErrorResponseException
     * @throws ServiceErrorResponseException
     */
    public function getCategoryBySlug($slug)
    {
        return $this->category->getCategoryBySlug($slug);
    }

    /**
     * @param PaginationDto|null $paginationDto
     * @param FilterCollectionDto|null $filterCollectionDto
     * @param SortCollectionDto|null $sortCollectionDto
     * @return PaginatedResponseDto
     * @throws ErrorResponseException
     * @throws ServiceErrorResponseException
     */
    public function getCategoryOptions(
        PaginationDto $paginationDto = null,
        FilterCollectionDto $filterCollectionDto = null,
        SortCollectionDto $sortCollectionDto = null
    )
    {
        return $this->category->getCategoryOptions($paginationDto, $filterCollectionDto, $sortCollectionDto);
    }

    /**
     * @param int $id
     * @return ResponseDto
     * @throws ErrorResponseException
     * @throws ServiceErrorResponseException
     */
    public function getCategoryOptionById($id)
    {
        return $this->category->getCategoryOptionById($id);
    }

    /**
     * @param string $slug
     * @return ResponseDto
     * @throws ErrorResponseException
     * @throws ServiceErrorResponseException
     */
    public function getCategoryOptionBySlug($slug)
    {
        return $this->category->getCategoryOptionBySlug($slug);
    }

    /**
     * @param string $basketToken
     * @return ResponseDto
     * @throws ErrorResponseException
     * @throws ServiceErrorResponseException
     */
    public function getBasket($basketToken)
    {
        return $this->basket->getBasket($basketToken);
    }

    /**
     * @param string $basketToken
     * @return ResponseDto
     * @throws ErrorResponseException
     * @throws ServiceErrorResponseException
     */
    public function getBasketDeliveryTypes($basketToken)
    {
        return $this->basket->getBasketDeliveryTypes($basketToken);
    }

    /**
     * @param int|null $userId
     * @param array $basketData
     * @param null $deliveryTypeId
     * @param int|null $customerId
     * @return ResponseDto
     * @throws ErrorResponseException
     * @throws ServiceErrorResponseException
     */
    public function createBasket($userId = null, array $basketData = [], $deliveryTypeId = null, $customerId = null)
    {
        return $this->basket->createBasket($userId, $basketData, $deliveryTypeId, $customerId);
    }

    /**
     * @param string $basketToken
     * @param int|null $userId
     * @param array|null $basketData
     * @param null $deliveryTypeId
     * @param int|null $customerId
     * @return ResponseDto
     * @throws ErrorResponseException
     * @throws ServiceErrorResponseException
     */
    public function updateBasket($basketToken, $userId = null, $basketData = null, $deliveryTypeId = null, $customerId = null)
    {
        return $this->basket->updateBasket($basketToken, $userId, $basketData, $deliveryTypeId, $customerId);
    }

    /**
     * @param string $basketToken
     * @return ResponseDto
     * @throws ErrorResponseException
     * @throws ServiceErrorResponseException
     */
    public function clearBasket($basketToken)
    {
        return $this->basket->clearBasket($basketToken);
    }

    /**
     * @param string $basketToken
     * @param int $productId
     * @param int $quantity
     * @param int|null $deliveryTypeId
     * @return ResponseDto
     * @throws ErrorResponseException
     * @throws ServiceErrorResponseException
     */
    public function addProductToBasket($basketToken, $productId, $quantity, $deliveryTypeId = null)
    {
        return $this->basket->addProductToBasket($basketToken, $productId, $quantity, $deliveryTypeId);
    }

    /**
     * @param string $basketToken
     * @param int $productId
     * @return ResponseDto
     * @throws ErrorResponseException
     * @throws ServiceErrorResponseException
     */
    public function removeProductFromBasket($basketToken, $productId)
    {
        return $this->basket->removeProductFromBasket($basketToken, $productId);
    }

    /**
     * @param string $basketToken
     * @param int $discountCodeId
     * @return ResponseDto
     * @throws ErrorResponseException
     * @throws ServiceErrorResponseException
     */
    public function applyDiscountToBasket($basketToken, $discountCodeId)
    {
        return $this->basket->applyDiscountToBasket($basketToken, $discountCodeId);
    }

    /**
     * @param string $basketToken
     * @param $deliveryTypeId
     * @return ResponseDto
     */
    public function applyDeliveryToBasket($basketToken, $deliveryTypeId)
    {
        return $this->basket->applyDeliveryToBasket($basketToken, $deliveryTypeId);
    }

    /**
     * @param $basketToken
     * @return ResponseDto
     * @throws ErrorResponseException
     * @throws ServiceErrorResponseException
     */
    public function removeDiscountCodesFromBasket($basketToken)
    {
        return $this->basket->removeDiscountCodesFromBasket($basketToken);
    }

    /**
     * @param string $code
     * @return ResponseDto
     * @throws ErrorResponseException
     * @throws ServiceErrorResponseException
     */
    public function getDiscountCode($code)
    {
        return $this->basket->getDiscountCode($code);
    }

    /**
     * @param int $id
     * @return ResponseDto
     * @throws ErrorResponseException
     * @throws ServiceErrorResponseException
     */
    public function getUserById($id)
    {
        return $this->user->getUserById($id);
    }

    /**
     * @param array $data
     * @return ResponseDto
     * @throws ErrorResponseException
     * @throws ServiceErrorResponseException
     */
    public function registerUser(array $data)
    {
        return $this->user->registerUser($data);
    }

    /**
     * @param int $id
     * @param array $data
     * @return ResponseDto
     * @throws ErrorResponseException
     * @throws ServiceErrorResponseException
     */
    public function updateUser($id, array $data)
    {
        return $this->user->updateUser($id, $data);
    }

    /**
     * @param string $code
     * @return ResponseDto
     * @throws ErrorResponseException
     * @throws ServiceErrorResponseException
     */
    public function activateAccount($code)
    {
        return $this->user->activateAccount($code);
    }

    /**
     * @param string $email
     * @return ResponseDto
     * @throws ErrorResponseException
     * @throws ServiceErrorResponseException
     */
    public function resendActivation($email)
    {
        return $this->user->resendActivation($email);
    }

    /**
     * @param string $email
     * @return ResponseDto
     * @throws ErrorResponseException
     * @throws ServiceErrorResponseException
     */
    public function forgottenPassword($email)
    {
        return $this->user->forgottenPassword($email);
    }

    /**
     * @param string $code
     * @param string $password
     * @return ResponseDto
     * @throws ErrorResponseException
     * @throws ServiceErrorResponseException
     */
    public function resetPassword($code, $password)
    {
        return $this->user->resetPassword($code, $password);
    }

    /**
     * @param string $email
     * @param string $password
     * @return ResponseDto
     * @throws ErrorResponseException
     * @throws ServiceErrorResponseException
     */
    public function login($email, $password)
    {
        return $this->user->login($email, $password);
    }

    /**
     * @param int $userId
     * @return PaginatedResponseDto
     * @throws ErrorResponseException
     * @throws ServiceErrorResponseException
     */
    public function getFavourites($userId)
    {
        return $this->user->getFavourites($userId);
    }

    /**
     * @param int $userId
     * @param int $productId
     * @return PaginatedResponseDto
     * @throws ErrorResponseException
     * @throws ServiceErrorResponseException
     */
    public function addProductToFavourites($userId, $productId)
    {
        return $this->user->addProductToFavourites($userId, $productId);
    }

    /**
     * @param int $userId
     * @param int $productId
     * @return PaginatedResponseDto
     * @throws ErrorResponseException
     * @throws ServiceErrorResponseException
     */
    public function removeProductFromFavourites($userId, $productId)
    {
        return $this->user->removeProductFromFavourites($userId, $productId);
    }

    /**
     * @param int $userId
     * @return PaginatedResponseDto
     * @throws ErrorResponseException
     * @throws ServiceErrorResponseException
     */
    public function getUserProductRecommendations($userId)
    {
        return $this->user->getUserProductRecommendations($userId);
    }

    /**
     * @param int $id
     * @return ResponseDto
     * @throws ErrorResponseException
     * @throws ServiceErrorResponseException
     */
    public function getCustomerById($id)
    {
        return $this->customer->getCustomerById($id);
    }

    /**
     * @param array $data
     * @return ResponseDto
     * @throws ErrorResponseException
     * @throws ServiceErrorResponseException
     */
    public function createCustomer(array $data)
    {
        return $this->customer->createCustomer($data);
    }

    /**
     * @param int $id
     * @param array $data
     * @return ResponseDto
     * @throws ErrorResponseException
     * @throws ServiceErrorResponseException
     */
    public function updateCustomer($id, array $data)
    {
        return $this->customer->updateCustomer($id, $data);
    }

    /**
     * @param int $id
     * @return PaginatedResponseDto
     * @throws ErrorResponseException
     * @throws ServiceErrorResponseException
     */
    public function getCustomerOrders($id)
    {
        return $this->customer->getCustomerOrders($id);
    }

    /**
     * @param int $id
     * @return ResponseDto
     * @throws ErrorResponseException
     * @throws ServiceErrorResponseException
     */
    public function getOrderById($id)
    {
        return $this->order->getOrderById($id);
    }

    /**
     * @param string $code
     * @return ResponseDto
     * @throws ErrorResponseException
     * @throws ServiceErrorResponseException
     */
    public function getOrderByCode($code)
    {
        return $this->order->getOrderByCode($code);
    }

    /**
     * @param null $customerId
     * @param null $basketReference
     * @param null $paymentGatewayId
     * @param null $termsId
     * @param null $partnerReference
     * @param array|null $productData
     * @param array|null $customerData
     * @return ResponseDto
     * @throws ErrorResponseException
     * @throws ServiceErrorResponseException
     */
    public function createOrder(
        $customerId = null,
        $basketReference = null,
        $paymentGatewayId = null,
        $termsId = null,
        $partnerReference = null,
        array $productData = null,
        array $customerData = null
    )
    {
        return $this->order->createOrder($customerId, $basketReference, $paymentGatewayId, $termsId, $partnerReference, $productData, $customerData);
    }

    /**
     * @param int $id
     * @param string $transactionReference
     * @param array $trace
     * @return ResponseDto
     * @throws ErrorResponseException
     * @throws ServiceErrorResponseException
     */
    public function completeOrder($id, $transactionReference, array $trace = [])
    {
        return $this->order->completeOrder($id, $transactionReference, $trace);
    }

    /**
     * @param int $id
     * @param array $trace
     * @return ResponseDto
     * @throws ErrorResponseException
     * @throws ServiceErrorResponseException
     */
    public function failedOrder($id, array $trace = [])
    {
        return $this->order->failedOrder($id, $trace);
    }

    /**
     * @param int $id
     * @return ResponseDto
     * @throws ErrorResponseException
     * @throws ServiceErrorResponseException
     */
    public function payOrder($id)
    {
        return $this->order->payOrder($id);
    }

    /**
     * @param PaginationDto|null $paginationDto
     * @param FilterCollectionDto|null $filterCollectionDto
     * @param SortCollectionDto|null $sortCollectionDto
     * @return PaginatedResponseDto
     * @throws ErrorResponseException
     * @throws ServiceErrorResponseException
     */
    public function getDeliveryTypes(
        PaginationDto $paginationDto = null,
        FilterCollectionDto $filterCollectionDto = null,
        SortCollectionDto $sortCollectionDto = null
    )
    {
        return $this->order->getDeliveryTypes($paginationDto, $filterCollectionDto, $sortCollectionDto);
    }

    /**
     * @param PaginationDto|null $paginationDto
     * @param FilterCollectionDto|null $filterCollectionDto
     * @param SortCollectionDto|null $sortCollectionDto
     * @return PaginatedResponseDto
     * @throws ErrorResponseException
     * @throws ServiceErrorResponseException
     */
    public function getTags(
        PaginationDto $paginationDto = null,
        FilterCollectionDto $filterCollectionDto = null,
        SortCollectionDto $sortCollectionDto = null
    )
    {
        return $this->tag->getTags($paginationDto, $filterCollectionDto, $sortCollectionDto);
    }

    /**
     * @param string $slug
     * @return ResponseDto
     * @throws ErrorResponseException
     * @throws ServiceErrorResponseException
     */
    public function getTagBySlug($slug)
    {
        return $this->tag->getTagBySlug($slug);
    }

    /**
     * @param PaginationDto|null $paginationDto
     * @param FilterCollectionDto|null $filterCollectionDto
     * @param SortCollectionDto|null $sortCollectionDto
     * @return PaginatedResponseDto
     * @throws ErrorResponseException
     * @throws ServiceErrorResponseException
     */
    public function getProductGroups(
        PaginationDto $paginationDto = null,
        FilterCollectionDto $filterCollectionDto = null,
        SortCollectionDto $sortCollectionDto = null
    )
    {
        return $this->group->getProductGroups($paginationDto, $filterCollectionDto, $sortCollectionDto);
    }

    /**
     * @param int $id
     * @return ResponseDto
     * @throws ErrorResponseException
     * @throws ServiceErrorResponseException
     */
    public function getProductGroupById($id)
    {
        return $this->group->getProductGroupById($id);
    }

    /**
     * @param string $slug
     * @return ResponseDto
     * @throws ErrorResponseException
     * @throws ServiceErrorResponseException
     */
    public function getProductGroupBySlug($slug)
    {
        return $this->group->getProductGroupBySlug($slug);
    }

    /**
     * @param array $data
     * @return PaginatedResponseDto
     * @throws ErrorResponseException
     * @throws ServiceErrorResponseException
     */
    public function getTerms(
        PaginationDto $paginationDto = null,
        FilterCollectionDto $filterCollectionDto = null,
        SortCollectionDto $sortCollectionDto = null
    )
    {
        return $this->term->getTerms($paginationDto, $filterCollectionDto, $sortCollectionDto);
    }

    /**
     * @param PaginationDto|null $paginationDto
     * @param FilterCollectionDto|null $filterCollectionDto
     * @param SortCollectionDto|null $sortCollectionDto
     * @return PaginatedResponseDto
     * @throws ErrorResponseException
     * @throws ServiceErrorResponseException
     */
    public function getPages(
        PaginationDto $paginationDto = null,
        FilterCollectionDto $filterCollectionDto = null,
        SortCollectionDto $sortCollectionDto = null
    )
    {
        return $this->page->getPages($paginationDto, $filterCollectionDto, $sortCollectionDto);
    }

    /**
     * @param int $id
     * @return ResponseDto
     * @throws ErrorResponseException
     * @throws ServiceErrorResponseException
     */
    public function getPageById($id)
    {
        return $this->page->getPageById($id);
    }

    /**
     * @param string $slug
     * @return ResponseDto
     * @throws ErrorResponseException
     * @throws ServiceErrorResponseException
     */
    public function getPageBySlug($slug)
    {
        return $this->page->getPageBySlug($slug);
    }

    /**
     * @param PaginationDto|null $paginationDto
     * @param FilterCollectionDto|null $filterCollectionDto
     * @param SortCollectionDto|null $sortCollectionDto
     * @return PaginatedResponseDto
     * @throws ErrorResponseException
     * @throws ServiceErrorResponseException
     */
    public function getSites(
        PaginationDto $paginationDto = null,
        FilterCollectionDto $filterCollectionDto = null,
        SortCollectionDto $sortCollectionDto = null
    )
    {
        return $this->site->getSites($paginationDto, $filterCollectionDto, $sortCollectionDto);
    }

    /**
     * @param int $id
     * @return ResponseDto
     * @throws ErrorResponseException
     * @throws ServiceErrorResponseException
     */
    public function getSiteById($id)
    {
        return $this->site->getSiteById($id);
    }

    /**
     * @param string $slug
     * @return ResponseDto
     * @throws ErrorResponseException
     * @throws ServiceErrorResponseException
     */
    public function getSiteBySlug($slug)
    {
        return $this->site->getSiteBySlug($slug);
    }

    /**
     * @return ResponseDto
     * @throws ErrorResponseException
     * @throws ServiceErrorResponseException
     */
    public function getCountries()
    {
        return $this->country->getCountries();
    }

    /**
     * @param array $data
     * @return ResponseDto
     * @throws ErrorResponseException
     * @throws ServiceErrorResponseException
     */
    public function createPartner(array $data)
    {
        return $this->partner->createPartner($data);
    }
}
