<?php

namespace Sugarcoat\APIWrapper\Config;

use Sugarcoat\APIWrapper\Exception\ConfigException;

class Config
{
    /**
     * @var array
     */
    protected $config = [];

    /**
     * Config constructor.
     * @param array $config
     */
    public function __construct(array $config = [])
    {
        $this->config = $config;
    }

    /**
     * @return string
     */
    public function getApiBaseUrl()
    {
        return $this->getConfig('api_base_url');
    }

    /**
     * @return string
     */
    public function getApiKey()
    {
        return $this->getConfig('api_key');
    }

    /**
     * @param $key
     * @return mixed
     * @throws ConfigException
     */
    private function getConfig($key)
    {
        if (!isset($this->config[$key])) {
            throw new ConfigException(self::class . ' - missing key {$key}');
        }

        return $this->config[$key];
    }
}
