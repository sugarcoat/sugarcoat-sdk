<?php

namespace Sugarcoat\APIWrapper\Constant;

class BaseConstant
{
    /**
     * @param string $value
     * @return bool
     */
    public static function validate($value)
    {
        $reflectionClass = new \ReflectionClass(get_called_class());

        foreach ($reflectionClass->getConstants() as $constantKey => $constantValue) {
            if ($value === $constantValue) {
                return true;
            }
        }

        return false;
    }
}