<?php

namespace Sugarcoat\APIWrapper\Constant;

class HttpMethod extends BaseConstant
{
    const HTTP_GET = 'GET';
    const HTTP_POST = 'POST';
    const HTTP_PUT = 'PUT';
    const HTTP_DELETE = 'DELETE';
    const HTTP_PATCH = 'PATCH';
}