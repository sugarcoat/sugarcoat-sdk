<?php

namespace Sugarcoat\APIWrapper\Constant;

class ProductRecommendationType extends BaseConstant
{
    const ALL = "all";
    const RELATED = "related";
    const TAGS  = "tags";
    const GROUPS  = "groups";
    const CATEGORIES  = "categories";
}