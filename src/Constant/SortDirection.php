<?php


namespace Sugarcoat\APIWrapper\Constant;

class SortDirection extends BaseConstant
{
    const SORT_ASCENDING = 'asc';
    const SORT_DESCENDING = 'desc';
}