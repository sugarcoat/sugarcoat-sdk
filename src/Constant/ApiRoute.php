<?php

namespace Sugarcoat\APIWrapper\Constant;

class ApiRoute extends BaseConstant
{
    const ROUTE_ANALYTIC_LIST = 'analytics';
    const ROUTE_AUTH_TOKEN_LIST = 'auth/token';
    const ROUTE_AUTH_TOKEN_SINGLE = 'auth/token/{token}';
    const ROUTE_BASKET_DISCOUNT_CODE_SINGLE = 'baskets/{token}/discount-codes/{discount_code_id}';
    const ROUTE_BASKET_DISCOUNT_CODE = 'baskets/{token}/discount-codes';
    const ROUTE_BASKET_PRODUCT_SINGLE = 'baskets/{token}/products/{product_id}';
    const ROUTE_BASKET_LIST = 'baskets';
    const ROUTE_BASKET_SINGLE = 'baskets/{token}';
    const ROUTE_BASKET_DELIVERY_TYPES = 'baskets/{token}/delivery-types';
    const ROUTE_BASKET_DELIVERY_TYPES_SINGLE = 'baskets/{token}/delivery-types/{delivery_type_id}';
    const ROUTE_COUNTRY_LIST = 'countries';
    const ROUTE_CATEGORY_LIST = 'categories';
    const ROUTE_CATEGORY_OPTION_LIST = 'category-options';
    const ROUTE_CATEGORY_OPTION_SINGLE = 'category-options/{id}';
    const ROUTE_CATEGORY_SINGLE = 'categories/{id}';
    const ROUTE_CUSTOMER_LIST = 'customers';
    const ROUTE_CUSTOMER_ORDER_LIST = 'customers/{id}/orders';
    const ROUTE_CUSTOMER_SINGLE = 'customers/{id}';
    const ROUTE_DELIVERY_TYPE_LIST = 'delivery-types';
    const ROUTE_DISCOUNT_CODE_SINGLE = 'discount-codes/{code}';
    const ROUTE_ORDER_ACTION_COMPLETE = 'orders/{id}/complete';
    const ROUTE_ORDER_ACTION_FAIL = 'orders/{id}/fail';
    const ROUTE_ORDER_ACTION_PAY = 'orders/{id}/pay';
    const ROUTE_ORDER_LIST = 'orders';
    const ROUTE_ORDER_SINGLE = 'orders/{id}';
    const ROUTE_PAGE_LIST = 'pages';
    const ROUTE_PAGE_SINGLE = 'pages/{id}';
    const ROUTE_SITE_LIST = 'sites';
    const ROUTE_SITE_SINGLE = 'sites/{id}';
    const ROUTE_PARTNER_LIST = 'partners';
    const ROUTE_PAYMENT_METHOD_LIST = 'payment-methods';
    const ROUTE_PRODUCT_GROUP_LIST = 'product-groups';
    const ROUTE_PRODUCT_GROUP_SINGLE = 'product-groups/{id}';
    const ROUTE_PRODUCT_LIST = 'products';
    const ROUTE_PRODUCT_RECOMMENDATION_LIST = 'products/recommendations';
    const ROUTE_PRODUCT_SINGLE = 'products/{id}';
    const ROUTE_PRODUCT_TYPE_LIST = 'product-types';
    const ROUTE_PRODUCT_TYPE_SINGLE = 'product-types/{id}';
    const ROUTE_SEARCH_PRODUCT_LIST = 'search/products';
    const ROUTE_STORE_LIST = 'stores';
    const ROUTE_TAG_LIST = 'tags';
    const ROUTE_TERM_LIST = 'terms';
    const ROUTE_USER_ACCOUNT_ACTIVATE = 'users/account/activate';
    const ROUTE_USER_ACCOUNT_FORGOTTEN_PASSWORD = 'users/account/forgotten-password';
    const ROUTE_USER_ACCOUNT_LOGIN = 'users/account/login';
    const ROUTE_USER_ACCOUNT_RESEND_ACTIVATION = 'users/account/resend-activation';
    const ROUTE_USER_ACCOUNT_RESET_PASSWORD = 'users/account/reset-password';
    const ROUTE_USER_FAVOURITE_LIST = 'users/{id}/favourites';
    const ROUTE_USER_FAVOURITE_SINGLE = 'users/{id}/favourites/{product_id}';
    const ROUTE_USER_LIST = 'users';
    const ROUTE_USER_RECOMMENDATION_LIST = 'users/{id}/recommendations';
    const ROUTE_USER_SINGLE = 'users/{id}';

    /**
     * @param $subject
     * @param array $arguments
     * @param string $prefix
     * @param string $suffix
     * @return mixed
     */
    public static function interpolate($subject, array $arguments = [], $prefix = "{", $suffix = "}")
    {
        foreach ($arguments as $key => $value) {
            $subject = str_replace($prefix . trim($key) . $suffix, trim($value), $subject);
        }

        return $subject;
    }
}