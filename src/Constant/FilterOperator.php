<?php

namespace Sugarcoat\APIWrapper\Constant;

class FilterOperator extends BaseConstant
{
    const OPERATOR_LIKE = 'LIKE';
    const OPERATOR_EQUALS = '=';
    const OPERATOR_NOT_EQUALS = '!=';
    const OPERATOR_GREATER_THAN = '>';
    const OPERATOR_WHERE = 'where';
    const OPERATOR_WHERE_IN = 'whereIn';
    const OPERATOR_WHERE_BETWEEN = 'whereBetween';
    const OPERATOR_WHERE_AND = 'whereAnd';
}