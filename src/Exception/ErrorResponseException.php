<?php

namespace Sugarcoat\APIWrapper\Exception;

use Psr\Http\Message\ResponseInterface;

class ErrorResponseException extends BaseException
{
    /**
     * @var ResponseInterface
     */
    protected $response;

    /**
     * @var array
     */
    protected $errors = [];

    /**
     * ApiErrorResponseException constructor.
     * @param ResponseInterface $response
     * @param array $errors
     * @param string $message
     * @param int $code
     * @param \Throwable|null $previous
     */
    public function __construct(ResponseInterface $response, array $errors = [], $message = "", $code = 0, \Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);

        $this->response = $response;
        $this->errors = $errors;
    }

    /**
     * @return ResponseInterface
     */
    public function getResponse()
    {
        return $this->response;
    }

    /**
     * @return array
     */
    public function getErrors()
    {
        return $this->errors;
    }
}