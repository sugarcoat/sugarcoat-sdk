<?php

namespace Sugarcoat\APIWrapper\Exception;

use Psr\Http\Message\ResponseInterface;

class ServiceErrorResponseException extends BaseException
{
    /**
     * @var ResponseInterface
     */
    protected $response;

    /**
     * ApiErrorResponseException constructor.
     * @param ResponseInterface $response
     * @param array $errors
     * @param string $message
     * @param int $code
     * @param \Throwable|null $previous
     */
    public function __construct(ResponseInterface $response, $message = "", $code = 0, \Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);

        $this->response = $response;
    }

    /**
     * @return ResponseInterface
     */
    public function getResponse()
    {
        return $this->response;
    }
}