<?php

namespace Sugarcoat\APIWrapper\Dto;

class PaginatedResponseDto extends ResponseDto
{
    /**
     * @var int
     */
    protected $count = 0;

    /**
     * @var int
     */
    protected $interval = 0;

    /**
     * @var int
     */
    protected $currentPage = 1;

    /**
     * @var int
     */
    protected $lastPage = 1;

    /**
     * PaginatedResponseDto constructor.
     * @param array $headers
     * @param array $data
     * @param int $count
     * @param int $interval
     * @param int $currentPage
     * @param int $lastPage
     */
    public function __construct(array $headers, array $data, $count, $interval, $currentPage, $lastPage)
    {
        parent::__construct($headers, $data);

        $this->count = $count;
        $this->interval = $interval;
        $this->currentPage = $currentPage;
        $this->lastPage = $lastPage;
    }

    /**
     * @return int
     */
    public function getCount()
    {
        return $this->count;
    }

    /**
     * @return int
     */
    public function getInterval()
    {
        return $this->interval;
    }

    /**
     * @return int
     */
    public function getCurrentPage()
    {
        return $this->currentPage;
    }

    /**
     * @return int
     */
    public function getLastPage()
    {
        return $this->lastPage;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return array_merge([
            'count' => $this->getCount(),
            'interval' => $this->getInterval(),
            'current_page' => $this->getCurrentPage(),
            'last_page' => $this->getLastPage(),
        ], parent::toArray());
    }
}