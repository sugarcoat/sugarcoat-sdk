<?php

namespace Sugarcoat\APIWrapper\Dto;

use Sugarcoat\APIWrapper\Constant\FilterOperator;
use Sugarcoat\APIWrapper\Exception\InvalidDtoException;

class FilterDto extends BaseDto
{
    /**
     * @var string
     */
    protected $property;

    /**
     * @var mixed
     */
    protected $value;

    /**
     * @var string
     */
    protected $operator;

    /**
     * FilterDto constructor.
     * @param null $property
     * @param null $value
     * @param string $operator
     * @throws InvalidDtoException
     */
    public function __construct($property = null, $value = null, $operator = FilterOperator::OPERATOR_EQUALS)
    {
        if (null !== $operator && !FilterOperator::validate($operator)) {
            throw new InvalidDtoException(sprintf("Filter operator '%s' not supported", $operator));
        }

        $this->property = $property;
        $this->value = $value;
        $this->operator = $operator;
    }

    /**
     * @return string
     */
    public function getProperty()
    {
        return $this->property;
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @return string
     */
    public function getOperator()
    {
        return $this->operator;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return [
            'property' => $this->getProperty(),
            'value' => $this->getValue(),
            'operator' => $this->getOperator()
        ];
    }
}