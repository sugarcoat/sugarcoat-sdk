<?php

namespace Sugarcoat\APIWrapper\Dto;

abstract class BaseDto implements \JsonSerializable
{
    /**
     * @return array
     */
    abstract function toArray();

    /**
     * @return array|mixed
     */
    public function jsonSerialize()
    {
        return $this->toArray();
    }
}