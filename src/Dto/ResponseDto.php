<?php

namespace Sugarcoat\APIWrapper\Dto;

class ResponseDto extends BaseDto
{
    /**
     * @var array
     */
    protected $headers = [];

    /**
     * @var array
     */
    protected $data = [];

    /**
     * ResponseDto constructor.
     * @param array $headers
     * @param array $data
     */
    public function __construct(array $headers, array $data)
    {
        $this->headers = $headers;
        $this->data = $data;
    }

    /**
     * @return array
     */
    public function getHeaders(): array
    {
        return $this->headers;
    }

    /**
     * @return array
     */
    public function getData(): array
    {
        return $this->data;
    }

    /**
     * @return array
     */
    function toArray()
    {
        return [
            'headers' => $this->getHeaders(),
            'data' => $this->getData()
        ];
    }
}