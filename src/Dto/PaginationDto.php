<?php

namespace Sugarcoat\APIWrapper\Dto;

class PaginationDto extends BaseDto
{
    /**
     * @var int
     */
    protected $pageNumber = 0;

    /**
     * @var int
     */
    protected $interval = 0;

    /**
     * PaginationDto constructor.
     * @param $pageNumber
     * @param $interval
     */
    public function __construct($pageNumber, $interval)
    {
        $this->pageNumber = $pageNumber;
        $this->interval = $interval;
    }

    /**
     * @return string
     */
    public function getPageNumber()
    {
        return $this->pageNumber;
    }

    /**
     * @return mixed
     */
    public function getInterval()
    {
        return $this->interval;
    }

    /**
     * @return array
     */
    function toArray()
    {
        return [
            'page' => $this->getPageNumber(),
            'pagination_interval' => $this->getInterval()
        ];
    }
}