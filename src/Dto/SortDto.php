<?php


namespace Sugarcoat\APIWrapper\Dto;

use Sugarcoat\APIWrapper\Constant\SortDirection;
use Sugarcoat\APIWrapper\Exception\InvalidDtoException;

class SortDto extends BaseDto
{
    /**
     * @var string
     */
    protected $property;

    /**
     * @var
     */
    protected $direction;

    /**
     * SortDto constructor.
     * @param string $property
     * @param $direction
     */
    public function __construct($property, $direction = SortDirection::SORT_ASCENDING)
    {
        if (null !== $direction && !SortDirection::validate($direction)) {
            throw new InvalidDtoException(sprintf("Sort direction '%s' not supported", $direction));
        }
        $this->property = $property;
        $this->direction = $direction;
    }

    /**
     * @return string
     */
    public function getProperty()
    {
        return $this->property;
    }

    /**
     * @return mixed
     */
    public function getDirection()
    {
        return $this->direction;
    }

    /**
     * @return array
     */
    function toArray()
    {
        return [
            'property' => $this->getProperty(),
            'direction' => $this->getDirection()
        ];
    }
}