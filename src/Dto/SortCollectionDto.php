<?php

namespace Sugarcoat\APIWrapper\Dto;

class SortCollectionDto extends BaseDto
{
    /**
     * @var SortDto[]
     */
    protected $sortDtos = [];

    /**
     * SortCollectionDto constructor.
     * @param SortDto[] $sortDtos
     */
    public function __construct(array $sortDtos = [])
    {
        foreach ($sortDtos as $sortDto) {
            $this->addSortDto($sortDto);
        }
    }

    /**
     * @param SortDto $sortDto
     * @return $this
     */
    public function addSortDto(SortDto $sortDto)
    {
        $this->sortDtos[] = $sortDto;
        return $this;
    }

    /**
     * @return SortDto[]
     */
    public function getSortDtos()
    {
        return $this->sortDtos;
    }

    /**
     * @return array
     */
    function toArray()
    {
        $sorting = array_map(function ($sortDto) {
            return $sortDto->toArray();
        }, $this->getSortDtos());

        return [
            'sort' => $sorting
        ];
    }
}