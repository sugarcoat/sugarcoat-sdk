<?php

namespace Sugarcoat\APIWrapper\Dto;

class FilterCollectionDto extends BaseDto
{
    /**
     * @var FilterDto[]
     */
    protected $filterDtos = [];

    /**
     * FilterCollectionDto constructor.
     * @param FilterDto[] $filterDtos
     */
    public function __construct(array $filterDtos = [])
    {
        foreach ($filterDtos as $filterDto) {
            $this->addFilterDto($filterDto);
        }
    }

    /**
     * @param FilterDto $filterDto
     * @return $this
     */
    public function addFilterDto(FilterDto $filterDto)
    {
        $this->filterDtos[] = $filterDto;
        return $this;
    }

    /**
     * @return FilterDto[]
     */
    public function getFilterDtos()
    {
        return $this->filterDtos;
    }

    /**
     * @return array
     */
    function toArray()
    {
        $filters = array_map(function ($filterDto) {
            return $filterDto->toArray();
        }, $this->getFilterDtos());

        return [
            'filters' => $filters
        ];
    }
}