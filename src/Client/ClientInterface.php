<?php

namespace Sugarcoat\APIWrapper\Client;

use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\ResponseInterface;
use Sugarcoat\APIWrapper\Exception\ErrorResponseException;
use Sugarcoat\APIWrapper\Exception\ServiceErrorResponseException;

interface ClientInterface
{
    /**
     * @param ServerRequestInterface $request
     * @return ResponseInterface
     * @throws ErrorResponseException
     * @throws ServiceErrorResponseException
     */
    public function sendRequest(ServerRequestInterface $request);

    /**
     * @param \Exception $exception
     * @param ResponseInterface $response
     * @throws ErrorResponseException
     */
    public function handleErrorResponse(\Exception $exception, ResponseInterface $response);

    /**
     * @param \Exception $exception
     * @param ResponseInterface|null $response
     * @throws ServiceErrorResponseException
     */
    public function handleServiceErrorResponse(\Exception $exception, ResponseInterface $response = null);
}