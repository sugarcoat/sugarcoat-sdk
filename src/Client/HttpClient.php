<?php

namespace Sugarcoat\APIWrapper\Client;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Psr7\Response;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\ResponseInterface;
use Sugarcoat\APIWrapper\Config\Config;
use Sugarcoat\APIWrapper\Constant\HttpMethod;
use Sugarcoat\APIWrapper\Exception\ErrorResponseException;
use Sugarcoat\APIWrapper\Exception\ServiceErrorResponseException;

class HttpClient implements ClientInterface
{
    /**
     * @var Client
     */
    protected $client;

    /**
     * @var Config
     */
    protected $config;

    /**
     * HttpClient constructor.
     * @param Config $config
     * @param array $defaults
     * @param Client|null $client
     */
    public function __construct(Config $config, $defaults = [], Client $client = null)
    {
        $this->config = $config;

        if (null === $client) {
            $client = new Client(array_merge([
                'base_uri' => $this->config->getApiBaseUrl()
            ], $defaults));
        }

        $this->client = $client;
    }


    /**
     * @inheritDoc
     */
    public function sendRequest(ServerRequestInterface $request)
    {
        try {
            if ($request->getMethod() === HttpMethod::HTTP_GET) {
                return $this->client->send($request, [
                        'query' => $request->getServerParams()
                    ]
                );
            }

            return $this->client->send($request, [
                    'json' => $request->getServerParams()
                ]
            );
        } catch (ClientException $exception) {
            // We've hit rate limits so lets attempt a roll off
            if($exception->getResponse()->getStatusCode() === 429){
                if(count($exception->getResponse()->getHeader('Retry-After')) > 0) {
                    sleep((int)$exception->getResponse()->getHeader('Retry-After')[0]);
                    return $this->sendRequest($request);
                }
            }

            $this->handleErrorResponse($exception, $exception->getResponse());
        } catch (GuzzleException $exception) {
            $this->handleServiceErrorResponse($exception, $exception->getResponse());
        } catch (\Exception $exception) {
            $this->handleServiceErrorResponse($exception, null);
        }
    }

    /**
     * @inheritDoc
     */
    public function handleErrorResponse(\Exception $exception, ResponseInterface $response)
    {
        $errors = json_decode($response->getBody()->getContents(), true);

        throw new ErrorResponseException($response, $errors, $exception->getMessage(), $exception->getCode(), $exception);
    }

    /**
     * @inheritDoc
     */
    public function handleServiceErrorResponse(\Exception $exception, ResponseInterface $response = null)
    {
        if (null === $response) {
            $response = new Response(500, [], 'Unknown API Error');
        }

        throw new ServiceErrorResponseException($response, $exception->getMessage(), $exception->getCode(), $exception);
    }
}