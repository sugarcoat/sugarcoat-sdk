<?php

namespace Sugarcoat\APIWrapper\Wrapper;

use Psr\Http\Message\ServerRequestInterface;
use Sugarcoat\APIWrapper\Client\ClientInterface;
use Sugarcoat\APIWrapper\Config\Config;
use Sugarcoat\APIWrapper\Exception\ErrorResponseException;
use Sugarcoat\APIWrapper\Exception\ServiceErrorResponseException;

class BaseWrapper
{
    /**
     * @var ClientInterface
     */
    protected $client;

    /**
     * @var Config
     */
    protected $config;

    /**
     * BaseWrapper constructor.
     * @param Config|null $config
     * @param null|ClientInterface $client
     */
    public function __construct(Config $config, ClientInterface $client)
    {
        $this->config = $config;
        $this->client = $client;
    }

    /**
     * @param ServerRequestInterface $request
     * @return \Psr\Http\Message\ResponseInterface
     * @throws ErrorResponseException
     * @throws ServiceErrorResponseException
     */
    public function sendRequest(ServerRequestInterface $request)
    {
        $request = $request->withAddedHeader('Authorization', $this->config->getApiKey());

        return $this->client->sendRequest($request);
    }

    /**
     * @return string
     */
    public function getApiBaseUrl()
    {
        return $this->config->getApiBaseUrl();
    }
}
