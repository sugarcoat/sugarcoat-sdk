<?php

namespace Sugarcoat\APIWrapper\Wrapper;

use Sugarcoat\APIWrapper\Constant\ApiRoute;
use Sugarcoat\APIWrapper\Constant\HttpMethod;
use Sugarcoat\APIWrapper\Dto\PaginatedResponseDto;
use Sugarcoat\APIWrapper\Dto\ResponseDto;
use Sugarcoat\APIWrapper\Exception\ErrorResponseException;
use Sugarcoat\APIWrapper\Exception\ServiceErrorResponseException;
use Sugarcoat\APIWrapper\Factory\RequestFactory;
use Sugarcoat\APIWrapper\Factory\ResponseFactory;

class CustomerWrapper extends BaseWrapper
{
    /**
     * @param int $id
     * @return ResponseDto
     * @throws ErrorResponseException
     * @throws ServiceErrorResponseException
     */
    public function getCustomerById($id)
    {
        $request = RequestFactory::factory(
            ApiRoute::interpolate(
                ApiRoute::ROUTE_CUSTOMER_SINGLE, [
                    'id' => $id
                ]
            ),
            HttpMethod::HTTP_GET
        );

        $response = $this->sendRequest($request);

        return ResponseFactory::responseDtoFactory($response);
    }

    /**
     * @param array $data
     * @return ResponseDto
     * @throws ErrorResponseException
     * @throws ServiceErrorResponseException
     */
    public function createCustomer(array $data)
    {
        $request = RequestFactory::factory(
            ApiRoute::ROUTE_CUSTOMER_LIST,
            HttpMethod::HTTP_POST,
            $data
        );

        $response = $this->sendRequest($request);

        return ResponseFactory::responseDtoFactory($response);
    }

    /**
     * @param int $id
     * @param array $data
     * @return ResponseDto
     * @throws ErrorResponseException
     * @throws ServiceErrorResponseException
     */
    public function updateCustomer($id, array $data)
    {
        $request = RequestFactory::factory(
            ApiRoute::interpolate(
                ApiRoute::ROUTE_CUSTOMER_SINGLE, [
                    'id' => $id
                ]
            ),
            HttpMethod::HTTP_PUT,
            $data
        );

        $response = $this->sendRequest($request);

        return ResponseFactory::responseDtoFactory($response);
    }

    /**
     * @param int $id
     * @return PaginatedResponseDto
     * @throws ErrorResponseException
     * @throws ServiceErrorResponseException
     */
    public function getCustomerOrders($id)
    {
        $request = RequestFactory::factory(
            ApiRoute::interpolate(
                ApiRoute::ROUTE_CUSTOMER_ORDER_LIST, [
                    'id' => $id
                ]
            ),
            HttpMethod::HTTP_GET
        );

        $response = $this->sendRequest($request);

        return ResponseFactory::paginatedResponseDtoFactory($response);
    }
}