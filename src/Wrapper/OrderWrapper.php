<?php

namespace Sugarcoat\APIWrapper\Wrapper;

use Sugarcoat\APIWrapper\Constant\ApiRoute;
use Sugarcoat\APIWrapper\Constant\HttpMethod;
use Sugarcoat\APIWrapper\Constant\RelationshipIncludes;
use Sugarcoat\APIWrapper\Dto\FilterCollectionDto;
use Sugarcoat\APIWrapper\Dto\FilterDto;
use Sugarcoat\APIWrapper\Dto\PaginatedResponseDto;
use Sugarcoat\APIWrapper\Dto\PaginationDto;
use Sugarcoat\APIWrapper\Dto\ResponseDto;
use Sugarcoat\APIWrapper\Dto\SortCollectionDto;
use Sugarcoat\APIWrapper\Exception\ErrorResponseException;
use Sugarcoat\APIWrapper\Exception\ServiceErrorResponseException;
use Sugarcoat\APIWrapper\Factory\RequestFactory;
use Sugarcoat\APIWrapper\Factory\ResponseFactory;

class OrderWrapper extends BaseWrapper
{
    /**
     * @param int $id
     * @return ResponseDto
     * @throws ErrorResponseException
     * @throws ServiceErrorResponseException
     */
    public function getOrderById($id)
    {
        $request = RequestFactory::factory(
            ApiRoute::interpolate(
                ApiRoute::ROUTE_ORDER_SINGLE, [
                    'id' => $id
                ]
            ),
            HttpMethod::HTTP_GET
        );

        $response = $this->sendRequest($request);

        return ResponseFactory::responseDtoFactory($response);
    }

    /**
     * @param string $code
     * @return ResponseDto
     * @throws ErrorResponseException
     * @throws ServiceErrorResponseException
     */
    public function getOrderByCode($code)
    {
        $filter = new FilterDto('code', $code);

        $request = RequestFactory::factory(
            ApiRoute::ROUTE_ORDER_LIST,
            HttpMethod::HTTP_GET,
            [],
            null,
            new FilterCollectionDto([
                $filter
            ]),
            null,
            [RelationshipIncludes::ALL]
        );

        $response = $this->sendRequest($request);

        $paginatedResponse = ResponseFactory::paginatedResponseDtoFactory($response);

        // Force the collection into a ResponseDto
        $data = count($paginatedResponse->getData()) ? $paginatedResponse->getData()[0] : [];
        return new ResponseDto($paginatedResponse->getHeaders(), $data);
    }

    /**
     * @param null $customerId
     * @param null $basketReference
     * @param null $paymentGatewayId
     * @param null $termsId
     * @param null $partnerReference
     * @param array|null $productData
     * @param array|null $customerData
     * @return ResponseDto
     * @throws ErrorResponseException
     * @throws ServiceErrorResponseException
     */
    public function createOrder(
        $customerId = null,
        $basketReference = null,
        $paymentGatewayId = null,
        $termsId = null,
        $partnerReference = null,
        array $productData = null,
        array $customerData = null
    )
    {
        $request = RequestFactory::factory(
            ApiRoute::ROUTE_ORDER_LIST,
            HttpMethod::HTTP_POST,
            [
                'customer_id' => $customerId,
                'basket_reference' => $basketReference,
                'payment_gateway_id' => $paymentGatewayId,
                'terms_id' => $termsId,
                'partner_reference' => $partnerReference,
                'products' => $productData,
                'customer' => $customerData
            ]
        );

        $response = $this->sendRequest($request);

        return ResponseFactory::responseDtoFactory($response);
    }

    /**
     * @param int $id
     * @param string $transactionReference
     * @param array $trace
     * @return ResponseDto
     * @throws ErrorResponseException
     * @throws ServiceErrorResponseException
     */
    public function completeOrder($id, $transactionReference, array $trace = [])
    {
        $request = RequestFactory::factory(
            ApiRoute::interpolate(
                ApiRoute::ROUTE_ORDER_ACTION_COMPLETE, [
                    'id' => $id
                ]
            ),
            HttpMethod::HTTP_POST,
            [
                'transaction_reference' => $transactionReference,
                'trace' => $trace,
            ]
        );

        $response = $this->sendRequest($request);

        return ResponseFactory::responseDtoFactory($response);
    }

    /**
     * @param int $id
     * @param array $trace
     * @return ResponseDto
     * @throws ErrorResponseException
     * @throws ServiceErrorResponseException
     */
    public function failedOrder($id, array $trace = [])
    {
        $request = RequestFactory::factory(
            ApiRoute::interpolate(
                ApiRoute::ROUTE_ORDER_ACTION_FAIL, [
                    'id' => $id
                ]
            ),
            HttpMethod::HTTP_POST,
            [
                'trace' => $trace,
            ]
        );

        $response = $this->sendRequest($request);

        return ResponseFactory::responseDtoFactory($response);
    }

    /**
     * @param int $id
     * @return ResponseDto
     * @throws ErrorResponseException
     * @throws ServiceErrorResponseException
     */
    public function payOrder($id)
    {
        $request = RequestFactory::factory(
            ApiRoute::interpolate(
                ApiRoute::ROUTE_ORDER_ACTION_PAY, [
                    'id' => $id
                ]
            ),
            HttpMethod::HTTP_GET
        );

        $response = $this->sendRequest($request);

        return ResponseFactory::responseDtoFactory($response);
    }

    /**
     * @param PaginationDto|null $paginationDto
     * @param FilterCollectionDto|null $filterCollectionDto
     * @param SortCollectionDto|null $sortCollectionDto
     * @return PaginatedResponseDto
     * @throws ErrorResponseException
     * @throws ServiceErrorResponseException
     */
    public function getDeliveryTypes(
        PaginationDto $paginationDto = null,
        FilterCollectionDto $filterCollectionDto = null,
        SortCollectionDto $sortCollectionDto = null
    )
    {
        $request = RequestFactory::factory(
            ApiRoute::ROUTE_DELIVERY_TYPE_LIST,
            HttpMethod::HTTP_GET,
            [],
            $paginationDto,
            $filterCollectionDto,
            $sortCollectionDto
        );

        $response = $this->sendRequest($request);

        return ResponseFactory::paginatedResponseDtoFactory($response);
    }
}