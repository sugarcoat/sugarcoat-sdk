<?php

namespace Sugarcoat\APIWrapper\Wrapper;

use Sugarcoat\APIWrapper\Constant\ApiRoute;
use Sugarcoat\APIWrapper\Constant\HttpMethod;
use Sugarcoat\APIWrapper\Dto\ResponseDto;
use Sugarcoat\APIWrapper\Exception\ErrorResponseException;
use Sugarcoat\APIWrapper\Exception\ServiceErrorResponseException;
use Sugarcoat\APIWrapper\Factory\RequestFactory;
use Sugarcoat\APIWrapper\Factory\ResponseFactory;

class BasketWrapper extends BaseWrapper
{
    /**
     * @param string $basketToken
     * @return ResponseDto
     * @throws ErrorResponseException
     * @throws ServiceErrorResponseException
     */
    public function getBasket($basketToken)
    {
        $request = RequestFactory::factory(
            ApiRoute::interpolate(
                ApiRoute::ROUTE_BASKET_SINGLE, [
                    'token' => $basketToken
                ]
            ),
            HttpMethod::HTTP_GET
        );

        $response = $this->sendRequest($request);

        return ResponseFactory::responseDtoFactory($response);
    }

    /**
     * @param int|null $userId
     * @param array $basketData
     * @param $deliveryTypeId
     * @param int|null $customerId
     * @return ResponseDto
     * @throws ErrorResponseException
     * @throws ServiceErrorResponseException
     */
    public function createBasket($userId = null, array $basketData = [], $deliveryTypeId = null, $customerId = null)
    {
        $request = RequestFactory::factory(
            ApiRoute::ROUTE_BASKET_LIST,
            HttpMethod::HTTP_POST,
            [
                'user_id' => $userId,
                'products' => $basketData,
                'delivery_type_id' => $deliveryTypeId,
                'customer_id' => $customerId
            ]
        );

        $response = $this->sendRequest($request);

        return ResponseFactory::responseDtoFactory($response);
    }

    /**
     * @param string $basketToken
     * @param int|null $userId
     * @param array|null $basketData
     * @param $deliveryTypeId
     * @param int|null $customerId
     * @return ResponseDto
     * @throws ErrorResponseException
     * @throws ServiceErrorResponseException
     */
    public function updateBasket($basketToken, $userId = null, $basketData = null, $deliveryTypeId = null, $customerId = null)
    {
        $request = RequestFactory::factory(
            ApiRoute::interpolate(
                ApiRoute::ROUTE_BASKET_SINGLE, [
                    'token' => $basketToken
                ]
            ),
            HttpMethod::HTTP_PUT,
            [
                'user_id' => $userId,
                'products' => $basketData,
                'delivery_type_id' => $deliveryTypeId,
                'customer_id' => $customerId
            ]
        );

        $response = $this->sendRequest($request);

        return ResponseFactory::responseDtoFactory($response);
    }

    /**
     * @param string $basketToken
     * @return ResponseDto
     * @throws ErrorResponseException
     * @throws ServiceErrorResponseException
     */
    public function clearBasket($basketToken)
    {
        $request = RequestFactory::factory(
            ApiRoute::interpolate(
                ApiRoute::ROUTE_BASKET_SINGLE, [
                    'token' => $basketToken
                ]
            ),
            HttpMethod::HTTP_DELETE
        );

        $response = $this->sendRequest($request);

        return ResponseFactory::responseDtoFactory($response);
    }

    /**
     * @param string $basketToken
     * @param int $productId
     * @param int $quantity
     * @param int|null $deliveryTypeId
     * @return ResponseDto
     * @throws ErrorResponseException
     * @throws ServiceErrorResponseException
     */
    public function addProductToBasket($basketToken, $productId, $quantity, $deliveryTypeId = null)
    {
        $request = RequestFactory::factory(
            ApiRoute::interpolate(
                ApiRoute::ROUTE_BASKET_PRODUCT_SINGLE, [
                    'token' => $basketToken,
                    'product_id' => $productId
                ]
            ),
            HttpMethod::HTTP_PUT,
            [
                'quantity' => $quantity,
                'delivery_type_id' => $deliveryTypeId
            ]
        );

        $response = $this->sendRequest($request);

        return ResponseFactory::responseDtoFactory($response);
    }

    /**
     * @param string $basketToken
     * @param int $productId
     * @return ResponseDto
     * @throws ErrorResponseException
     * @throws ServiceErrorResponseException
     */
    public function removeProductFromBasket($basketToken, $productId)
    {
        $request = RequestFactory::factory(
            ApiRoute::interpolate(
                ApiRoute::ROUTE_BASKET_PRODUCT_SINGLE, [
                    'token' => $basketToken,
                    'product_id' => $productId
                ]
            ),
            HttpMethod::HTTP_DELETE
        );

        $response = $this->sendRequest($request);

        return ResponseFactory::responseDtoFactory($response);
    }

    /**
     * @param string $code
     * @return ResponseDto
     * @throws ErrorResponseException
     * @throws ServiceErrorResponseException
     */
    public function getDiscountCode($code)
    {
        $request = RequestFactory::factory(
            ApiRoute::interpolate(
                ApiRoute::ROUTE_DISCOUNT_CODE_SINGLE, [
                    'code' => $code,
                ]
            ),
            HttpMethod::HTTP_GET
        );

        $response = $this->sendRequest($request);

        return ResponseFactory::responseDtoFactory($response);
    }

    /**
     * @param string $basketToken
     * @param int $discountCodeId
     * @return ResponseDto
     * @throws ErrorResponseException
     * @throws ServiceErrorResponseException
     */
    public function applyDiscountToBasket($basketToken, $discountCodeId)
    {
        $request = RequestFactory::factory(
            ApiRoute::interpolate(
                ApiRoute::ROUTE_BASKET_DISCOUNT_CODE_SINGLE, [
                    'token' => $basketToken,
                    'discount_code_id' => $discountCodeId
                ]
            ),
            HttpMethod::HTTP_PUT
        );

        $response = $this->sendRequest($request);

        return ResponseFactory::responseDtoFactory($response);
    }

    /**
     * @param $basketToken
     * @param $deliveryTypeId
     * @return ResponseDto
     * @throws ErrorResponseException
     * @throws ServiceErrorResponseException
     */
    public function applyDeliveryToBasket($basketToken, $deliveryTypeId)
    {
        $request = RequestFactory::factory(
            ApiRoute::interpolate(
                ApiRoute::ROUTE_BASKET_DELIVERY_TYPES_SINGLE, [
                    'token' => $basketToken,
                    'delivery_type_id' => $deliveryTypeId
                ]
            ),
            HttpMethod::HTTP_PUT
        );

        $response = $this->sendRequest($request);

        return ResponseFactory::responseDtoFactory($response);
    }

    /**
     * @param $basketToken
     * @return ResponseDto
     * @throws ErrorResponseException
     * @throws ServiceErrorResponseException
     */
    public function removeDiscountCodesFromBasket($basketToken)
    {
        $request = RequestFactory::factory(
            ApiRoute::interpolate(
                ApiRoute::ROUTE_BASKET_DISCOUNT_CODE, [
                    'token' => $basketToken
                ]
            ),
            HttpMethod::HTTP_DELETE
        );

        $response = $this->sendRequest($request);

        return ResponseFactory::responseDtoFactory($response);
    }

    /**
     * @param string $basketToken
     * @return ResponseDto
     * @throws ErrorResponseException
     * @throws ServiceErrorResponseException
     */
    public function getBasketDeliveryTypes($basketToken)
    {
        $request = RequestFactory::factory(
            ApiRoute::interpolate(
                ApiRoute::ROUTE_BASKET_DELIVERY_TYPES, [
                    'token' => $basketToken
                ]
            ),
            HttpMethod::HTTP_GET
        );

        $response = $this->sendRequest($request);

        return ResponseFactory::responseDtoFactory($response);
    }
}