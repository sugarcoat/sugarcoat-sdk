<?php

namespace Sugarcoat\APIWrapper\Wrapper;

use Sugarcoat\APIWrapper\Constant\ApiRoute;
use Sugarcoat\APIWrapper\Constant\HttpMethod;
use Sugarcoat\APIWrapper\Dto\ResponseDto;
use Sugarcoat\APIWrapper\Exception\ErrorResponseException;
use Sugarcoat\APIWrapper\Exception\ServiceErrorResponseException;
use Sugarcoat\APIWrapper\Factory\RequestFactory;
use Sugarcoat\APIWrapper\Factory\ResponseFactory;

class AuthWrapper extends BaseWrapper
{
    /**
     * string $refreshToken
     * @return ResponseDto
     * @throws ErrorResponseException
     * @throws ServiceErrorResponseException
     */
    public function getToken($refreshToken = null)
    {
        $params = [];

        if($refreshToken !== null){
            $params['refresh_token'] = $refreshToken;
        }

        $request = RequestFactory::factory(
            ApiRoute::ROUTE_AUTH_TOKEN_LIST,
            HttpMethod::HTTP_GET,
            $params
        );

        $response = $this->sendRequest($request);

        return ResponseFactory::responseDtoFactory($response);
    }

    /**
     * @param string $email
     * @param string $password
     * @return ResponseDto
     * @throws ErrorResponseException
     * @throws ServiceErrorResponseException
     */
    public function createUserToken($email, $password)
    {
        $request = RequestFactory::factory(
            ApiRoute::ROUTE_AUTH_TOKEN_LIST,
            HttpMethod::HTTP_POST,
            [
                'email' => $email,
                'password' => $password,
            ]
        );

        $response = $this->sendRequest($request);

        return ResponseFactory::responseDtoFactory($response);
    }

    /**
     * @param string $token
     * @return ResponseDto
     * @throws ErrorResponseException
     * @throws ServiceErrorResponseException
     */
    public function getTokenByToken($token)
    {
        $request = RequestFactory::factory(
            ApiRoute::interpolate(
                ApiRoute::ROUTE_AUTH_TOKEN_SINGLE, [
                    'token' => $token
                ]
            ),
            HttpMethod::HTTP_GET
        );

        $response = $this->sendRequest($request);

        return ResponseFactory::responseDtoFactory($response);
    }
}