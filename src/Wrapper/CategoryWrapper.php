<?php

namespace Sugarcoat\APIWrapper\Wrapper;

use Sugarcoat\APIWrapper\Constant\ApiRoute;
use Sugarcoat\APIWrapper\Constant\HttpMethod;
use Sugarcoat\APIWrapper\Constant\RelationshipIncludes;
use Sugarcoat\APIWrapper\Dto\FilterCollectionDto;
use Sugarcoat\APIWrapper\Dto\FilterDto;
use Sugarcoat\APIWrapper\Dto\PaginatedResponseDto;
use Sugarcoat\APIWrapper\Dto\PaginationDto;
use Sugarcoat\APIWrapper\Dto\ResponseDto;
use Sugarcoat\APIWrapper\Dto\SortCollectionDto;
use Sugarcoat\APIWrapper\Exception\ErrorResponseException;
use Sugarcoat\APIWrapper\Exception\ServiceErrorResponseException;
use Sugarcoat\APIWrapper\Factory\RequestFactory;
use Sugarcoat\APIWrapper\Factory\ResponseFactory;

class CategoryWrapper extends BaseWrapper
{
    /**
     * @param PaginationDto|null $paginationDto
     * @param FilterCollectionDto|null $filterCollectionDto
     * @param SortCollectionDto|null $sortCollectionDto
     * @return PaginatedResponseDto
     * @throws ErrorResponseException
     * @throws ServiceErrorResponseException
     */
    public function getCategories(
        PaginationDto $paginationDto = null,
        FilterCollectionDto $filterCollectionDto = null,
        SortCollectionDto $sortCollectionDto = null
    )
    {
        $request = RequestFactory::factory(
            ApiRoute::ROUTE_CATEGORY_LIST,
            HttpMethod::HTTP_GET,
            [],
            $paginationDto,
            $filterCollectionDto,
            $sortCollectionDto
        );

        $response = $this->sendRequest($request);

        return ResponseFactory::paginatedResponseDtoFactory($response);
    }

    /**
     * @param int $id
     * @return ResponseDto
     * @throws ErrorResponseException
     * @throws ServiceErrorResponseException
     */
    public function getCategoryById($id)
    {
        $request = RequestFactory::factory(
            ApiRoute::interpolate(
                ApiRoute::ROUTE_CATEGORY_SINGLE, [
                    'id' => $id
                ]
            ),
            HttpMethod::HTTP_GET
        );

        $response = $this->sendRequest($request);

        return ResponseFactory::responseDtoFactory($response);
    }

    /**
     * @param string $slug
     * @return ResponseDto
     * @throws ErrorResponseException
     * @throws ServiceErrorResponseException
     */
    public function getCategoryBySlug($slug)
    {
        $filter = new FilterDto('slug', $slug);

        $request = RequestFactory::factory(
            ApiRoute::ROUTE_CATEGORY_LIST,
            HttpMethod::HTTP_GET,
            [],
            null,
            new FilterCollectionDto([
                $filter
            ]),
            null,
            [RelationshipIncludes::ALL]
        );

        $response = $this->sendRequest($request);

        $paginatedResponse = ResponseFactory::paginatedResponseDtoFactory($response);

        // Force the collection into a ResponseDto
        $data = count($paginatedResponse->getData()) ? $paginatedResponse->getData()[0] : [];
        return new ResponseDto($paginatedResponse->getHeaders(), $data);
    }

    /**
     * @param PaginationDto|null $paginationDto
     * @param FilterCollectionDto|null $filterCollectionDto
     * @param SortCollectionDto|null $sortCollectionDto
     * @return PaginatedResponseDto
     * @throws ErrorResponseException
     * @throws ServiceErrorResponseException
     */
    public function getCategoryOptions(
        PaginationDto $paginationDto = null,
        FilterCollectionDto $filterCollectionDto = null,
        SortCollectionDto $sortCollectionDto = null
    )
    {
        $request = RequestFactory::factory(
            ApiRoute::ROUTE_CATEGORY_OPTION_LIST,
            HttpMethod::HTTP_GET,
            [],
            $paginationDto,
            $filterCollectionDto,
            $sortCollectionDto
        );

        $response = $this->sendRequest($request);

        return ResponseFactory::paginatedResponseDtoFactory($response);
    }

    /**
     * @param int $id
     * @return ResponseDto
     * @throws ErrorResponseException
     * @throws ServiceErrorResponseException
     */
    public function getCategoryOptionById($id)
    {
        $request = RequestFactory::factory(
            ApiRoute::interpolate(
                ApiRoute::ROUTE_CATEGORY_OPTION_SINGLE, [
                    'id' => $id
                ]
            ),
            HttpMethod::HTTP_GET
        );

        $response = $this->sendRequest($request);

        return ResponseFactory::responseDtoFactory($response);
    }

    /**
     * @param string $slug
     * @return ResponseDto
     * @throws ErrorResponseException
     * @throws ServiceErrorResponseException
     */
    public function getCategoryOptionBySlug($slug)
    {
        $filter = new FilterDto('slug', $slug);

        $request = RequestFactory::factory(
            ApiRoute::ROUTE_CATEGORY_OPTION_LIST,
            HttpMethod::HTTP_GET,
            [],
            null,
            new FilterCollectionDto([
                $filter
            ]),
            null,
            [RelationshipIncludes::ALL]
        );

        $response = $this->sendRequest($request);

        $paginatedResponse = ResponseFactory::paginatedResponseDtoFactory($response);

        // Force the collection into a ResponseDto
        $data = count($paginatedResponse->getData()) ? $paginatedResponse->getData()[0] : [];
        return new ResponseDto($paginatedResponse->getHeaders(), $data);
    }
}