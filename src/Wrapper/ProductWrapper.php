<?php

namespace Sugarcoat\APIWrapper\Wrapper;

use Sugarcoat\APIWrapper\Constant\ApiRoute;
use Sugarcoat\APIWrapper\Constant\FilterOperator;
use Sugarcoat\APIWrapper\Constant\HttpMethod;
use Sugarcoat\APIWrapper\Constant\ProductRecommendationType;
use Sugarcoat\APIWrapper\Constant\RelationshipIncludes;
use Sugarcoat\APIWrapper\Dto\FilterCollectionDto;
use Sugarcoat\APIWrapper\Dto\FilterDto;
use Sugarcoat\APIWrapper\Dto\PaginatedResponseDto;
use Sugarcoat\APIWrapper\Dto\PaginationDto;
use Sugarcoat\APIWrapper\Dto\ResponseDto;
use Sugarcoat\APIWrapper\Dto\SortCollectionDto;
use Sugarcoat\APIWrapper\Exception\ErrorResponseException;
use Sugarcoat\APIWrapper\Exception\ServiceErrorResponseException;
use Sugarcoat\APIWrapper\Factory\RequestFactory;
use Sugarcoat\APIWrapper\Factory\ResponseFactory;

class ProductWrapper extends BaseWrapper
{
    /**
     * @param PaginationDto|null $paginationDto
     * @param FilterCollectionDto|null $filterCollectionDto
     * @param SortCollectionDto|null $sortCollectionDto
     * @return PaginatedResponseDto
     * @throws ErrorResponseException
     * @throws ServiceErrorResponseException
     */
    public function getProducts(
        PaginationDto $paginationDto = null,
        FilterCollectionDto $filterCollectionDto = null,
        SortCollectionDto $sortCollectionDto = null
    )
    {
        $request = RequestFactory::factory(
            ApiRoute::ROUTE_PRODUCT_LIST,
            HttpMethod::HTTP_GET,
            [],
            $paginationDto,
            $filterCollectionDto,
            $sortCollectionDto
        );

        $response = $this->sendRequest($request);

        return ResponseFactory::paginatedResponseDtoFactory($response);
    }

    /**
     * @param int $id
     * @return ResponseDto
     * @throws ErrorResponseException
     * @throws ServiceErrorResponseException
     */
    public function getProductById($id)
    {
        $request = RequestFactory::factory(
            ApiRoute::interpolate(
                ApiRoute::ROUTE_PRODUCT_SINGLE, [
                    'id' => $id
                ]
            ),
            HttpMethod::HTTP_GET
        );

        $response = $this->sendRequest($request);

        return ResponseFactory::responseDtoFactory($response);
    }

    /**
     * @param string $name
     * @return PaginatedResponseDto
     * @throws ErrorResponseException
     * @throws ServiceErrorResponseException
     */
    public function getProductByName($name)
    {
        $filter = new FilterDto('name', $name);

        $request = RequestFactory::factory(
            ApiRoute::ROUTE_PRODUCT_LIST,
            HttpMethod::HTTP_GET,
            [],
            null,
            new FilterCollectionDto([
                $filter
            ]),
            null,
            [RelationshipIncludes::ALL]
        );

        $response = $this->sendRequest($request);

        return ResponseFactory::paginatedResponseDtoFactory($response);
    }

    /**
     * @param string $slug
     * @return ResponseDto
     * @throws ErrorResponseException
     * @throws ServiceErrorResponseException
     */
    public function getProductBySlug($slug)
    {
        $filter = new FilterDto('slug', $slug);

        $request = RequestFactory::factory(
            ApiRoute::ROUTE_PRODUCT_LIST,
            HttpMethod::HTTP_GET,
            [],
            null,
            new FilterCollectionDto([
                $filter
            ]),
            null,
            [RelationshipIncludes::ALL]
        );

        $response = $this->sendRequest($request);

        $paginatedResponse = ResponseFactory::paginatedResponseDtoFactory($response);

        // Force the collection into a ResponseDto
        $data = count($paginatedResponse->getData()) ? $paginatedResponse->getData()[0] : [];
        return new ResponseDto($paginatedResponse->getHeaders(), $data);
    }

    /**
     * @param array $ids
     * @return PaginatedResponseDto
     * @throws ErrorResponseException
     * @throws ServiceErrorResponseException
     */
    public function getProductsByTagIds(array $ids)
    {
        $filter = new FilterDto('tags', $ids, FilterOperator::OPERATOR_WHERE);

        $request = RequestFactory::factory(
            ApiRoute::ROUTE_PRODUCT_LIST,
            HttpMethod::HTTP_GET,
            [],
            null,
            new FilterCollectionDto([
                $filter
            ])
        );

        $response = $this->sendRequest($request);

        return ResponseFactory::paginatedResponseDtoFactory($response);
    }

    /**
     * @param array $ids
     * @return PaginatedResponseDto
     * @throws ErrorResponseException
     * @throws ServiceErrorResponseException
     */
    public function getProductsByIds(array $ids)
    {
        $filter = new FilterDto('id', $ids, FilterOperator::OPERATOR_WHERE_IN);

        $request = RequestFactory::factory(
            ApiRoute::ROUTE_PRODUCT_LIST,
            HttpMethod::HTTP_GET,
            [],
            null,
            new FilterCollectionDto([
                $filter
            ])
        );

        $response = $this->sendRequest($request);

        return ResponseFactory::paginatedResponseDtoFactory($response);
    }

    /**
     * @param string $searchTerm
     * @return PaginatedResponseDto
     * @throws ErrorResponseException
     * @throws ServiceErrorResponseException
     */
    public function searchProducts($searchTerm)
    {
        $request = RequestFactory::factory(
            ApiRoute::ROUTE_SEARCH_PRODUCT_LIST,
            HttpMethod::HTTP_GET,
            [
                'query' => $searchTerm
            ]
        );

        $response = $this->sendRequest($request);

        return ResponseFactory::paginatedResponseDtoFactory($response);
    }

    /**
     * @param array $ids
     * @param array $type
     * @param PaginationDto|null $paginationDto
     * @param FilterCollectionDto|null $filterCollectionDto
     * @param SortCollectionDto|null $sortCollectionDto
     * @return PaginatedResponseDto
     * @throws ErrorResponseException
     * @throws ServiceErrorResponseException
     */
    public function getRecommendedProducts(
        array $ids,
        $types = [ProductRecommendationType::ALL],
        PaginationDto $paginationDto = null,
        FilterCollectionDto $filterCollectionDto = null,
        SortCollectionDto $sortCollectionDto = null
    )
    {
        $request = RequestFactory::factory(
            ApiRoute::ROUTE_PRODUCT_RECOMMENDATION_LIST,
            HttpMethod::HTTP_GET,
            [
                'product_id' => $ids,
                'type' => $types
            ],
            $paginationDto,
            $filterCollectionDto,
            $sortCollectionDto
        );

        $response = $this->sendRequest($request);

        return ResponseFactory::paginatedResponseDtoFactory($response);
    }
}