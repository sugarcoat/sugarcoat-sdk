<?php

namespace Sugarcoat\APIWrapper\Wrapper;

use Sugarcoat\APIWrapper\Constant\ApiRoute;
use Sugarcoat\APIWrapper\Constant\HttpMethod;
use Sugarcoat\APIWrapper\Dto\PaginatedResponseDto;
use Sugarcoat\APIWrapper\Dto\ResponseDto;
use Sugarcoat\APIWrapper\Exception\ErrorResponseException;
use Sugarcoat\APIWrapper\Exception\ServiceErrorResponseException;
use Sugarcoat\APIWrapper\Factory\RequestFactory;
use Sugarcoat\APIWrapper\Factory\ResponseFactory;

class UserWrapper extends BaseWrapper
{
    /**
     * @param int $id
     * @return ResponseDto
     * @throws ErrorResponseException
     * @throws ServiceErrorResponseException
     */
    public function getUserById($id)
    {
        $request = RequestFactory::factory(
            ApiRoute::interpolate(
                ApiRoute::ROUTE_USER_SINGLE, [
                    'id' => $id
                ]
            ),
            HttpMethod::HTTP_GET
        );

        $responseDto = $this->sendRequest($request);

        return ResponseFactory::responseDtoFactory($responseDto);
    }

    /**
     * @param array $data
     * @return ResponseDto
     * @throws ErrorResponseException
     * @throws ServiceErrorResponseException
     */
    public function registerUser(array $data)
    {
        $request = RequestFactory::factory(
            ApiRoute::ROUTE_USER_LIST,
            HttpMethod::HTTP_POST,
            $data
        );

        $responseDto = $this->sendRequest($request);

        return ResponseFactory::responseDtoFactory($responseDto);
    }

    /**
     * @param int $id
     * @param array $data
     * @return ResponseDto
     * @throws ErrorResponseException
     * @throws ServiceErrorResponseException
     */
    public function updateUser($id, array $data)
    {
        $request = RequestFactory::factory(
            ApiRoute::interpolate(
                ApiRoute::ROUTE_USER_SINGLE, [
                    'id' => $id
                ]
            ),
            HttpMethod::HTTP_PUT,
            $data
        );

        $responseDto = $this->sendRequest($request);

        return ResponseFactory::responseDtoFactory($responseDto);
    }

    /**
     * @param string $code
     * @return ResponseDto
     * @throws ErrorResponseException
     * @throws ServiceErrorResponseException
     */
    public function activateAccount($code)
    {
        $request = RequestFactory::factory(
            ApiRoute::ROUTE_USER_ACCOUNT_ACTIVATE,
            HttpMethod::HTTP_POST,
            [
                'code' => $code
            ]
        );

        $responseDto = $this->sendRequest($request);

        return ResponseFactory::responseDtoFactory($responseDto);
    }

    /**
     * @param string $email
     * @return ResponseDto
     * @throws ErrorResponseException
     * @throws ServiceErrorResponseException
     */
    public function resendActivation($email)
    {
        $request = RequestFactory::factory(
            ApiRoute::ROUTE_USER_ACCOUNT_RESEND_ACTIVATION,
            HttpMethod::HTTP_POST,
            [
                'email' => $email
            ]
        );

        $responseDto = $this->sendRequest($request);

        return ResponseFactory::responseDtoFactory($responseDto);
    }

    /**
     * @param string $email
     * @return ResponseDto
     * @throws ErrorResponseException
     * @throws ServiceErrorResponseException
     */
    public function forgottenPassword($email)
    {
        $request = RequestFactory::factory(
            ApiRoute::ROUTE_USER_ACCOUNT_FORGOTTEN_PASSWORD,
            HttpMethod::HTTP_POST,
            [
                'email' => $email
            ]
        );

        $responseDto = $this->sendRequest($request);

        return ResponseFactory::responseDtoFactory($responseDto);
    }

    /**
     * @param string $code
     * @param string $password
     * @return ResponseDto
     * @throws ErrorResponseException
     * @throws ServiceErrorResponseException
     */
    public function resetPassword($code, $password)
    {
        $request = RequestFactory::factory(
            ApiRoute::ROUTE_USER_ACCOUNT_RESET_PASSWORD,
            HttpMethod::HTTP_POST,
            [
                'code' => $code,
                'password' => $password,
            ]
        );

        $responseDto = $this->sendRequest($request);

        return ResponseFactory::responseDtoFactory($responseDto);
    }

    /**
     * @param string $email
     * @param string $password
     * @return ResponseDto
     * @throws ErrorResponseException
     * @throws ServiceErrorResponseException
     */
    public function login($email, $password)
    {
        $request = RequestFactory::factory(
            ApiRoute::ROUTE_USER_ACCOUNT_LOGIN,
            HttpMethod::HTTP_POST,
            [
                'email' => $email,
                'password' => $password,
            ]
        );

        $responseDto = $this->sendRequest($request);

        return ResponseFactory::responseDtoFactory($responseDto);
    }

    /**
     * @param int $userId
     * @return PaginatedResponseDto
     * @throws ErrorResponseException
     * @throws ServiceErrorResponseException
     */
    public function getFavourites($userId)
    {
        $request = RequestFactory::factory(
            ApiRoute::interpolate(
                ApiRoute::ROUTE_USER_FAVOURITE_LIST, [
                    'id' => $userId
                ]
            ),
            HttpMethod::HTTP_GET
        );

        $responseDto = $this->sendRequest($request);

        return ResponseFactory::paginatedResponseDtoFactory($responseDto);
    }

    /**
     * @param int $userId
     * @param int $productId
     * @return PaginatedResponseDto
     * @throws ErrorResponseException
     * @throws ServiceErrorResponseException
     */
    public function addProductToFavourites($userId, $productId)
    {
        $request = RequestFactory::factory(
            ApiRoute::interpolate(
                ApiRoute::ROUTE_USER_FAVOURITE_SINGLE, [
                    'id' => $userId,
                    'product_id' => $productId
                ]
            ),
            HttpMethod::HTTP_PUT
        );

        $responseDto = $this->sendRequest($request);

        return ResponseFactory::paginatedResponseDtoFactory($responseDto);
    }

    /**
     * @param int $userId
     * @param int $productId
     * @return PaginatedResponseDto
     * @throws ErrorResponseException
     * @throws ServiceErrorResponseException
     */
    public function removeProductFromFavourites($userId, $productId)
    {
        $request = RequestFactory::factory(
            ApiRoute::interpolate(
                ApiRoute::ROUTE_USER_FAVOURITE_SINGLE, [
                    'id' => $userId,
                    'product_id' => $productId
                ]
            ),
            HttpMethod::HTTP_DELETE
        );

        $responseDto = $this->sendRequest($request);

        return ResponseFactory::paginatedResponseDtoFactory($responseDto);
    }

    /**
     * @param int $userId
     * @return PaginatedResponseDto
     * @throws ErrorResponseException
     * @throws ServiceErrorResponseException
     */
    public function getUserProductRecommendations($userId)
    {
        $request = RequestFactory::factory(
            ApiRoute::interpolate(
                ApiRoute::ROUTE_USER_RECOMMENDATION_LIST, [
                    'id' => $userId
                ]
            ),
            HttpMethod::HTTP_GET
        );

        $responseDto = $this->sendRequest($request);

        return ResponseFactory::paginatedResponseDtoFactory($responseDto);
    }
}

