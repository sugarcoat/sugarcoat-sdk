<?php

namespace Sugarcoat\APIWrapper\Wrapper;

use Sugarcoat\APIWrapper\Constant\ApiRoute;
use Sugarcoat\APIWrapper\Constant\HttpMethod;
use Sugarcoat\APIWrapper\Dto\PaginatedResponseDto;
use Sugarcoat\APIWrapper\Dto\ResponseDto;
use Sugarcoat\APIWrapper\Exception\ErrorResponseException;
use Sugarcoat\APIWrapper\Exception\ServiceErrorResponseException;
use Sugarcoat\APIWrapper\Factory\RequestFactory;
use Sugarcoat\APIWrapper\Factory\ResponseFactory;

class PartnerWrapper extends BaseWrapper
{
    /**
     * @param array $data
     * @return ResponseDto
     * @throws ErrorResponseException
     * @throws ServiceErrorResponseException
     */
    public function createPartner(array $data)
    {
        $request = RequestFactory::factory(
            ApiRoute::ROUTE_PARTNER_LIST,
            HttpMethod::HTTP_POST,
            $data
        );

        $response = $this->sendRequest($request);

        return ResponseFactory::responseDtoFactory($response);
    }
}

