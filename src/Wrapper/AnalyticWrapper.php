<?php

namespace Sugarcoat\APIWrapper\Wrapper;

use Sugarcoat\APIWrapper\Constant\ApiRoute;
use Sugarcoat\APIWrapper\Constant\HttpMethod;
use Sugarcoat\APIWrapper\Dto\ResponseDto;
use Sugarcoat\APIWrapper\Exception\ErrorResponseException;
use Sugarcoat\APIWrapper\Exception\ServiceErrorResponseException;
use Sugarcoat\APIWrapper\Factory\RequestFactory;
use Sugarcoat\APIWrapper\Factory\ResponseFactory;

class AnalyticWrapper extends BaseWrapper
{
    /**
     * @param array $data
     * @return ResponseDto
     * @throws ErrorResponseException
     * @throws ServiceErrorResponseException
     */
    public function trackAnalytic(array $data = [])
    {
        $request = RequestFactory::factory(
            ApiRoute::ROUTE_ANALYTIC_LIST,
            HttpMethod::HTTP_GET,
            $data
        );

        $response = $this->sendRequest($request);

        return ResponseFactory::responseDtoFactory($response);
    }

    /**
     * @param int $id
     * @param array $data
     * @return ResponseDto
     * @throws ErrorResponseException
     * @throws ServiceErrorResponseException
     */
    public function trackAnalyticByProductId($id, array $data = [])
    {
        $request = RequestFactory::factory(
            ApiRoute::ROUTE_ANALYTIC_LIST,
            HttpMethod::HTTP_GET,
            array_merge($data, [
                'product_id' => $id
            ])
        );

        $response = $this->sendRequest($request);

        return ResponseFactory::responseDtoFactory($response);
    }

    /**
     * @param string $destination
     * @param array $data
     * @return ResponseDto
     * @throws ErrorResponseException
     * @throws ServiceErrorResponseException
     */
    public function trackAnalyticByDestination($destination, array $data = [])
    {
        $request = RequestFactory::factory(
            ApiRoute::ROUTE_ANALYTIC_LIST,
            HttpMethod::HTTP_GET,
            array_merge($data, [
                'destination' => $destination
            ])
        );

        $response = $this->sendRequest($request);

        return ResponseFactory::responseDtoFactory($response);
    }

    /**
     * @param string $type
     * @param array $data
     * @return ResponseDto
     * @throws ErrorResponseException
     * @throws ServiceErrorResponseException
     */
    public function trackAnalyticByType($type, array $data = [])
    {
        $request = RequestFactory::factory(
            ApiRoute::ROUTE_ANALYTIC_LIST,
            HttpMethod::HTTP_GET,
            array_merge($data, [
                'type' => $type
            ])
        );

        $response = $this->sendRequest($request);

        return ResponseFactory::responseDtoFactory($response);
    }
}