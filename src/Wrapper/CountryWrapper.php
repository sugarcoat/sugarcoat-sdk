<?php

namespace Sugarcoat\APIWrapper\Wrapper;

use Sugarcoat\APIWrapper\Constant\ApiRoute;
use Sugarcoat\APIWrapper\Constant\HttpMethod;
use Sugarcoat\APIWrapper\Dto\PaginatedResponseDto;
use Sugarcoat\APIWrapper\Exception\ErrorResponseException;
use Sugarcoat\APIWrapper\Exception\ServiceErrorResponseException;
use Sugarcoat\APIWrapper\Factory\RequestFactory;
use Sugarcoat\APIWrapper\Factory\ResponseFactory;

class CountryWrapper extends BaseWrapper
{
    /**
     * @return PaginatedResponseDto
     * @throws ErrorResponseException
     * @throws ServiceErrorResponseException
     */
    public function getCountries()
    {
        $request = RequestFactory::factory(
            ApiRoute::ROUTE_COUNTRY_LIST,
            HttpMethod::HTTP_GET,
            []
        );

        $response = $this->sendRequest($request);

        return ResponseFactory::paginatedResponseDtoFactory($response);
    }
}