<?php

namespace Sugarcoat\APIWrapper\Wrapper;

use Sugarcoat\APIWrapper\Constant\ApiRoute;
use Sugarcoat\APIWrapper\Constant\HttpMethod;
use Sugarcoat\APIWrapper\Dto\FilterCollectionDto;
use Sugarcoat\APIWrapper\Dto\PaginatedResponseDto;
use Sugarcoat\APIWrapper\Dto\PaginationDto;
use Sugarcoat\APIWrapper\Dto\SortCollectionDto;
use Sugarcoat\APIWrapper\Exception\ErrorResponseException;
use Sugarcoat\APIWrapper\Exception\ServiceErrorResponseException;
use Sugarcoat\APIWrapper\Factory\RequestFactory;
use Sugarcoat\APIWrapper\Factory\ResponseFactory;

class TermWrapper extends BaseWrapper
{
    /**
     * @param PaginationDto|null $paginationDto
     * @param FilterCollectionDto|null $filterCollectionDto
     * @param SortCollectionDto|null $sortCollectionDto
     * @return PaginatedResponseDto
     * @throws ErrorResponseException
     * @throws ServiceErrorResponseException
     */
    public function getTerms(
        PaginationDto $paginationDto = null,
        FilterCollectionDto $filterCollectionDto = null,
        SortCollectionDto $sortCollectionDto = null
    )
    {
        $request = RequestFactory::factory(
            ApiRoute::ROUTE_TERM_LIST,
            HttpMethod::HTTP_GET,
            [],
            $paginationDto,
            $filterCollectionDto,
            $sortCollectionDto
        );

        $responseDto = $this->sendRequest($request);

        return ResponseFactory::paginatedResponseDtoFactory($responseDto);
    }
}