<?php

namespace Sugarcoat\APIWrapper\Factory;

use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\ServerRequest;
use Psr\Http\Message\ServerRequestInterface;
use Sugarcoat\APIWrapper\Dto\FilterCollectionDto;
use Sugarcoat\APIWrapper\Dto\PaginationDto;
use Sugarcoat\APIWrapper\Dto\SortCollectionDto;

class RequestFactory
{
    /**
     * @param $endPoint
     * @param $method
     * @param array $data
     * @param PaginationDto|null $paginationDto
     * @param FilterCollectionDto|null $filterCollectionDto
     * @param SortCollectionDto|null $sortCollectionDto
     * @param array $includes
     * @return \Psr\Http\Message\ServerRequestInterface
     */
    public static function factory(
        $endPoint,
        $method,
        array $data = [],
        PaginationDto $paginationDto = null,
        FilterCollectionDto $filterCollectionDto = null,
        SortCollectionDto $sortCollectionDto = null,
        array $includes = []
    )
    {
        $body = $data;

        if (null != $paginationDto) {
            $body = array_merge($body, $paginationDto->toArray());
        }

        if (null != $filterCollectionDto) {
            $body = array_merge($body, $filterCollectionDto->toArray());
        }

        if (null != $sortCollectionDto) {
            $body = array_merge($body, $sortCollectionDto->toArray());
        }

        if (!empty($includes)) {
            $body = array_merge($body, [
                'include' => implode(',', $includes)
            ]);
        }

        return new ServerRequest($method, $endPoint, [], null, '1.1', $body);
    }
}