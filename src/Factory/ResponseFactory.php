<?php

namespace Sugarcoat\APIWrapper\Factory;

use Psr\Http\Message\ResponseInterface;
use Sugarcoat\APIWrapper\Dto\PaginatedResponseDto;
use Sugarcoat\APIWrapper\Dto\ResponseDto;

class ResponseFactory
{
    /**
     * @param ResponseInterface $response
     * @return ResponseDto
     */
    public static function responseDtoFactory(ResponseInterface $response)
    {
        $bodyData = [];

        $data = json_decode($response->getBody()->getContents(), true);

        if (count(array_keys($data)) === 1) {
            // Assumed keyed object
            $bodyData = $data[array_keys($data)[0]];
        } else {
            // Assumed non-keyed object
            $bodyData = $data;
        }

        return new ResponseDto($response->getHeaders(), $bodyData);
    }

    /**
     * @param ResponseInterface $response
     * @return PaginatedResponseDto
     */
    public static function paginatedResponseDtoFactory(ResponseInterface $response)
    {
        $data = json_decode($response->getBody()->getContents(), true);

        $count = isset($data['count']) ? (int)$data['count'] : 0;
        $currentPage = isset($data['current_page']) ? (int)$data['current_page'] : 0;
        $lastPage = isset($data['last_page']) ? (int)$data['last_page'] : 0;
        $interval = isset($data['interval']) ? (int)$data['interval'] : 0;

        // Unset from the main response body
        unset($data['count'], $data['current_page'], $data['last_page'], $data['interval']);


        if (count(array_keys($data)) === 1) {
            // Assumed keyed object
            $bodyData = $data[array_keys($data)[0]];
        } else {
            // Assumed non-keyed object
            $bodyData = $data;
        }

        // Reset data to get the collection response key values
        return new PaginatedResponseDto($response->getHeaders(), $bodyData, $count, $interval, $currentPage, $lastPage);
    }
}